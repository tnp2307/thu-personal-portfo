module.exports = {
  i18n: {
    locales: ["vi", "cn", "cm", "la"],
    defaultLocale: "vi",
    localeDetection: false,
  },
  trailingSlash: true,
};
