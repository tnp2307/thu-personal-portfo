importScripts("https://www.gstatic.com/firebasejs/8.10.1/firebase-app.js");
importScripts(
  "https://www.gstatic.com/firebasejs/8.10.1/firebase-messaging.js"
);

firebase.initializeApp({
  apiKey: "AIzaSyCbwf12qeC94VdcnShSsG0DJkdkri8yuKU",
  authDomain: "best-english-3a100.firebaseapp.com",
  projectId: "best-english-3a100",
  storageBucket: "best-english-3a100.appspot.com",
  messagingSenderId: "250446488952",
  appId: "1:250446488952:web:7e8a6d7b7860773f9a948d",
  measurementId: "G-JG5YXW1YK7",
});

const messaging = firebase.messaging();

