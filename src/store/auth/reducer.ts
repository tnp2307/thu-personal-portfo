import {
  GET_USER_FAIL,
  GET_USER_REQUEST,
  GET_USER_SUCCESS,
  LOGIN_FAIL,
  LOGIN_MANUAL_REQUEST,
  LOGIN_SOCIAL_REQUEST,
} from "./actionTypes";
import type { AuthActions, AuthState } from "./types";

const initialState: AuthState = {
  pending: false,
  profile: null,
  error: null,
};

// eslint-disable-next-line @typescript-eslint/default-param-last
const authReducer = (state = initialState, action: AuthActions) => {
  switch (action.type) {
    case LOGIN_MANUAL_REQUEST:
    case LOGIN_SOCIAL_REQUEST:
      return {
        ...state,
        pending: true,
      };
    case LOGIN_FAIL:
      return {
        ...state,
        error: action.payload.error,
        pending: false,
      };
    case GET_USER_REQUEST:
      return {
        ...state,
        pending: true,
      };
    case GET_USER_SUCCESS:
      return {
        ...state,
        profile: action.payload.profile,
        pending: false,
      };
    case GET_USER_FAIL:
      return {
        ...state,
        error: action.payload.error,
        pending: false,
      };
    default:
      return {
        ...state,
      };
  }
};
export default authReducer;
