import { message } from "antd";
import { event } from "nextjs-google-analytics";
import { all, call, put, takeLatest } from "redux-saga/effects";

import type { ResGetUserProfile, ResLogin } from "@/interfaces/auth.interface";
import AuthService from "@/services/AuthService";
import LearnersService from "@/services/LearnersService";
import { RES_MESS, STORAGE_KEY } from "@/utils/config";
import { localStorageHelpers } from "@/utils/localStorage";

import {
  getUserFail,
  getUserRequest,
  getUserSuccess,
  loginFail,
} from "./actions";
import {
  GET_USER_REQUEST,
  LOGIN_MANUAL_REQUEST,
  LOGIN_SOCIAL_REQUEST,
} from "./actionTypes";
import type { LoginManualRequest, LoginSocialRequest } from "./types";

function* loginManualSaga({ payload }: LoginManualRequest): any {
  const key = "updatables";
  try {
    const response = yield call(AuthService.login, payload);
    const result: ResLogin = response.data;

    if (result.message === RES_MESS.SUCCESSFULL) {
      localStorageHelpers.saveToken(result.data.token);

      if (payload.rememberAccount) {
        localStorageHelpers.saveState(STORAGE_KEY.ACCOUNT, payload);
      } else {
        localStorageHelpers.deleteData(STORAGE_KEY.ACCOUNT);
      }
      event("Login event", {
        category: "Login",
        label: "Login count",
      });
      yield put(getUserRequest());
    } else {
      message.error({
        key,
        type: "loading",
        content: "Đăng nhập thất bại!",
      });
      yield put(
        loginFail({
          error: result.message,
        })
      );
    }
  } catch (e) {
    yield put(
      loginFail({
        error: e.message,
      })
    );
  }
}

function* loginSocialSaga({ payload }: LoginSocialRequest): any {
  try {
    const response = yield call(AuthService.loginSocial, payload);
    const result: ResLogin = response.data;
    if (result.message === RES_MESS.SUCCESSFULL) {
      localStorageHelpers.saveToken(result.data.token);
      localStorageHelpers.saveState(STORAGE_KEY.SOCIAL, payload);
      event("Login event", {
        category: "Login",
        label: "Login count",
      });
      yield put(getUserRequest());
    } else {
      yield put(
        loginFail({
          error: result.message,
        })
      );
    }
  } catch (e) {
    yield put(
      loginFail({
        error: e.message,
      })
    );
  }
}

function* getUserSaga(): any {
  try {
    const response = yield LearnersService.getProfile();
    const result: ResGetUserProfile = response.data;
    if (result.message === RES_MESS.SUCCESSFULL) {
      yield put(
        getUserSuccess({
          profile: result.data,
        })
      );
    }
  } catch (e) {
    yield put(
      getUserFail({
        error: e.message,
      })
    );
  }
}

export default function* authSaga() {
  yield all([
    takeLatest(LOGIN_MANUAL_REQUEST, loginManualSaga),
    takeLatest(LOGIN_SOCIAL_REQUEST, loginSocialSaga),
    takeLatest(GET_USER_REQUEST, getUserSaga),
  ]);
}
