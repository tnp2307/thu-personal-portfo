import type { ReqLogin, ReqLoginSocial } from "@/interfaces/auth.interface";

import {
  GET_USER_FAIL,
  GET_USER_REQUEST,
  GET_USER_SUCCESS,
  LOGIN_FAIL,
  LOGIN_MANUAL_REQUEST,
  LOGIN_SOCIAL_REQUEST,
} from "./actionTypes";
import type {
  GetUserFail,
  GetUserFailPayload,
  GetUserRequest,
  GetUserSuccess,
  GetUserSuccessPayload,
  LoginFail,
  LoginFailPayload,
  LoginManualRequest,
  LoginSocialRequest,
} from "./types";

export const loginManualRequest = (payload: ReqLogin): LoginManualRequest => ({
  type: LOGIN_MANUAL_REQUEST,
  payload,
});

export const loginSocialRequest = (
  payload: ReqLoginSocial
): LoginSocialRequest => ({
  type: LOGIN_SOCIAL_REQUEST,
  payload,
});

export const loginFail = (payload: LoginFailPayload): LoginFail => ({
  type: LOGIN_FAIL,
  payload,
});

export const getUserRequest = (): GetUserRequest => ({
  type: GET_USER_REQUEST,
});

export const getUserSuccess = (
  payload: GetUserSuccessPayload
): GetUserSuccess => ({
  type: GET_USER_SUCCESS,
  payload,
});

export const getUserFail = (payload: GetUserFailPayload): GetUserFail => ({
  type: GET_USER_FAIL,
  payload,
});
