import type * as interfaces from "@/interfaces/notebook.interfaces";

import * as actionType from "./actionTypes";
import type * as type from "./types";

const initialState: type.NoteBookState = {
  pending: false,
  data: null,
  error: null,
  total: null,
  dataNoti: {
    noti: {
      totaldata: null,
      totalLength: 0,
      listunwatched: null,
      unwatchedLength: 0,
      call: false,
    },
  },
};

// eslint-disable-next-line @typescript-eslint/default-param-last
const pageReducer = (state = initialState, action: type.NoteLessonActions) => {
  switch (action.type) {
    case actionType.GET_FOOTER_REQUEST:
      return {
        ...state,
        action,
        pending: true,
      };
    case actionType.GET_FOOTER_REQUEST_SUCC:
      return {
        ...state,
        data: action?.payload?.items,
        pending: false,
        total: action?.payload?.items.length,
      };
    case actionType.GET_FOOTER_REQUEST_FAIL:
      return {
        ...state,
        error: action.payload.error,
        pending: false,
        total: 0,
      };

    //
    case actionType.GET_NOTIFICATION_REQUEST:
      return {
        ...state,
        action,
        pending: true,
      };
    case actionType.GET_NOTIFICATION_REQUEST_SUCC:
      // eslint-disable-next-line no-case-declarations
      const dataPayload: any = action?.payload?.items?.items;

      // eslint-disable-next-line no-case-declarations
      const data = dataPayload?.sort((a: any, b: any) => {
        const timeA: any = new Date(a.addedTimeStamp);
        const timeB: any = new Date(b.addedTimeStamp);
        return timeB - timeA;
      });

      // eslint-disable-next-line no-case-declarations
      const listData: interfaces.ResGetListNotificationData[] | null =
        data?.filter((item: any) => item.isChecked === false);

      return {
        ...state,
        pending: false,
        dataNoti: {
          ...state.dataNoti,
          noti: {
            totaldata: data,
            totalLength: data?.length,
            listunwatched: listData,
            unwatchedLength: listData?.length,
            call: true,
          },
        },
      };
    case actionType.GET_NOTIFICATION_REQUEST_FAIL:
      return {
        ...state,
        error: action.payload.error,
        pending: false,
      };
    default:
      return {
        ...state,
      };
  }
};
export default pageReducer;
