import type * as interfaces from "@/interfaces/notebook.interfaces";

import type * as action from "./actionTypes";

export interface NoteBookState {
  data: interfaces.ResGetListNoteBookData[] | null;
  pending: boolean;
  error: string | null;
  total: number | null;
  dataNoti: interfaces.Notifi;
  // {
  //   noti: {
  //     totaldata: interfaces.ResGetListNotificationData[] | null;
  //     listunwatched: interfaces.ResGetListNotificationData[] | null;
  //     totalLength: number;
  //     unwatchedLength: number;
  //     call: boolean;
  //   };
  // };
}

export interface NoteBookDataRequest {
  type: typeof action.GET_FOOTER_REQUEST;
}
export interface NotifiDataRequest {
  type: typeof action.GET_NOTIFICATION_REQUEST;
}

export type NoteLessonActions = NoteBookDataRequest | NotifiDataRequest | any;
