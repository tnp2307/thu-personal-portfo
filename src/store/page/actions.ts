import * as action from "./actionTypes";
import type * as type from "./types";

export const FooterRequest = (): type.NoteBookDataRequest => {
  return {
    type: action.GET_FOOTER_REQUEST,
  };
};

export const FooterRequestSuccess = (payload: any): any => ({
  type: action.GET_FOOTER_REQUEST_SUCC,
  payload,
});

export const FooterRequestFail = (payload: any): any => ({
  type: action.GET_FOOTER_REQUEST_FAIL,
  payload,
});

export const NotificationRequest = (): type.NotifiDataRequest => {
  return {
    type: action.GET_NOTIFICATION_REQUEST,
  };
};

export const NotificationRequestSuccess = (payload: any): any => ({
  type: action.GET_NOTIFICATION_REQUEST_SUCC,
  payload,
});

export const NotificationRequestFail = (payload: any): any => ({
  type: action.GET_NOTIFICATION_REQUEST_FAIL,
  payload,
});
