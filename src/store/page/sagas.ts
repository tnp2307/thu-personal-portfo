import { all, call, put, takeLatest } from "redux-saga/effects";

import LearnersService from "@/services/ItemCourses";
import { RES_MESS } from "@/utils/config";

import * as actions from "./actions";
import * as actionTypes from "./actionTypes";

function* FooterReqSaga(): any {
  try {
    const response = yield call(LearnersService.staticPageFooter);

    const result: any = response.data;

    if (result?.message === RES_MESS.SUCCESSFULL) {
      yield put(
        actions.FooterRequestSuccess({
          items: result.data,
        })
      );
    } else {
      yield put(
        actions.FooterRequestFail({
          error: result.message,
        })
      );
    }
  } catch (e) {
    yield put(
      actions.FooterRequestFail({
        error: e.message,
      })
    );
  }
}

function* NotiReqSaga(): any {
  try {
    const response = yield call(LearnersService.notification);

    const result: any = response.data;

    if (result?.message === RES_MESS.SUCCESSFULL) {
      yield put(
        actions.NotificationRequestSuccess({
          items: result.data,
        })
      );
    } else {
      yield put(
        actions.NotificationRequestFail({
          error: result.message,
        })
      );
    }
  } catch (e) {
    yield put(
      actions.NotificationRequestFail({
        error: e.message,
      })
    );
  }
}
export default function* authSaga() {
  yield all([takeLatest(actionTypes.GET_FOOTER_REQUEST, FooterReqSaga)]);
  yield all([takeLatest(actionTypes.GET_NOTIFICATION_REQUEST, NotiReqSaga)]);
}
