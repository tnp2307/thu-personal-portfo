import { createSelector } from "reselect";

import type { AppState } from "../rootReducer";

const getPending = (state: AppState) => state.page.pending;

const getPage = (state: AppState) => state.page;

const getError = (state: AppState) => state.page.error;

export const getPageSelector = createSelector(getPage, (todos) => todos);

export const getPendingSelector = createSelector(
  getPending,
  (pending) => pending
);

export const getErrorSelector = createSelector(getError, (error) => error);
