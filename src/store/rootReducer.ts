import { combineReducers } from "redux";

import authReducer from "@/store/auth/reducer";
import blogReducer from "@/store/blog/reducer";
import notebookReducer from "@/store/notebook/reducer";
import pageReducer from "@/store/page/reducer";
import todoReducer from "@/store/todo/reducer";
import unitReducer from "@/store/unit/reducer";

const rootReducer = combineReducers({
  todo: todoReducer,
  auth: authReducer,
  unit: unitReducer,
  notebook: notebookReducer,
  page: pageReducer,
  blog: blogReducer,
});

export type AppState = ReturnType<typeof rootReducer>;

export default rootReducer;
