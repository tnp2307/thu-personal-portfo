import { all, fork } from "redux-saga/effects";

import authSaga from "./auth/sagas";
import blogSaga from "./blog/sagas";
import notebookSaga from "./notebook/sagas";
import pageSaga from "./page/sagas";
import todoSaga from "./todo/sagas";
import unitSaga from "./unit/sagas";

export function* rootSaga() {
  yield all([
    fork(todoSaga),
    fork(authSaga),
    fork(unitSaga),
    fork(notebookSaga),
    fork(pageSaga),
    fork(blogSaga),
  ]);
}
