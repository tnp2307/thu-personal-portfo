import { createSelector } from "reselect";

import type { AppState } from "../rootReducer";

const getPending = (state: AppState) => state.notebook.pending;

const getNoteBook = (state: AppState) => state.notebook;

const getError = (state: AppState) => state.notebook.error;

export const getNoteBookSelector = createSelector(
  getNoteBook,
  (todos) => todos
);

export const getPendingSelector = createSelector(
  getPending,
  (pending) => pending
);

export const getErrorSelector = createSelector(getError, (error) => error);
