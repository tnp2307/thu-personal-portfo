import { all, call, put, takeLatest } from "redux-saga/effects";

import LearnersService from "@/services/NoteService";
import { RES_MESS } from "@/utils/config";

import * as actions from "./actions";
import * as actionTypes from "./actionTypes";
import type * as types from "./types";

function* NoteBookReqSaga({ payload, search }: types.NoteBookDataRequest): any {
  try {
    const response = yield call(
      LearnersService.PostNoteBookSearch,
      payload,
      search
    );
    const result: any = response.data;

    if (result?.message === RES_MESS.SUCCESSFULL) {
      yield put(
        actions.notebookRequestSuccess({
          items: result.data,
        })
      );
    } else {
      yield put(
        actions.notebookRequestFail({
          error: result.message,
        })
      );
    }
  } catch (e) {
    yield put(
      actions.notebookRequestFail({
        error: e.message,
      })
    );
  }
}

function* StatisticalRequestReqSaga(): any {
  try {
    const response = yield call(LearnersService.Statistical);
    const result: any = response.data;

    if (result?.message === RES_MESS.SUCCESSFULL) {
      yield put(
        actions.StatisticalSuccess({
          items: result.data,
        })
      );
    } else {
      yield put(
        actions.notebookRequestFail({
          error: result.message,
        })
      );
    }
  } catch (e) {
    yield put(
      actions.notebookRequestFail({
        error: e.message,
      })
    );
  }
}

export default function* authSaga() {
  yield all([takeLatest(actionTypes.GET_NOTEBOOK_REQUEST, NoteBookReqSaga)]);
  yield all([
    takeLatest(actionTypes.GET_STATIS_REQUEST, StatisticalRequestReqSaga),
  ]);
}
