import type * as interfaces from "@/interfaces/notebook.interfaces";

import * as action from "./actionTypes";
import type * as type from "./types";

export const notebookRequest = (
  payload: interfaces.NoteBookSearch,
  search: interfaces.NoteBookSearchs
): type.NoteBookDataRequest => ({
  type: action.GET_NOTEBOOK_REQUEST,
  payload,
  search,
});

export const notebookRequestSuccess = (payload: any): any => ({
  type: action.GET_NOTEBOOK_REQUEST_SUCC,
  payload,
});

export const notebookRequestFail = (payload: any): any => ({
  type: action.GET_NOTEBOOK_REQUEST_FAIL,
  payload,
});

export const StatisticalRequest = (): any => ({
  type: action.GET_STATIS_REQUEST,
});

export const StatisticalSuccess = (payload: any): any => ({
  type: action.GET_STATIS_REQUEST_SUCC,
  payload,
});

export const StatisticalFail = (payload: any): any => ({
  type: action.GET_STATIS_REQUEST_FAIL,
  payload,
});
