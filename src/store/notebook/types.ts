import type * as interfaces from "@/interfaces/notebook.interfaces";

import type * as action from "./actionTypes";

export interface NoteBookState {
  data: interfaces.ResGetListNoteBookData[] | null;
  statistical: any;
  pending: boolean;
  error: string | null;
  total: number | null;
}

export interface NoteBookDataRequest {
  type: typeof action.GET_NOTEBOOK_REQUEST;
  payload: interfaces.NoteBookSearch;
  search: interfaces.NoteBookSearchs;
}

export type NoteLessonActions = NoteBookDataRequest | any;
