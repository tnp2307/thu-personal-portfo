import * as actionType from "./actionTypes";
import type * as type from "./types";

const initialState: type.NoteBookState = {
  pending: false,
  data: null,
  error: null,
  total: null,
  statistical: null,
};

// eslint-disable-next-line @typescript-eslint/default-param-last
const noteReducer = (state = initialState, action: type.NoteLessonActions) => {
  switch (action.type) {
    case actionType.GET_NOTEBOOK_REQUEST:
      return {
        ...state,
        action,
        pending: true,
      };
    case actionType.GET_NOTEBOOK_REQUEST_SUCC:
      // eslint-disable-next-line no-case-declarations
      const totalVoca: any = action.payload.items.filter(
        (x: any) => x.isReview === true
      );
      return {
        ...state,
        data: action.payload.items,
        pending: false,
        total: totalVoca.length,
      };
    case actionType.GET_NOTEBOOK_REQUEST_FAIL:
      return {
        ...state,
        error: action.payload.error,
        pending: false,
        total: 0,
      };
    case actionType.GET_STATIS_REQUEST:
      return {
        ...state,
        action,
        pending: true,
      };
    case actionType.GET_STATIS_REQUEST_SUCC:
      return {
        ...state,
        pending: false,
        statistical: action?.payload?.items,
      };
    default:
      return {
        ...state,
      };
  }
};
export default noteReducer;
