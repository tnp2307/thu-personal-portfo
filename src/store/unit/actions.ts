import type * as interfaces from "@/interfaces/unit.interface";
import type { ILearnStatus } from "@/services/LearnStatusService";

import * as action from "./actionTypes";
import type * as type from "./types";

export const unitRequest = (
  payload: interfaces.UnitDataRequest
): type.UnitDataRequest => ({
  type: action.GET_UNIT_REQUEST,
  payload,
});

export const unitRequestSuccess = (
  payload: interfaces.ResUnitData
): type.UnitSuccess => ({
  type: action.GET_UNIT_REQUEST_SUCCESS,
  payload,
});

export const unitRequestFail = (
  payload: type.UnitFailPayload
): type.UnitFail => ({
  type: action.GET_UNIT_REQUEST_FAIL,
  payload,
});
export const learnStatusRequest = (
  payload: ILearnStatus
): type.LearnStatusRequest => ({
  type: action.GET_STATUS_REQUEST,
  payload,
});

export const learnStatusSuccess = (payload: any): type.LearnStatusSuccess => ({
  type: action.GET_STATUS_REQUEST_SUCCESS,
  payload,
});

export const learnStatusFail = (payload: any): type.LearnStatusFail => ({
  type: action.GET_STATUS_REQUEST_FAIL,
  payload,
});

export const unitRequestAuth = (
  payload: interfaces.UnitDataRequest
): type.UnitDataRequestAuth => ({
  type: action.GET_UNIT_REQUEST_AUTH,
  payload,
});
