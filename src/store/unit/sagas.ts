import { all, call, put, takeLatest } from "redux-saga/effects";

import type * as interfaces from "@/interfaces/unit.interface";
import ItemCoursesService from "@/services/ItemCourses";
import LearnStatus from "@/services/LearnStatusService";
import { RES_MESS } from "@/utils/config";

import * as actions from "./actions";
import * as actionTypes from "./actionTypes";
import type * as types from "./types";

function* unitReqSaga({ payload }: types.UnitDataRequest): any {
  try {
    const response = yield call(ItemCoursesService.getDataCourses, payload);
    const result: interfaces.ResUnit = response.data;

    if (result?.message === RES_MESS.SUCCESSFULL) {
      yield put(
        actions.unitRequestSuccess({
          items: result.data?.items,
          totalRecord: result?.data?.totalRecord,
        })
      );
    } else {
      yield put(
        actions.unitRequestFail({
          error: result.message,
        })
      );
    }
  } catch (e) {
    yield put(
      actions.unitRequestFail({
        error: e.message,
      })
    );
  }
}

function* unitReqAuthSaga({ payload }: types.UnitDataRequestAuth): any {
  try {
    const response = yield call(ItemCoursesService.getDataCoursesAuth, payload);
    const result: interfaces.ResUnit = response.data;

    if (result?.message === RES_MESS.SUCCESSFULL) {
      yield put(
        actions.unitRequestSuccess({
          items: result.data?.items,
          totalRecord: result?.data?.totalRecord,
        })
      );
    } else {
      yield put(
        actions.unitRequestFail({
          error: result.message,
        })
      );
    }
  } catch (e) {
    yield put(
      actions.unitRequestFail({
        error: e.message,
      })
    );
  }
}
function* learnStatusSaga({ payload }: types.LearnStatusRequest): any {
  try {
    const response = yield call(LearnStatus.postStatus, payload);
    const result: any = response.data;

    if (result?.message === RES_MESS.SUCCESSFULL) {
      yield put(
        actions.learnStatusSuccess({
          items: result.data?.items,
        })
      );
    } else {
      yield put(
        actions.learnStatusFail({
          error: result.message,
        })
      );
    }
  } catch (e) {
    yield put(
      actions.learnStatusFail({
        error: e.message,
      })
    );
  }
}

export default function* authSaga() {
  yield all([takeLatest(actionTypes.GET_UNIT_REQUEST, unitReqSaga)]);
  yield all([takeLatest(actionTypes.GET_STATUS_REQUEST, learnStatusSaga)]);
  yield all([takeLatest(actionTypes.GET_UNIT_REQUEST_AUTH, unitReqAuthSaga)]);
}
