import * as actionType from "./actionTypes";
import type * as type from "./types";

const initialState: type.UnitState = {
  pending: false,
  unit: null,
  error: null,
};

// eslint-disable-next-line @typescript-eslint/default-param-last
const authReducer = (state = initialState, action: type.UnitLessonActions) => {
  switch (action.type) {
    case actionType.GET_UNIT_REQUEST:
      return {
        ...state,
        pending: true,
      };
    case actionType.GET_UNIT_REQUEST_AUTH:
      return {
        ...state,
        pending: true,
      };
    case actionType.GET_UNIT_REQUEST_FAIL:
      return {
        ...state,
        error: action.payload.error,
        pending: false,
      };
    case actionType.GET_UNIT_REQUEST_SUCCESS:
      // eslint-disable-next-line no-case-declarations
      const data = action?.payload?.items?.filter(
        (item: any) => item.isActive === true
      );
      return {
        ...state,
        unit: data,
        pending: false,
      };
    case actionType.GET_STATUS_REQUEST:
      return {
        ...state,
        pending: true,
      };
    case actionType.GET_STATUS_REQUEST_FAIL:
      return {
        ...state,
        error: action.payload.error,
        pending: false,
      };
    case actionType.GET_STATUS_REQUEST_SUCCESS:
      // eslint-disable-next-line no-case-declarations
      const dataStatus = action?.payload?.items?.filter(
        (item: any) => item.isActive === true
      );
      return {
        ...state,
        unit: dataStatus,
        pending: false,
      };
    default:
      return {
        ...state,
      };
  }
};
export default authReducer;
