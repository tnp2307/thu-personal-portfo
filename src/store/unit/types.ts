import type * as interfaces from "@/interfaces/unit.interface";
import type { ILearnStatus } from "@/services/LearnStatusService";

import type * as action from "./actionTypes";

export interface UnitState {
  unit: interfaces.ResGetListUnitData[] | null;
  pending: boolean;
  error: string | null;
}

// Unit Type
export interface UnitDataRequest {
  type: typeof action.GET_UNIT_REQUEST;
  payload: interfaces.UnitDataRequest;
}
export interface UnitDataRequestAuth {
  type: typeof action.GET_UNIT_REQUEST_AUTH;
  payload: interfaces.UnitDataRequest;
}

// Unit type Success
export interface UnitSuccess {
  type: typeof action.GET_UNIT_REQUEST_SUCCESS;
  payload: interfaces.ResUnitData;
}

// Unit type fail
export interface UnitFailPayload {
  error: string;
}
export interface UnitFail {
  type: typeof action.GET_UNIT_REQUEST_FAIL;
  payload: UnitFailPayload;
}

// Learn status Type
export interface LearnStatusRequest {
  type: typeof action.GET_STATUS_REQUEST;
  payload: ILearnStatus;
}

// Learn status type Success
export interface LearnStatusSuccess {
  type: typeof action.GET_STATUS_REQUEST_SUCCESS;
  payload: interfaces.ResUnitData;
}

// Learn status type fail
export interface LearnStatusPayload {
  error: string;
}
export interface LearnStatusFail {
  type: typeof action.GET_STATUS_REQUEST_FAIL;
  payload: UnitFailPayload;
}

export type UnitLessonActions =
  | UnitDataRequest
  | UnitDataRequestAuth
  | UnitFail
  | UnitSuccess
  | LearnStatusRequest
  | LearnStatusSuccess
  | LearnStatusFail;
