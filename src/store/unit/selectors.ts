import { createSelector } from "reselect";

const getPending = (state: any) => state.unit.pending;

const getUnit = (state: any) => state.unit.unit;

const getError = (state: any) => state.unit.error;

export const getUnitSelector = createSelector(getUnit, (todos) => todos);

export const getPendingSelector = createSelector(
  getPending,
  (pending) => pending
);

export const getErrorSelector = createSelector(getError, (error) => error);
