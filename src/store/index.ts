import { applyMiddleware, createStore } from "redux";
import logger from "redux-logger";
import { persistReducer, persistStore } from "redux-persist";
import storage from "redux-persist/lib/storage"; //
import createSagaMiddleware from "redux-saga";

import rootReducer from "@/store/rootReducer";
import { rootSaga } from "@/store/rootSaga";

const persistConfig = {
  key: "root",
  storage,
  whitelist: [],
};

// Create the saga middleware
const sagaMiddleware = createSagaMiddleware();

const persistedReducer = persistReducer(persistConfig, rootReducer);

// Mount it on the Store
const store = createStore(
  persistedReducer,
  applyMiddleware(sagaMiddleware, logger)
);

const persistor = persistStore(store);

// Run the saga
sagaMiddleware.run(rootSaga);

const config = { store, persistor };

export default config;
