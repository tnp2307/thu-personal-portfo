import type * as interfaces from "@/interfaces/blog.interfaces";

import type * as action from "./actionTypes";

export interface BlogState {
  data: interfaces.Blog;
  pending: boolean;
  error: string | null;
}

export interface NoteBookDataRequest {
  type: typeof action.GET_BLOG_REQUEST;
}
export type NoteLessonActions = NoteBookDataRequest | any;
