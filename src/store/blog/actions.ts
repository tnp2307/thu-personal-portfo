import * as action from "./actionTypes";

export const BlogRequest = (data: any): any => {
  return {
    payload: data,
    type: action.GET_BLOG_REQUEST,
  };
};

export const BlogRequestSuccess = (payload: any): any => ({
  type: action.GET_BLOG_REQUEST_SUCC,
  payload,
});

export const BlogRequestFail = (payload: any): any => ({
  type: action.GET_BLOG_REQUEST_FAIL,
  payload,
});
