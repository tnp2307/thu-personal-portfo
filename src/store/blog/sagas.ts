import { all, call, put, takeLatest } from "redux-saga/effects";

import LearnersService from "@/services/ItemCourses";
import { RES_MESS } from "@/utils/config";

import * as actions from "./actions";
import * as actionTypes from "./actionTypes";

function* BlogReqSaga(data: any): any {
  try {
    const response = yield call(LearnersService.Blog, data?.payload);

    const result: any = response?.data;

    if (result?.message === RES_MESS.SUCCESSFULL) {
      yield put(
        actions.BlogRequestSuccess({
          items: result.data,
        })
      );
    } else {
      yield put(
        actions.BlogRequestFail({
          error: result.message,
        })
      );
    }
  } catch (e) {
    yield put(
      actions.BlogRequestFail({
        error: e.message,
      })
    );
  }
}

export default function* authSaga() {
  yield all([takeLatest(actionTypes.GET_BLOG_REQUEST, BlogReqSaga)]);
}
