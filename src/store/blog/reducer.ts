import * as actionType from "./actionTypes";
import type * as type from "./types";

const initialState: type.BlogState = {
  pending: false,
  data: {
    blog: {
      data: null,
      total: 0,
    },
    homepage: {
      data: null,
      total: 0,
    },
  },
  error: null,
};

// eslint-disable-next-line @typescript-eslint/default-param-last
const pageReducer = (state = initialState, action: type.NoteLessonActions) => {
  switch (action.type) {
    case actionType.GET_BLOG_REQUEST:
      return {
        ...state,
        action,
        pending: true,
      };
    case actionType.GET_BLOG_REQUEST_SUCC:
      // eslint-disable-next-line no-case-declarations
      const dataBlog = action?.payload?.items;

      // eslint-disable-next-line no-case-declarations
      const filterData = dataBlog?.items?.filter(
        (item: any) => item.isActive === true
      );
      // eslint-disable-next-line no-case-declarations
      const getDataHomePage = filterData
        ?.filter((obj: any) => obj.publicHome === true)
        .slice(0, 3);
      return {
        ...state,
        data: {
          ...state.data,
          blog: {
            ...state.data.blog,
            data: filterData,
            total: filterData?.length,
          },
          homepage: {
            ...state.data.homepage,
            data: getDataHomePage,
            total: getDataHomePage?.length,
          },
        },
        pending: false,
      };
    case actionType.GET_BLOG_REQUEST_FAIL:
      return {
        ...state,
        error: action.payload.error,
        pending: false,
      };
    default:
      return {
        ...state,
      };
  }
};
export default pageReducer;
