import { ApiRouters } from "@/constants/apiRouters";
import { api } from "@/utils/axios.config";

const LEARNER_PATH = "/Learners";

const ItemReviseService = {
  revises: (value: number, index: number, year: number) => {
    return api.post(
      `${ApiRouters.DASHBROADS_CHART}?type=${value}&index=${index}&year=${year}`
    );
  },
  revisesLevel: () => {
    return api.post(`${ApiRouters.DASHBROADS_CHART_LEVEL}/`);
  },
};

export default ItemReviseService;
