import { ApiRouters, BaseRouter } from "@/constants/apiRouters";
import type * as type from "@/interfaces/courses.interfaces";
// import type * as type from "@/interfaces/question.interface";
import { api } from "@/utils/axios.config";

const QUESTION_PATH = "/FAQs";

const ItemQuestionService = {
  // question: (value: number) => {
  //   return api.post(`${ApiRouters.QUESTION_SEARCH}`);
  // },
  // revisesLevel: () => {
  //   return api.post(`${ApiRouters.DASHBROADS_CHART_LEVEL}/`);
  // },
  question: (body: type.ReqGetCourses) => {
    return api.post(`${ApiRouters.QUESTION_SEARCH}`, body);
  },
  blog: (body: type.ReqGetCourses) => {
    return api.post(`${BaseRouter.BLOG}${ApiRouters.SEARCH}`, body);
  },
};

export default ItemQuestionService;
