import { ApiRouters } from "@/constants/apiRouters";
import { api } from "@/utils/axios.config";

export interface ILearnStatus {
  isCompleted: boolean;
  unitHoursLearner: number;
  unitiD: string;
}

const LearnStatus = {
  postStatus: (body: ILearnStatus) => {
    return api.post(`${ApiRouters.LEARNSTATUS}`, body);
  },
};

export default LearnStatus;
