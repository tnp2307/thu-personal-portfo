import { ApiRouters, BaseRouter } from "@/constants/apiRouters";
import type * as type from "@/interfaces/learnnewworks.interfaces";
import { api } from "@/utils/axios.config";

const LEARNER_PATH = "/Learners";

const LearnersService = {
  getProfile: () => {
    return api.get(`${LEARNER_PATH}/Profile`);
  },
  learnNewWord: (body: type.ResponsaveNewLearn, id: any) => {
    return api.post(
      `${BaseRouter.LEARN_AND_REVIEWS}/${id}${ApiRouters.STUDIED}`,
      body
    );
  },
  saveHandBook: (body: type.ResponsaveHandBook) => {
    return api.put(`${BaseRouter.LEARN}${ApiRouters.NOTEBOOK}`, body);
  },
  postBindNotifi: (body: any) => {
    return api.post(
      `${BaseRouter.NOTIQUEUES}${ApiRouters.SCHEDULEQUEUE}`,
      body
    );
  },
};

export default LearnersService;
