import { apinotFO } from "@/utils/axiosnotFO.config";

const COMMON_PATH = "/Commons";

const CommonService = {
  getDistrict: (id: number | undefined) => {
    return apinotFO.get(`${COMMON_PATH}/districts?provinceId=${id}`);
  },
  getProvince: () => {
    return apinotFO.get(`${COMMON_PATH}/provinces`);
  },
};

export default CommonService;
