import { ApiRouters, BaseRouter } from "@/constants/apiRouters";
import type * as type from "@/interfaces/courses.interfaces";
import { api } from "@/utils/axios.config";
import { apinotFO } from "@/utils/axiosnotFO.config";

const LEARNER_PATH = "/Learners";

const ItemCoursesService = {
  getProfile: () => {
    return api.post(`${LEARNER_PATH}/Profile`);
  },
  getDataCourses: (body: any) => {
    return api.post(`/Courses/Search/NoAuth`, body);
  },
  getDataCoursesAuth: (body: any) => {
    return api.post(`Courses/SearchCourse/Auth`, body);
  },
  initCourses: (body: type.ReqGetCourses, id: string) => {
    return api.post(
      `${BaseRouter.COURSES}/${id}${ApiRouters.UNIT_NOT_LOGIN}`,
      body
    );
  },
  initCoursesAuth: (body: type.ReqGetCourses, id: string) => {
    return api.post(
      `${BaseRouter.COURSES}/${id}${ApiRouters.UNIT_LOGIN}`,
      body
    );
  },
  initLesson: (body: type.ReqGetCourses, id: string) => {
    return api.post(
      `${BaseRouter.COURSES_UNIT}/${id}${ApiRouters.UNIT_LESSON_NOT_LOGIN}`,
      body
    );
  },
  learnWords: (body: type.ReqGetCourses, id: string) => {
    return api.post(
      `${BaseRouter.LEARN_AND_REVIEWS}/${id}${ApiRouters.GET_DATA_LEARN}`,
      body
    );
  },
  ServiceUPdateAccount: (data: any) => {
    return api.post(
      `${BaseRouter.LANDINGPAGE}${ApiRouters.SEND_UPGRADE_ACCOUNT}`,
      data
    );
  },
  ServiceUPdateAccountInfor: (data: any) => {
    return api.put(`${ApiRouters.SEND_UPGRADE_ACCOUNT_INFO}`, data);
  },
  staticPage: () => {
    return apinotFO.get(
      `${BaseRouter.STATICPAGESFOS}${ApiRouters.GETSTATICPAGESFO}`
    );
  },
  staticPageDetails: (url: any) => {
    return apinotFO.get(
      `${BaseRouter.STATICPAGESFOS}${ApiRouters.GETSTATICPAGESDATAIL}/${url}`
    );
  },
  staticPageFooter: () => {
    return apinotFO.get(
      `${BaseRouter.STATICPAGESFOS}${ApiRouters.GROUPBYTYPEASYNCFO}/?lang=0`
    );
  },
  notification: () => {
    return api.get(`${BaseRouter.NOTIFYS}${ApiRouters.SEARCH}`);
  },
  SearchHashtagAsync: (data: any) => {
    return api.post(
      `${BaseRouter.BLOG}${
        ApiRouters.SEARCHHASHTAGASYNC
      }?hashTag=${encodeURIComponent(data)}`
    );
  },
  RefreshTokensFireBase: (body: any) => {
    return api.post(
      `${BaseRouter.AUTH}${ApiRouters.UPDATEFIREBASETOKEN}`,
      body
    );
  },
  UdpateNotifi: (body: any) => {
    return api.post(
      `${BaseRouter.NOTIFYS}${ApiRouters.UPDATEMPTIFIASYNC}`,
      body
    );
  },
  Blog: (body: any) => {
    return api.post(`${BaseRouter.BLOG}${ApiRouters.SEARCH}`, body);
  },
};

export default ItemCoursesService;
