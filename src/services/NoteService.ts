import { ApiRouters, BaseRouter } from "@/constants/apiRouters";
import type * as types from "@/interfaces/courses.interfaces";
import type * as type from "@/interfaces/notebook.interfaces";
// eslint-disable-next-line import/no-cycle
import { api } from "@/utils/axios.config";
import { apinotFO } from "@/utils/axiosnotFO.config";

const LearnersService = {
  PostNoteBookSearch: (
    getIDtype: type.NoteBookSearch,
    search: types.ReqGetCourses
  ) => {
    return api.post(
      `${BaseRouter.LEARN}${ApiRouters.NOTEBOOK_SEARCH}?type=${getIDtype.type}`,
      search
    );
  },
  PostNoteBookSearchs: (
    getIDtype: type.NoteBookSearch,
    search: types.ReqGetCourses
  ) => {
    return api.post(
      `${BaseRouter.LEARN}${ApiRouters.NOTEBOOK_SEARCH}?type=${getIDtype}`,
      search
    );
  },

  DeleteVocabularyNote: (getIDnote: type.VocabularyID) => {
    return apinotFO.delete(
      `${BaseRouter.VOCABULARY}${ApiRouters.VOCABULARY}${getIDnote}`
    );
  },

  ResDataReview: () => {
    return api.get(`${BaseRouter.LEARN_AND_REVIEWS}${ApiRouters.DATA_REVIEW}`);
  },
  PushDataReview: (bindAPIData: any) => {
    return api.put(
      `${BaseRouter.LEARN_AND_REVIEWS}${ApiRouters.REVIEW}`,
      bindAPIData
    );
  },
  RuleReiew: (bindAPIData: any) => {
    return api.post(
      `${BaseRouter.LEARN_AND_REVIEWS}${ApiRouters.RULEREVIEW}`,
      bindAPIData
    );
  },
  RemoveTokenFirebase: (token: any) => {
    return api.post(`${BaseRouter.LANDINGPAGE}${ApiRouters.SIGNOUT}`, token);
  },
  Statistical: () => {
    return api.post(`${ApiRouters.DASHBROADS_CHART_LEVEL}`);
  },
};

export default LearnersService;
