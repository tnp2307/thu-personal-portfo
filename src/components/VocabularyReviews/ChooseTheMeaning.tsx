/* eslint-disable no-nested-ternary */
import { message } from "antd";
import React, { useEffect, useRef, useState } from "react";

import { highlightWord } from "@/common/convert-string-learn/string-learn";
import * as callAPI from "@/common/services/apiReviewNoteBook";
import useTrans from "@/utils/userTrans";

import ModalFalse from "../../common/modal/beforeFail";
import ModalSuccess from "../../common/modal/beforeSuccess";

const ChooseTheMeaning = ({
  data,
  handleSaveData,
  setShowResults,
  showResults,
  keyStep,
}: any) => {
  const [correctItem, setCorrectItem] = useState("");
  const [choosenItem, setChoosenItem] = useState("");
  const [result, setResult] = useState("");
  const [newArr, setnewArr] = useState([]);
  const divRef: any = useRef(null);
  const [nextgames, setNextGames] = useState<any>(0);
  useEffect(() => {
    const convertData = [
      {
        id: 1,
        name: data?.meaning,
        // name: data?.meaningOfWordOne,
      },
      {
        id: 2,
        name: data?.meaningOfWordTwo,
      },
      {
        id: 3,
        name: data?.meaningOfWordThree,
      },
      {
        id: 4,
        name: data?.meaningOfWordFour,
      },
    ];
    const newArrs: any = convertData.sort(() => Math.random() - 0.5);
    setnewArr(newArrs);
  }, [data?.meaning]);
  const audioTune = new Audio(
    `${process.env.NEXT_PUBLIC_API_AUDIO}${data?.maleAudioVocabulary}`
    // "http://codeskulptor-demos.commondatastorage.googleapis.com/pang/pop.mp3"
  );
  const playSound = () => {
    // setAudioTunes();
    // audioTune.playbackRate = 1;
    audioTune.play();
  };
  const handleSubmit = async () => {
    if (choosenItem !== "") {
      const selectedAnswer: any = newArr.find(
        (item: any) => item.name === choosenItem
      );

      const dataRule = {
        gameType: 5, // Game số 5
        vocabularyLevel: data?.vocabularyLevel || 1,
        correctAnswer: choosenItem,
      };

      const response = await callAPI.ruleReview(dataRule);
      if (response?.message === "SUCCESSFULL") {
        if (selectedAnswer.name.toLowerCase() === data.meaning.toLowerCase()) {
          setShowResults((prevState: any) => ({
            ...prevState,
            id: data.id,
            submit: true,
            success: "1",
            keyStep,
          }));
          setCorrectItem(choosenItem);
        } else {
          const getCheckerTrue: any = newArr.find(
            (item: any) =>
              item.name.toLowerCase() === data.meaning.toLowerCase()
          );
          setResult(selectedAnswer?.name);
          setCorrectItem(getCheckerTrue?.name);
          setShowResults((prevState: any) => ({
            ...prevState,
            id: data.id,
            submit: true,
            success: "0",
            keyStep,
          }));
        }
      } else {
        message.error("Lỗi server:", response?.message);
      }
    }
  };

  const changeQuestion = (e: string) => {
    setChoosenItem(e);
    setResult("");
  };

  const handleNextStepChild = () => {
    setChoosenItem("");
    setCorrectItem("");
    setResult("");
    handleSaveData(showResults);
  };

  const ForgotQuestion = () => {
    setCorrectItem(data.meaning);
    setShowResults((prevState: any) => ({
      ...prevState,
      id: data.id,
      submit: true,
      success: "1",
      keyStep,
    }));
  };

  const handleKeyDown = (event: any) => {
    if (event.key === "Enter") {
      if (nextgames === 0) {
        handleSubmit();
        setNextGames(1);
      } else {
        handleNextStepChild();
        setNextGames(0);
      }
    }
  };
  const trans = useTrans();
  return (
    <>
      <div className="wrapper-item-cnt-render-bg">
        <h1>{trans["reviseWord.title.chooseMeaning"]}</h1>
        <div className="question-ctn-render-vocabulary-reviews">
          <div className="bg-question-child-header">
            <img src="/assets/images/img-boy-question.png" alt="img question" />
            <div className="info-box">
              <img
                src="/assets/icons/ic-speak-english.png"
                alt="icon speak"
                className="cursor-pointer"
                onClick={() => {
                  playSound();
                }}
              />
              <p>
                {highlightWord(
                  data?.sentence,
                  data?.correctAnswer || data?.word || ""
                )}
              </p>
            </div>
          </div>
          <div
            className="bg-item-choose-child-main"
            ref={divRef}
            tabIndex={0}
            onKeyDown={handleKeyDown}
          >
            {newArr.length > 0 &&
              newArr.map((key: any, value) => {
                return (
                  <div
                    key={value}
                    className={`choose-items ${
                      correctItem === key?.name
                        ? "active"
                        : choosenItem === key?.name
                        ? "choosen"
                        : ""
                    } ${result === key?.name ? "false" : ""}`}
                    onClick={() => {
                      if (!showResults?.submit) {
                        changeQuestion(key?.name);
                      }
                    }}
                    // onKeyDown={(e: any) => {
                    //   // eslint-disable-next-line @typescript-eslint/no-unused-expressions
                    //   e.keyCode === 13 && handleSubmit();
                    // }}
                    // tabIndex={0}
                  >
                    <span className="lowercase">{key?.name}</span>
                    {correctItem === key?.name ? (
                      <img src="/assets/icons/ic_check.svg" alt="icon check" />
                    ) : result === key?.name ? (
                      <img src="/assets/icons/ic_false.svg" alt="icon false" />
                    ) : choosenItem === key?.name ? (
                      <img
                        src="/assets/icons/ic-choosen.svg"
                        alt="icon choosen"
                      />
                    ) : (
                      <img
                        src="/assets/icons/ic-no-tick.svg"
                        alt="icon no tick"
                      />
                    )}
                  </div>
                );
              })}
          </div>
        </div>
      </div>
      {showResults?.submit ? (
        <div className="mt-0 flex flex-col items-center justify-center md:mt-[2rem]">
          <button
            className="button-submit-in-Vocabulary-Reviews z-10"
            onClick={() => handleNextStepChild()}
          >
            {trans["common.button.continue"]}
          </button>
        </div>
      ) : (
        <div className="mt-0 flex flex-col items-center justify-center md:mt-[2rem]">
          <button
            className="button-submit-in-Vocabulary-Reviews "
            disabled={!choosenItem}
            onClick={() => handleSubmit()}
          >
            {trans["common.button.check"]}
          </button>
          <div className="not-remember " onClick={() => ForgotQuestion()}>
            {trans["common.button.rememberedWords"]}
          </div>
        </div>
      )}
      {showResults?.success === "1" ? (
        <ModalSuccess data={data} />
      ) : (
        showResults?.success === "0" && <ModalFalse data={data} />
      )}
    </>
  );
};

export default ChooseTheMeaning;
