/* eslint-disable no-nested-ternary */
import { message } from "antd";
import React, { useRef, useState } from "react";

import {
  highlightWord,
  shuffleArray,
} from "@/common/convert-string-learn/string-learn";
import * as callAPI from "@/common/services/apiReviewNoteBook";
import useTrans from "@/utils/userTrans";

import ModalFalse from "../../common/modal/beforeFail";
import ModalSuccess from "../../common/modal/beforeSuccess";

const RewriteInVietnamese = ({
  data,
  handleSaveData,
  setShowResults,
  showResults,
  keyStep,
}: any) => {
  const divRef: any = useRef(null);
  const [nextgames, setNextGames] = useState<any>(0);
  const words = data?.meanSentence.split(" ");
  const wordObjects = words?.map((word: any, id: number) => ({
    show: false,
    name: word,
    id,
  }));

  const [isSpeaking, setIsSpeaking] = useState(false);
  const utteranceRef = useRef<any>(null);

  const shuffledArray = shuffleArray(wordObjects || []);

  // const shuffledWords = words.sort(() => Math.random() - 0.5);
  const [showData, setShowData] = useState({
    inputItems: [],
    chooseItems: shuffledArray,
  });

  const handleAddForm = (value: any) => {
    setShowData((prev: any) => {
      const updatedChooseItems = [...prev.chooseItems];
      const chosenItem = updatedChooseItems.find(
        (item) => item.id === value.id
      );

      if (chosenItem) {
        chosenItem.show = true;
      }
      return {
        ...prev,
        inputItems: [...prev.inputItems, { name: value.name, id: value.id }],
        chooseItems: updatedChooseItems,
      };
    });
  };

  const handleRemoveForm = (value: any) => {
    setShowData((prev: any) => {
      const updatedChooseItems = [...prev.chooseItems];
      const chosenItem = updatedChooseItems.find(
        (item: any) => item.id === value.id
      );
      if (chosenItem) {
        chosenItem.show = false;
      }
      return {
        ...prev,
        inputItems: prev.inputItems.filter((x: any) => x.id !== value.id),
        chooseItems: updatedChooseItems,
      };
    });
  };

  const handleSubmit = async () => {
    const output = showData.inputItems.map((item: any) => item.name);
    const sentence = output.join(" ");

    const dataRule = {
      gameType: 6, //  Game 6
      vocabularyLevel: data?.vocabularyLevel || 1,
      correctAnswer: sentence,
    };

    const response = await callAPI.ruleReview(dataRule);

    if (response?.message === "SUCCESSFULL") {
      if (sentence.toLowerCase() === data.meanSentence.toLowerCase()) {
        setShowResults((prev: any) => ({
          ...prev,
          id: data.id,
          submit: true,
          success: "1",
          keyStep,
        }));
      } else {
        setShowResults((prev: any) => ({
          ...prev,
          id: data.id,
          submit: true,
          success: "0",
          keyStep,
        }));
      }
    } else {
      message.error("Lỗi server:", response?.message);
    }
  };

  const handleNextStepChild = () => {
    setShowData({
      inputItems: [],
      chooseItems: shuffledArray,
    });
    handleSaveData(showResults);
  };

  const ForgotQuestion = () => {
    setShowResults((prevState: any) => ({
      ...prevState,
      id: data.id,
      submit: true,
      success: "1",
      keyStep,
    }));
  };

  const HandleSpeak = (rate: any, word: any) => {
    if (isSpeaking && utteranceRef.current) {
      utteranceRef.current.onend = null;
      utteranceRef.current.onerror = null;
      window.speechSynthesis.cancel();
      setIsSpeaking(false);
      // eslint-disable-next-line @typescript-eslint/no-use-before-define
      speech(rate, word);
      return;
    }

    // eslint-disable-next-line @typescript-eslint/no-use-before-define
    speech(rate, word);
  };

  const speech = (rate: any, word: any) => {
    const utterance = new SpeechSynthesisUtterance(word);
    setIsSpeaking(true);
    utteranceRef.current = utterance;
    utterance.rate = rate;

    utterance.onend = () => {
      setIsSpeaking(false);
    };
    utterance.onerror = () => {
      setIsSpeaking(false);
    };

    window.speechSynthesis.speak(utterance);
  };

  const handleKeyDown = (event: any) => {
    if (event.key === "Enter") {
      if (nextgames === 0) {
        handleSubmit();
        setNextGames(1);
      } else {
        handleNextStepChild();
        setNextGames(0);
      }
    }
  };
  const trans = useTrans();
  return (
    <>
      <div
        className="wrapper-item-cnt-render-bg"
        // onKeyDown={(e: any) => {
        //   // eslint-disable-next-line @typescript-eslint/no-unused-expressions
        //   e.keyCode === 13 && handleSubmit();
        // }}
        // tabIndex={0}
      >
        <h1>{trans["reviseWord.title"]}</h1>
        <div className="question-ctn-render-vocabulary-reviews">
          <div className="bg-question-child-header">
            <img src="/assets/images/img-boy-question.png" alt="img question" />
            <div className="info-box">
              <img
                src="/assets/icons/ic-speak-english.png"
                alt="icon speak"
                style={{ cursor: "pointer" }}
                onClick={() => HandleSpeak(0.6, data?.sentence)}
              />
              <p>
                {" "}
                {highlightWord(
                  data?.sentence,
                  data?.correctAnswer || data?.word || ""
                )}
              </p>
            </div>
          </div>
          <div
            className="bg-item-choose-child-rewrite"
            ref={divRef}
            tabIndex={0}
            onKeyDown={handleKeyDown}
          >
            <div className="bg-show-item">
              {showData?.inputItems?.map((value: any, index: number) => {
                return (
                  <div
                    className="bg-child-items hover"
                    key={index}
                    onClick={() => handleRemoveForm(value)}
                  >
                    {value.name}
                  </div>
                );
              })}
            </div>
            <div className="bg-choose-items">
              {showData?.chooseItems?.map((word: any, index: number) => {
                return (
                  <>
                    {/* {word?.show ? (
                      <div className="bg-child-items disabled" key={index}>
                        {word?.name}
                      </div>
                    ) : (
                      <div
                        className="bg-child-items"
                        key={index}
                        onClick={() => handleAddForm(word?.name)}
                      >
                        {word?.name}
                      </div>
                    )} */}
                    {!word.show && (
                      <div
                        className="bg-child-items"
                        key={index}
                        onClick={() => handleAddForm(word)}
                      >
                        {word?.name}
                      </div>
                    )}
                  </>
                );
              })}
            </div>
          </div>
        </div>
      </div>
      {showResults?.submit ? (
        <div className="mt-0 flex flex-col items-center justify-center md:mt-[2rem]">
          <button
            className="button-submit-in-Vocabulary-Reviews z-10"
            onClick={() => handleNextStepChild()}
          >
            {trans["common.button.continue"]}
          </button>
        </div>
      ) : (
        <div className="mt-0 flex flex-col items-center justify-center md:mt-[2rem]">
          <button
            className="button-submit-in-Vocabulary-Reviews"
            disabled={showData.inputItems.length !== shuffledArray.length}
            onClick={() => handleSubmit()}
          >
            {trans["common.button.check"]}
          </button>
          <div className="not-remember" onClick={() => ForgotQuestion()}>
            {trans["common.button.rememberedWords"]}
          </div>
        </div>
      )}
      {showResults?.success === "1" ? (
        <ModalSuccess data={data} />
      ) : (
        showResults?.success === "0" && <ModalFalse data={data} />
      )}
    </>
  );
};

export default RewriteInVietnamese;
