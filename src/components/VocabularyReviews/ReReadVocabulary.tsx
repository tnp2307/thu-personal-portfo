// react plugin used to create charts
import { Button, message } from "antd";

import BeforeFail from "@/common/modal/beforeFail";
import BeforeSuccess from "@/common/modal/beforeSuccess";
import * as callAPI from "@/common/services/apiReviewNoteBook";
import useTrans from "@/utils/userTrans";

import SpeakWord from "../SpeakWord";

const CpnReReadVocabulary = (props: any) => {
  const { data, endUnitStep, setstateTitle, stateTitle, keyStep } = props;
  const audioTune = new Audio(
    `${process.env.NEXT_PUBLIC_API_AUDIO}${data?.maleAudioVocabulary}`
    // "http://codeskulptor-demos.commondatastorage.googleapis.com/pang/pop.mp3"
  );
  // const [playInLoop, setPlayInLoop] = useState(false);
  const playSound = () => {
    // setAudioTunes();
    // audioTune.playbackRate = 1;
    audioTune.play();
  };
  const handleCheckType = async () => {
    const dataRule = {
      gameType: 2, //  Game 2
      vocabularyLevel: data?.vocabularyLevel || 1,
      correctAnswer: stateTitle.title,
    };

    const response = await callAPI.ruleReview(dataRule);
    if (response?.message === "SUCCESSFULL") {
      if (
        data.word.toLowerCase().trim() === stateTitle.title.toLowerCase().trim()
      ) {
        setstateTitle((prev: any) => ({
          ...prev,
          id: data.id,
          submit: true,
          success: "1",
          isSkipped: true,
          keyStep,
        }));
      } else {
        setstateTitle((prev: any) => ({
          ...prev,
          id: data.id,
          submit: true,
          success: "0",
          keyStep,
        }));
      }
    } else {
      message.error("Lỗi server:", response?.message);
    }
  };

  const handleStepQuestion = () => {
    setstateTitle((prev: any) => ({
      ...prev,
      isSkipped: true,
      id: data.id,
      submit: true,
      success: "1",
      keyStep,
      title: data.word.toLowerCase().trim(),
    }));
    // endUnitStep({
    //   id: data.id,
    //   submit: true,
    //   success: "1",
    //   keyStep,
    //   title: data.word.toLowerCase().trim(),
    // });
  };
  const trans = useTrans();
  return (
    <>
      <div className="mx-auto my-[1rem] w-[90%] md:w-[85%]">
        <SpeakWord
          setstateTitle={setstateTitle}
          stateTitle={stateTitle}
          dataAudio={data?.maleAudioVocabulary}
          handleNextQuestion={endUnitStep}
          handleCheckType={handleCheckType}
        />
      </div>{" "}
      <div className=" flex flex-col items-center justify-center">
        {stateTitle.submit ? (
          <div>
            {" "}
            {stateTitle.isSkipped === true ? (
              <Button
                size="large"
                type="primary"
                className="rounded-4"
                style={{ display: "block", height: "46px" }}
                onClick={() => endUnitStep(stateTitle)}
                disabled={!stateTitle.title}
              >
                <span className="leading-16">
                  {trans["common.button.continue"]}
                </span>
              </Button>
            ) : (
              <Button
                size="large"
                type="primary"
                className={`rounded-4`}
                style={{ display: "block", height: "46px" }}
                onClick={() => endUnitStep(stateTitle)}
                disabled={!stateTitle.title}
              >
                <span className="leading-16">
                  {trans["common.button.continue"]}
                </span>
              </Button>
            )}
          </div>
        ) : (
          <Button
            size="large"
            type="primary"
            className="rounded-4"
            style={{ display: "block", height: "46px" }}
            onClick={() => handleCheckType()}
            disabled={!stateTitle.title}
          >
            <span className="leading-16">{trans["common.button.check"]}</span>
          </Button>
        )}
      </div>
      <div
        onClick={() => handleStepQuestion()}
        className="center-txt-work z-10 my-3"
      >
        {trans["common.button.link.skipQuestion"]}
      </div>
      {stateTitle.success === "1" ? (
        <div style={{ marginTop: "200px" }}>
          <BeforeSuccess data={data} />
        </div>
      ) : (
        stateTitle.success === "0" && (
          <div style={{ marginTop: "200px" }}>
            <BeforeFail data={data} />
          </div>
        )
      )}
    </>
  );
};
export default CpnReReadVocabulary;
