// react plugin used to create charts
import { Button, message } from "antd";

import * as callAPI from "@/common/services/apiReviewNoteBook";
import useTrans from "@/utils/userTrans";

import ListenAndWrite from "../ListenAndWrite/ListenAndWriteReview";

const CpnListenAndRewrite = (props: any) => {
  const { data, handleSaveData, setShowResults, showResults, keyStep } = props;

  const handleSubmit = async () => {
    const dataRule = {
      gameType: 1, //  Game 1
      vocabularyLevel: data?.vocabularyLevel || 1,
      correctAnswer: showResults.title,
    };

    const response = await callAPI.ruleReview(dataRule);

    if (response?.message === "SUCCESSFULL") {
      if (
        data.word.toLowerCase().trim() ===
        showResults.title.toLowerCase().trim()
      ) {
        setShowResults((prev: any) => ({
          ...prev,
          id: data.id,
          submit: true,
          success: "1",
          keyStep,
        }));
      } else {
        setShowResults((prev: any) => ({
          ...prev,
          id: data.id,
          submit: true,
          success: "0",
          keyStep,
        }));
      }
    } else {
      message.error("Lỗi server:", response?.message);
    }
  };

  const ForgotQuestion = () => {
    setShowResults((prev: any) => ({
      ...prev,
      id: data.id,
      submit: true,
      success: "1",
      keyStep,
    }));
  };
  const trans = useTrans();
  return (
    <div>
      <div className="mx-auto my-[1rem] mb-[2rem] w-[90%] md:mb-[1.5rem] md:w-[85%]">
        <ListenAndWrite
          data={data}
          setstateTitle={setShowResults}
          stateTitle={showResults}
          handleCheckType={handleSubmit}
          handleSubmit={handleSubmit}
          handleSaveData={handleSaveData}
        />
      </div>
      <div className="mt-0 flex flex-col items-center justify-center md:mt-[2rem]">
        {showResults.submit ? (
          <Button
            size="large"
            type="primary"
            className="rounded-4"
            style={{ display: "block", height: "46px" }}
            onClick={() => handleSaveData(showResults)}
          >
            <span className="leading-16">
              {trans["common.button.continue"]}
            </span>
          </Button>
        ) : (
          <>
            <Button
              size="large"
              type="primary"
              className="rounded-4"
              style={{ display: "block", height: "46px" }}
              onClick={() => handleSubmit()}
              disabled={!showResults.title}
            >
              <span className="leading-16">{trans["common.button.check"]}</span>
            </Button>
            <div className="not-remember" onClick={() => ForgotQuestion()}>
              {trans["common.button.rememberedWords"]}
            </div>
          </>
        )}
      </div>
    </div>
  );
};
export default CpnListenAndRewrite;
