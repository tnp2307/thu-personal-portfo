// react plugin used to create charts
import { Button } from "antd";
import React, { useState } from "react";

import BeforeFail from "@/common/modal/beforeFail";
import BeforeSuccess from "@/common/modal/beforeSuccess";
import useTrans from "@/utils/userTrans";

import FillWord from "../FillWord";

const CpnFillWords = (props: any) => {
  const { data, handleSaveData, setstateTitle, stateTitle, keyStep } = props;

  const filteredKeyGameTyping = data?.keyGameTyping?.filter(
    (letter: any) => letter !== " "
  );

  const [placeholders, setPlaceholders] = useState("");

  const handleplaceholders = () => {
    const isEqual = data?.word === placeholders;

    if (isEqual) {
      setstateTitle((prev: any) => ({
        ...prev,
        success: "1",
        id: data.id,
        submit: true,
        keyStep,
      }));
    } else {
      setstateTitle((prev: any) => ({
        ...prev,
        success: "0",
        id: data.id,
        submit: true,
        keyStep,
      }));
    }
  };

  const handleNextQuestion = () => {
    handleSaveData(stateTitle);
  };
  const trans = useTrans();
  return (
    <>
      <div className="mx-auto my-[4rem] mb-[2rem] md:mb-[4rem]">
        <FillWord
          className="fade-up-animation"
          keyGameTyping={filteredKeyGameTyping}
          setPlaceholders={setPlaceholders}
          placeholders={placeholders}
          data={data}
          setstateTitle={setstateTitle}
          stateTitle={stateTitle}
          handleplaceholders={handleplaceholders}
          handleNextQuestion={handleNextQuestion}
        />
      </div>
      <div className="mt-0 flex flex-col items-center justify-center md:mt-[2rem]">
        {stateTitle.submit ? (
          <Button
            size="large"
            type="primary"
            className="rounded-4"
            style={{ display: "block", height: "46px" }}
            onClick={() => handleSaveData(stateTitle)}
          >
            <span className="leading-16">
              {trans["common.button.continue"]}
            </span>
          </Button>
        ) : (
          <Button
            size="large"
            type="primary"
            className="rounded-4"
            style={{ display: "block", height: "46px" }}
            onClick={() => handleplaceholders()}
            disabled={placeholders?.length !== filteredKeyGameTyping.length}
          >
            <span className="leading-16">{trans["common.button.check"]}</span>
          </Button>
        )}
        {stateTitle.success === "1" ? (
          <div style={{ marginTop: "300px" }}>
            <BeforeSuccess data={data} />
          </div>
        ) : (
          stateTitle.success === "0" && (
            <div style={{ marginTop: "300px" }}>
              <BeforeFail data={data} />
            </div>
          )
        )}
      </div>
    </>
  );
};
export default CpnFillWords;
