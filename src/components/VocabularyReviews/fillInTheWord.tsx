/* eslint-disable no-nested-ternary */
import { message } from "antd";
import React, { useEffect, useRef, useState } from "react";

import { hideHighlightWord } from "@/common/convert-string-learn/string-learn";
import * as callAPI from "@/common/services/apiReviewNoteBook";
import useTrans from "@/utils/userTrans";

import ModalFalse from "../../common/modal/beforeFail";
import ModalSuccess from "../../common/modal/beforeSuccess";

const FillInTheWork = ({
  data,
  handleSaveData,
  showResults,
  setShowResults,
  keyStep,
}: any) => {
  const [correctItem, setCorrectItem] = useState("");
  const [choosenItem, setChoosenItem] = useState("");
  const [result, setResult] = useState("");
  const [newArr, setnewArr] = useState([]);
  const divRef: any = useRef(null);
  const [nextgames, setNextGames] = useState<any>(0);

  const [isSpeaking, setIsSpeaking] = useState(false);
  const utteranceRef = useRef<any>(null);
  const audioTune = new Audio(
    `${process.env.NEXT_PUBLIC_API_AUDIO}${data?.audioSentence}`
    // "http://codeskulptor-demos.commondatastorage.googleapis.com/pang/pop.mp3"
  );
  const playSound = () => {
    // setAudioTunes();
    // audioTune.playbackRate = 1;
    audioTune.play();
  };
  useEffect(() => {
    const convertData = [
      {
        id: 1,
        name: data?.fillInTheBlankOne,
      },
      {
        id: 2,
        name: data?.fillInTheBlankTwo,
      },
      {
        id: 3,
        name: data?.fillInTheBlankThree,
      },
      {
        id: 4,
        name: data?.fillInTheBlankFour,
        // name: data?.word,
      },
    ];

    const newArrs: any = convertData.sort(() => Math.random() - 0.5);

    setnewArr(newArrs);
  }, [data?.fillInTheBlankOne]);

  const handleSubmit = async () => {
    if (choosenItem !== "") {
      const selectedAnswer: any = newArr.find(
        (item: any) => item.name === choosenItem
      );

      const dataRule = {
        gameType: 3, // Game số 3
        vocabularyLevel: data?.vocabularyLevel || 1,
        correctAnswer: choosenItem,
      };

      const response = await callAPI.ruleReview(dataRule);

      if (response?.message === "SUCCESSFULL") {
        if (selectedAnswer.name.toLowerCase() === data.word.toLowerCase()) {
          setShowResults((prevState: any) => ({
            ...prevState,
            id: data.id,
            submit: true,
            success: "1",
            keyStep,
          }));
          setCorrectItem(choosenItem);
        } else if (
          data?.correctAnswer
            ?.toLowerCase()
            ?.includes(selectedAnswer.name?.toLowerCase()?.trim()) ||
          data?.sentence
            ?.toLowerCase()
            ?.includes(selectedAnswer.name?.toLowerCase()?.trim()) ||
          response?.data === true
        ) {
          setShowResults((prevState: any) => ({
            ...prevState,
            id: data.id,
            submit: true,
            success: "1",
            keyStep,
          }));
          setCorrectItem(choosenItem);
        } else {
          let getCheckerTrue: any;
          if (data?.correctAnswer !== "") {
            getCheckerTrue = newArr.find((item: any) =>
              data?.correctAnswer
                ?.toLowerCase()
                ?.includes(item.name?.toLowerCase()?.trim())
            );
          } else {
            getCheckerTrue = newArr.find(
              (item: any) => item.name.toLowerCase() === data.word.toLowerCase()
            );
          }
          setResult(selectedAnswer?.name);
          setCorrectItem(getCheckerTrue?.name);
          setShowResults((prevState: any) => ({
            ...prevState,
            id: data.id,
            submit: true,
            success: "0",
            keyStep,
          }));
        }
      } else {
        message.error("Lỗi server:", response?.message);
      }
    }
  };

  const changeQuestion = (e: string) => {
    setChoosenItem(e);
    setResult("");
  };

  const handleNextStepChild = () => {
    setChoosenItem("");
    setCorrectItem("");
    setResult("");
    handleSaveData(showResults);
  };

  const ForgotQuestion = () => {
    if (data.correctAnswer !== "") {
      setCorrectItem(data.correctAnswer);
    } else {
      setCorrectItem(data.word);
    }
    setShowResults((prevState: any) => ({
      ...prevState,
      id: data.id,
      submit: true,
      success: "1",
      keyStep,
    }));
  };
  // const HandleSpeak = (rate: any, word: any) => {
  //   if (isSpeaking && utteranceRef.current) {
  //     utteranceRef.current.onend = null;
  //     utteranceRef.current.onerror = null;
  //     window.speechSynthesis.cancel();
  //     setIsSpeaking(false);
  //     // eslint-disable-next-line @typescript-eslint/no-use-before-define
  //     speech(rate, word);
  //     return;
  //   }

  //   // eslint-disable-next-line @typescript-eslint/no-use-before-define
  //   speech(rate, word);
  // };

  // const speech = (rate: any, word: any) => {
  //   const utterance = new SpeechSynthesisUtterance(word);
  //   setIsSpeaking(true);
  //   utteranceRef.current = utterance;
  //   utterance.rate = rate;

  //   utterance.onend = () => {
  //     setIsSpeaking(false);
  //   };
  //   utterance.onerror = () => {
  //     setIsSpeaking(false);
  //   };

  //   window.speechSynthesis.speak(utterance);
  // };

  const handleKeyDown = (event: any) => {
    if (event.key === "Enter") {
      if (nextgames === 0) {
        handleSubmit();
        setNextGames(1);
      } else {
        handleNextStepChild();
        setNextGames(0);
      }
    }
  };
  const trans = useTrans();

  return (
    <>
      <div className="wrapper-item-cnt-render-bg">
        <h1>{trans["reviseWord.title.fillTheWord"]}</h1>
        <div className="question-ctn-render-vocabulary-reviews">
          <div className="bg-question-child-header">
            <img src="/assets/images/img-boy-question.png" alt="img question" />
            <div className="info-box">
              <img
                src="/assets/icons/ic-speak-english.png"
                alt="icon speak"
                style={{ cursor: "pointer" }}
                onClick={() => playSound()}
              />
              <p>
                {/* {selectedItem
                  ? highlightWords(data?.sentence, data?.word, selectedItem)
                  : hideHighlightWord(data?.sentence, data?.word)} */}
                {hideHighlightWord(
                  data?.sentence,
                  data?.correctAnswer || data?.word || ""
                )}
                {/* {translate
              ? highlightWord(data?.meanSentence, data?.meaning)
              : highlightWord(
                  data?.sentence,
                  data?.correctAnswer || data?.word || ""
                )} */}
              </p>
            </div>
          </div>
          <div
            className="bg-item-choose-child-main"
            ref={divRef}
            tabIndex={0}
            onKeyDown={handleKeyDown}
          >
            {newArr.length > 0 &&
              newArr.map((key: any, value) => {
                return (
                  <div
                    key={value}
                    className={`choose-items ${
                      correctItem === key?.name
                        ? "active"
                        : choosenItem === key?.name
                        ? "choosen"
                        : ""
                    } ${result === key?.name ? "false" : ""}`}
                    onClick={() => {
                      if (!showResults?.submit) {
                        changeQuestion(key?.name);
                      }
                    }}
                    // onKeyDown={(e: any) => {
                    //   // eslint-disable-next-line @typescript-eslint/no-unused-expressions
                    //   e.keyCode === 13 && handleSubmit();
                    // }}
                    // tabIndex={0}
                  >
                    <span>{key?.name}</span>
                    {correctItem === key?.name ? (
                      <img src="/assets/icons/ic_check.svg" alt="icon check" />
                    ) : result === key?.name ? (
                      <img src="/assets/icons/ic_false.svg" alt="icon false" />
                    ) : choosenItem === key?.name ? (
                      <img
                        src="/assets/icons/ic-choosen.svg"
                        alt="icon choosen"
                      />
                    ) : (
                      <img
                        src="/assets/icons/ic-no-tick.svg"
                        alt="icon no tick"
                      />
                    )}
                  </div>
                );
              })}
          </div>
        </div>
      </div>
      {showResults?.submit ? (
        <div className="mt-0 flex flex-col items-center justify-center md:mt-[2rem] ">
          <button
            className="button-submit-in-Vocabulary-Reviews z-10"
            onClick={() => handleNextStepChild()}
          >
            {trans["common.button.continue"]}
          </button>
        </div>
      ) : (
        <div className="mt-0 flex flex-col items-center justify-center md:mt-[2rem]">
          <button
            className="button-submit-in-Vocabulary-Reviews "
            disabled={!choosenItem}
            onClick={() => handleSubmit()}
          >
            {trans["common.button.check"]}
          </button>
          <div className="not-remember" onClick={() => ForgotQuestion()}>
            {trans["common.button.rememberedWords"]}
          </div>
        </div>
      )}
      {showResults?.success === "1" ? (
        <ModalSuccess data={data} />
      ) : (
        showResults?.success === "0" && <ModalFalse data={data} />
      )}
    </>
  );
};

export default FillInTheWork;
