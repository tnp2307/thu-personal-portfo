import Link from "next/link";
import React, { useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";

import Ic_Linkin from "@/public/assets/icons/footer-social-linkin.svg";
import Ic_Insta from "@/public/assets/icons/footer-social-insta.svg";
import Ic_Behance from "@/public/assets/icons/footer-social-behance.svg";

import { FooterRequest } from "@/store/page/actions";
import { getPageSelector } from "@/store/page/selectors";
import useTrans from "@/utils/userTrans";

const Footer = () => {
  const dataFooter = useSelector(getPageSelector);

  const trans = useTrans();
  const useWindowDimensions = () => {
    const hasWindow = typeof window !== "undefined";

    function getWindowDimensions() {
      return {
        width: hasWindow ? window.innerWidth : null,
        height: hasWindow ? window.innerHeight : null,
      };
    }

    const [windowDimensions, setWindowDimensions] = useState<{
      width: number | null;
      height: number | null;
    }>({
      width: null,
      height: null,
    });

    const handleResize = () => {
      setWindowDimensions(getWindowDimensions());
    };

    // eslint-disable-next-line consistent-return
    useEffect(() => {
      if (hasWindow) {
        handleResize();
        window.addEventListener("resize", handleResize);
        return () => window.removeEventListener("resize", handleResize);
      }
    }, [hasWindow]);

    return windowDimensions;
  };
  const { width } = useWindowDimensions();
  const breakpointMD = 550;
  return (
    <footer className="main-footer">
      <div className="container">
        {width && width >= breakpointMD ? (
          <div className="flex justify-between items-start">
            <div className="footer-infor">
              <div className="">
                <p>Available for</p>
                <span>new projects</span>
              </div>
              <ul className="flex gap-60 justify-start">
                <li>Home</li>
                <li>About</li>
                <li>Works</li>
                <li>MyCV</li>
              </ul>
            </div>
            <div className="footer-contact">
              <button>
                <a target="_blank" href="https://cal.com/thuphansone/call-me">
                  Schedule a call
                </a>
              </button>
              <div className="footer-social">
                <div>
                  <Ic_Linkin />
                </div>
                <div>
                  <Ic_Insta />
                </div>
                <div>
                  <Ic_Behance />
                </div>
              </div>
            </div>
          </div>
        ) : (
          <div></div>
        )}
      </div>
    </footer>
  );
};

export default Footer;
