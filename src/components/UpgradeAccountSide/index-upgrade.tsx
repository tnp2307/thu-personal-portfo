import { useRouter } from "next/router";
import { useState } from "react";

import UpgradeAccount from "@/common/popup/popup_Upgrade";
import IcPiece from "@/public/assets/icons/ic-white-piece.svg";
import { localStorageHelpers } from "@/utils/localStorage";
import useTrans from "@/utils/userTrans";

const PageUpgradeAccountSide = () => {
  const [open, setOpen] = useState(false);
  const router = useRouter();
  const trans = useTrans();
  const accessToken = localStorageHelpers.getToken();
  const hanldeEventLogin = () => {
    if (accessToken) {
      setOpen(!open);
    } else {
      router.push("/#upgrade-form");
    }
  };
  return (
    <>
      <div className="relative mt-[15vh] w-[238px] animate-pulse">
        <div className="rounded-[2rem] bg-white p-[1rem] py-[2rem]  text-center text-18 font-medium text-[#EC3237]">
          <q>A different language is a different vision of life.</q>
          <h3>{trans["formUpgrade.quote"]}</h3>
          <h4 className="pt-30 text-16 font-light">~~ Federico Fellini ~~</h4>
        </div>
        {/* <IcSharkSupriced /> */}
        <div className="absolute -bottom-30 left-20">
          <IcPiece />
        </div>
        <img
          src="/assets/icons/ic-shark-supriced.gif"
          alt="gif"
          style={{ width: "80%", height: "80%" }}
          className="-left-90 absolute -bottom-48"
        />
      </div>
      {open && <UpgradeAccount setOpen={setOpen} open={open} />}
    </>
  );
};

export default PageUpgradeAccountSide;
