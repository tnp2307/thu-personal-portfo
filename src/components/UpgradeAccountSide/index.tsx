import { Button } from "antd";
import { useRouter } from "next/router";
import { useState } from "react";

import UpgradeAccount from "@/common/popup/popup_Upgrade";
import { localStorageHelpers } from "@/utils/localStorage";
import useTrans from "@/utils/userTrans";

const UpgradeAccountSide = () => {
  const [open, setOpen] = useState(false);
  const router = useRouter();
  const trans = useTrans();
  const accessToken = localStorageHelpers.getToken();
  const hanldeEventLogin = () => {
    if (accessToken) {
      setOpen(!open);
    } else {
      router.push("/#upgrade-form");
    }
  };
  return (
    <>
      <div className="mt-[15vh] animate-pulse">
        <div className="rounded-[2rem] bg-white p-[1.5rem]">
          <span className="text-18 font-bold text-black-600">
            {trans["learn.side.upgrade"]}
          </span>
          <h2 className="py-12 text-[1.6rem] font-bold text-secondary">
            {trans["learn.side.upgradeword"]}
          </h2>
          <Button
            className="w-full break-words p-1"
            size="large"
            type="primary"
            onClick={() => hanldeEventLogin()}
          >
            {trans["common.button.upgradeNow"]}
          </Button>
        </div>
        {/* <IcSharkSupriced /> */}
        <img
          src="/assets/icons/ic-shark-supriced.gif"
          alt="gif"
          style={{ width: "80%", height: "80%" }}
        />
      </div>
      {open && <UpgradeAccount setOpen={setOpen} open={open} />}
    </>
  );
};

export default UpgradeAccountSide;
