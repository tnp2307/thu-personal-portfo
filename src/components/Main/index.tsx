import type { ReactNode } from "react";

import Footer from "../Footer";
import Menu from "../Menu";
// eslint-disable-next-line import/no-named-as-default
import MenuHeader from "../MenuHeader";

type IMainProps = {
  meta: ReactNode;
  children: ReactNode;
  noContainer?: boolean;
  noHeader?: boolean;
  noFooter?: boolean;
  id?: string;
  headerLanding?: boolean;
};

const Main = (props: IMainProps) => (
  <div className="text-black-400 relative" id={props?.id}>
    {props.meta}

    <div className={`mx-auto ${!props?.noContainer && "container"}`}>
      {!props.noHeader && (
        <>{props.headerLanding ? <MenuHeader /> : <Menu />}</>
      )}

      <main className="content lg:text-xl">{props.children}</main>
      {!props.noFooter && <Footer />}
    </div>
  </div>
);

export { Main };
