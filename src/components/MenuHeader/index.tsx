import Image from "next/image";
import Link from "next/link";
import { useRouter } from "next/router";
import { useState } from "react";
import OutsideClickHandler from "react-outside-click-handler";

import IcBurger from "@/public/assets/icons/ic-menu.svg";
import IconHome from "@/public/assets/icons/ic_home_page.svg";
import IconHomeSelected from "@/public/assets/icons/ic_home_page_select.svg";
import LogoImage from "@/public/assets/images/logo.png";
import Logo from "@/public/assets/images/logo.svg";

type NavItemProps = {
  path: string;
  name: string;
};
const NavItem = (props: NavItemProps) => {
  const router = useRouter();
  return (
    <li>
      {props.name === "home" ? (
        <Link
          href={props.path}
          className={`${
            (props.path !== "/" && router.pathname.includes(props.path)) ||
            (props.path === "/" && router.pathname === "/")
              ? "nav-active"
              : "nav-unactive"
          } rounded-[1rem] py-8 px12 `}
        >
          {(props.path !== "/" && router.pathname.includes(props.path)) ||
          (props.path === "/" && router.pathname === "/") ? (
            <IconHomeSelected />
          ) : (
            <IconHome />
          )}
        </Link>
      ) : (
        <Link
          href={props.path}
          className={`${
            (props.path !== "/" && router.pathname.includes(props.path)) ||
            (props.path === "/" && router.pathname === "/")
              ? "nav-active"
              : "nav-unactive"
          } rounded-[1rem] py-8 px12 `}
        >
          {props.name}
        </Link>
      )}
    </li>
  );
};
const NavItemVertical = (props: NavItemProps) => {
  const router = useRouter();
  return (
    <li className="px-7 py-3 ">
      {props.name === "home" ? (
        <Link
          href={props.path}
          className={`${
            (props.path !== "/" && router.pathname.includes(props.path)) ||
            (props.path === "/" && router.pathname === "/")
              ? "nav-active"
              : "nav-unactive"
          } rounded-[1rem] py-8 px12 `}
        >
          {(props.path !== "/" && router.pathname.includes(props.path)) ||
          (props.path === "/" && router.pathname === "/") ? (
            <IconHomeSelected />
          ) : (
            <IconHome />
          )}
        </Link>
      ) : (
        <Link
          href={props.path}
          className={`${
            (props.path !== "/" && router.pathname.includes(props.path)) ||
            (props.path === "/" && router.pathname === "/")
              ? "nav-active"
              : "nav-unactive"
          } rounded-[1rem] py-8 px12 `}
        >
          {props.name}
        </Link>
      )}
    </li>
  );
};

// eslint-disable-next-line unused-imports/no-unused-vars
const MenuHeader = () => {
  const menuList = [
    { name: `home`, path: "/" },
    { name: `Work`, path: "/work" },
    { name: `About`, path: "/about" },
    { name: `Contact`, path: "/contact" },
  ];

  const [openMenu, setOpenMenu] = useState(false);
  const router = useRouter();
  const directHomePage = () => {
    router.push("/");
  };

  const [isSticky, setIsSticky] = useState(false);
  window.onscroll = () => {
    addSticky();
  };
  let addSticky = () => {
    if (window.pageYOffset > 100) {
      setIsSticky(true);
    } else {
      setIsSticky(false);
    }
  };

  return (
    <header
      className={`inset-x-0 top-0 z-50 mx-auto   ${
        isSticky
          ? " sticky transition-opacity"
          : " flex items-center h-[96px]  bg-[#ffffff] lg:h-[96px]"
      }`}
    >
      <div
        className={` container mx-auto    ${
          isSticky
            ? "pt-[1.5625rem]"
            : " pt-[2.1875rem] grid-cols-3 items-center gap-4 hidden lg:grid"
        }`}
      >
        {isSticky ? null : (
          <div
            className="flex cursor-pointer items-center justify-start mb-16"
            onClick={directHomePage}
          >
            <img
              src="/assets/images/logo.png"
              alt="img"
              className="w-[10rem]"
            />
          </div>
        )}

        <div
          className={`flex h-full items-center justify-center ${
            isSticky ? " mb-11 " : ""
          } `}
        >
          <ul
            className={`px-[1.5rem] py-2 rounded-[4.375rem] bg-[#F2F2F2] flex gap-[24px] ${
              isSticky ? "shadow-sm" : ""
            }`}
          >
            {menuList.map((item, index) => (
              <NavItem key={index} path={item.path} name={item.name} />
            ))}
          </ul>
        </div>
        {isSticky ? null : (
          <div className="flex items-center justify-end mb-16">
            <div className="mr-16 header-text">
              <span className="">Availabe for</span>
              <br />
              <span className="">New Project</span>
            </div>
            <img
              src="/assets/icons/20-arrow-down.svg"
              alt="ICON"
              className="w-[2.25rem]"
            />
          </div>
        )}
      </div>
      <div className="relative flex w-full items-center justify-between px-16 lg:hidden">
        <div
          className={`fixed left-0 top-0 z-50 flex h-screen w-full justify-start transition-all duration-700 lg:hidden ${
            openMenu ? "translate-x-0" : "-translate-x-full"
          }`}
        >
          <OutsideClickHandler
            onOutsideClick={() => {
              setOpenMenu(false);
            }}
            display="inline"
          >
            <div className="flex h-full w-screen-60 flex-col justify-between bg-secondary pb-26 pt-[0px] md:w-[30vh]">
              <div className="pt-[32px]">
                <ul>
                  {menuList.map((item, index) => (
                    <NavItemVertical
                      key={index}
                      path={item.path}
                      name={item.name}
                    />
                  ))}
                </ul>
              </div>
            </div>
          </OutsideClickHandler>
        </div>
        <button onClick={() => setOpenMenu(true)}>
          <IcBurger className="h-[32px] w-[32px]" />
        </button>
        <Link href="/">
          <Image src={LogoImage} alt="LogoImage" className="h-auto w-[128px]" />
        </Link>
        <div className="container mx-auto hidden h-full grid-cols-12 gap-4 lg:grid">
          <div
            className="col-span-2 flex cursor-pointer items-center"
            onClick={directHomePage}
          >
            <Logo key="desktop" />
          </div>
          <div className="col-span-8 flex h-full items-center">
            <ul className="mb-0 grid h-full w-full grid-cols-3 gap-[24px]">
              {menuList.map((item, index) => (
                <NavItem key={index} path={item.path} name={item.name} />
              ))}
            </ul>
          </div>
        </div>
      </div>
    </header>
  );
};

export default MenuHeader;
