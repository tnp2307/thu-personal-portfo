import { DownOutlined } from "@ant-design/icons";
import type { MenuProps } from "antd";
import { Button, Dropdown, Space } from "antd";
import Image from "next/image";
import Link from "next/link";
import { useRouter } from "next/router";
import IconCM from "public/assets/icons/circle-flag-of-cambodia.png";
import IconLA from "public/assets/icons/circle-flag-of-laos.png";
import IconCN from "public/assets/icons/vecteezy_circle-flag-of-china.png";
import IconVN from "public/assets/icons/vietnam-flat-rounded-flag.png";

import IconTick from "@/public/assets/icons/true-tick.svg";
import Logo from "@/public/assets/images/logo.svg";
import useTrans from "@/utils/userTrans";

type NavItemProps = {
  path: string;
  name: string;
};
const NavItem = (props: NavItemProps) => {
  const router = useRouter();
  return (
    <li>
      <Link
        href={props.path}
        className={`${
          router.pathname === props.path
            ? "font-bold text-primary"
            : "text-gray-500"
        } text-18`}
      >
        {props.name}
      </Link>
    </li>
  );
};

const menuList = [
  { name: "Về Best-English", path: "/" },
  // { name: "Về Bestenglish", path: "/123" },
  { name: "Tổng hợp ngữ pháp", path: "/blog" },
];

// eslint-disable-next-line unused-imports/no-unused-vars
const MenuHeaderMobile = () => {
  const { locale } = useRouter();
  const items: MenuProps["items"] = [
    {
      label: (
        <Link href="/" locale="vi">
          <div className=" flex w-52 justify-between">
            <div>
              <Image
                className="mr-2 inline-block"
                height={24}
                src={IconVN}
                alt="vn"
              />
              <span>Vietnamese</span>
            </div>
            <div className={locale === "vi" ? "block" : "hidden"}>
              <IconTick />
            </div>
          </div>
        </Link>
      ),
      key: "0",
    },
    {
      label: (
        <Link href="/" locale="cn">
          <div className=" flex w-52 justify-between">
            <div>
              <Image
                className="mr-2 inline-block"
                height={24}
                src={IconCN}
                alt="cn"
              />
              <span>Chinese</span>
            </div>
            <div className={locale === "cn" ? "block" : "hidden"}>
              <IconTick />
            </div>
          </div>
        </Link>
      ),
      key: "1",
    },
    {
      label: (
        <Link href="/" locale="cm">
          <div className=" flex w-52 justify-between">
            <div>
              <Image
                className="mr-2 inline-block"
                height={24}
                src={IconCM}
                alt="cm"
              />
              <span>Cambodia</span>
            </div>
            <div className={locale === "cm" ? "block" : "hidden"}>
              <IconTick />
            </div>
          </div>
        </Link>
      ),
      key: "2",
    },
    {
      label: (
        <Link href="/" locale="la">
          <div className=" flex w-52 justify-between">
            <div>
              <Image
                className="mr-2 inline-block"
                height={24}
                src={IconLA}
                alt="la"
              />
              <span>Laos</span>
            </div>
            <div className={locale === "la" ? "block" : "hidden"}>
              <IconTick />
            </div>
          </div>
        </Link>
      ),
      key: "3",
    },
  ];
  const trans = useTrans();
  const renderNationIcon = () => {
    switch (locale) {
      case "cn":
        return <Image height={24} src={IconCN} alt="cn" />;
      case "vi":
        return <Image height={24} src={IconVN} alt="cn" />;
      case "cm":
        return <Image height={24} src={IconCM} alt="cn" />;
      case "la":
        return <Image height={24} src={IconLA} alt="cn" />;
      default:
        break;
    }
    return <Image height={24} src={IconVN} alt="vn" />;
  };
  return (
    <header
      className="sticky inset-x-0 top-0 z-50 mx-auto flex items-center bg-[#ffffff]"
      style={{ height: "6rem" }}
    >
      <div className="container mx-auto grid grid-cols-3 items-center gap-4">
        <div className="flex h-full items-center">
          <ul className="class-uppercase mb-0 flex gap-8">
            {menuList.map((item, index) => (
              <NavItem key={index} path={item.path} name={item.name} />
            ))}
          </ul>
        </div>
        <div className="flex items-center justify-center">
          <Logo />
        </div>
        <div className="flex items-center justify-end">
          <div
            className="dropdown-language flex items-center border"
            style={{
              marginRight: "12px",
              height: "3.5rem",
              borderRadius: "8px",
              border: "1px solid #EAEAEA",
              padding: "12px 14px",
            }}
          >
            <Dropdown
              overlayClassName="dropdown-language-list"
              menu={{ items }}
              trigger={["click"]}
              disabled={true}
            >
              <a onClick={(e) => e.preventDefault()}>
                <Space>
                  {renderNationIcon()}
                  <DownOutlined />
                </Space>
              </a>
            </Dropdown>
          </div>
          <Button
            type="primary"
            size="large"
            style={{
              height: "3.5rem",
              minWidth: "10.5rem",
              fontSize: "1.125rem",
            }}
          >
            {trans["common.button.trialNow"]}
          </Button>
        </div>
      </div>
    </header>
  );
};

export default MenuHeaderMobile;
