import IcArrow from "@/public/assets/icons/ic-arrow-diag.svg";
import { useRouter } from "next/router";
import { useState } from "react";

// eslint-disable-next-line unused-imports/no-unused-vars

interface ProjectItemProps {
  size?: string;
  img?: string;
  url?: string;
}
const ProjectItem = (props: ProjectItemProps) => {
  const router = useRouter();
  const { size, img, url } = props;
  const handleUrl = (url: string | undefined) => {
    router.push(url ? url : "/");
  };
  return (
    <div className={`${size} main-box`}>
      <div className="b1">
        <img src={img} alt="" />
      </div>
      <div className="b2">
        <button
          onClick={() => {
            handleUrl(url);
          }}
          className="diag-arrow"
        >
          <IcArrow />
        </button>
      </div>
    </div>
  );
};

export default ProjectItem;
