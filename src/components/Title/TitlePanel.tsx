import * as React from "react";

export interface ITitlePanelProps {
  title: string;
  subTitle: string;
}

export default function TitlePanel(props: ITitlePanelProps) {
  const { title, subTitle } = props;
  return (
    <h1 className="mb-[5rem] flex flex-row gap-24 pb-[2rem] border-b border-b-gray-500">
      <span className="flex items-start text-[2.5rem] h-24 text-black-0 font-bold">
        {title}
      </span>
      <div className="flex flex-row gap-24 translate-y-2 font-normal text-[1.5rem] text-black-0">
        <span className="flex items-center">/</span>
        <span className="flex items-end">{subTitle}</span>
      </div>
    </h1>
  );
}
