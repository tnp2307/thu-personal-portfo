import { useRouter } from "next/router";
import React from "react";

import IcArrowRight from "@/public/assets/icons/ic-arrow-right.svg";
import Img from "@/public/assets/images/img-unit.png";

const UnitList = (props: any) => {
  const dataLesson = props?.lesson;
  const router = useRouter();
  return (
    <ul className="grid gap-20 md:grid-cols-2 md:gap-24">
      {dataLesson?.length > 0 &&
        dataLesson?.map((value: any, index: number) => {
          return (
            <li
              key={index}
              className="unit-item course-item flex items-center justify-between 
        rounded-12 border border-blue-secondary px-12 py-16 transition-all 
        duration-200 hover:border-green-300"
              style={{ cursor: "pointer" }}
              onClick={() => router.push(`/learn-new-words/${value.id}`)}
            >
              <div className="flex items-center">
                <img
                  className="img-circle"
                  src={
                    value?.imagesLesson
                      ? // eslint-disable-next-line no-unsafe-optional-chaining
                        process.env.NEXT_PUBLIC_API_IMAGES + value?.imagesLesson
                      : Img
                  }
                  alt=""
                />
                <div className="ml-8">
                  <p className="mb-0 text-18 font-bold leading-25.6 text-black-600">
                    {value?.name}
                  </p>
                  <p className="mb-0 mt-4 text-16 leading-25.6">
                    {value?.description}
                  </p>
                </div>
              </div>
              <IcArrowRight className="ic-arrow" />
            </li>
          );
        })}
    </ul>
  );
};

export default UnitList;
