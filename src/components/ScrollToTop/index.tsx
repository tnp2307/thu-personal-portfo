import React from "react";

import IcToTop from "@/public/assets/icons/ic-to-top.svg";

export default function ScrollToTop() {
  return (
    <div
      onClick={() => {
        window.scrollTo({
          top: 0,
          left: 0,
          behavior: "smooth",
        });
      }}
      className=" flex h-[48px] w-[48px] items-center justify-center scroll-smooth rounded-full bg-[#74CDD9] shadow-md transition-all ease-in-out hover:opacity-80"
    >
      <IcToTop />
    </div>
  );
}
