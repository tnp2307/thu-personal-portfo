import { Avatar } from "antd";
import Image from "next/image";
import Link from "next/link";
import { useRouter } from "next/router";
import type { ReactNode } from "react";
import { useEffect, useState } from "react";
import OutsideClickHandler from "react-outside-click-handler";
import { useSelector } from "react-redux";

import ExpiredAccount from "@/containers/ctn-expired-account";
import IcLearn from "@/public/assets/icons/ic-learn-1.svg";
import IcLogin from "@/public/assets/icons/ic-log-in.svg";
import IcLogout from "@/public/assets/icons/ic-log-out.svg";
import IcMenu from "@/public/assets/icons/ic-menu.svg";
import IcNews from "@/public/assets/icons/ic-news.svg";
import IcNote from "@/public/assets/icons/ic-note-1.svg";
import IcRevise from "@/public/assets/icons/ic-revise-1.svg";
import IcUnlogged from "@/public/assets/icons/ic-unlogged.svg";
// import LogoHorizontal from "@/public/assets/images/logo-horizontal.svg";
import LogoImage from "@/public/assets/images/logo.png";
import Logo from "@/public/assets/images/logo.svg";
import { getProfileSelector } from "@/store/auth/selectors";
import { logout } from "@/utils/helpers";
import { localStorageHelpers } from "@/utils/localStorage";
import useTrans from "@/utils/userTrans";

import MenuAvatarCustom from "../MenuAvatarCustom";
import ModalAddCode from "../ModalAddCode";

type NavItemProps = {
  path: string;
  name: string;
  icon: ReactNode;
};
type NavItemVerticalProps = {
  path: string;
  name: string;
};
const handleOutput = (input: any) => {
  if (input.includes("/")) {
    const index = input.lastIndexOf("/");

    let output = input.substring(0, index + 1);

    output = output.replace(/\/$/, "");

    return output;
  }
  return input;
};

const NavItem = (props: NavItemProps) => {
  const router = useRouter();
  const newQuery =
    handleOutput(router.pathname) === ""
      ? router.pathname
      : handleOutput(router.pathname);
  return (
    <li
      className={`relative flex flex-col items-center justify-center gap-4 ${
        // eslint-disable-next-line no-nested-ternary
        newQuery === props.path
          ? // "border-b-4 border-primary"
            "NavItem-custom-border"
          : newQuery === "/review-not-login" && props.path === "/revise"
          ? // "border-b-4 border-primary"
            "NavItem-custom-border"
          : ""
      }`}
    >
      <Link
        href={props.path}
        className={`${
          // eslint-disable-next-line no-nested-ternary
          newQuery === props.path
            ? "NavItem-custom font-bold text-primary"
            : newQuery === "/review-not-login" && props.path === "/revise"
            ? "NavItem-custom font-bold text-primary"
            : "NavItem-custom-active text-gray-500"
        } flex flex-col items-center justify-center gap-4 text-18`}
      >
        <div>{props.icon}</div>
        <span>{props.name}</span>
        <div
          className={`${
            // eslint-disable-next-line no-nested-ternary
            newQuery === props.path
              ? "line_navItem"
              : newQuery === "/review-not-login" && props.path === "/revise"
              ? "line_navItem"
              : ""
          }`}
        ></div>
      </Link>
    </li>
  );
};
const NavItemVertical = (props: NavItemVerticalProps) => {
  const router = useRouter();
  const newQuery =
    handleOutput(router.pathname) === ""
      ? router.pathname
      : handleOutput(router.pathname);
  return (
    <li
      className={`relative mb-3 flex flex-col items-start pl-5 ${
        // eslint-disable-next-line no-nested-ternary
        newQuery === props.path
          ? // "border-b-4 border-primary"
            "NavItem-custom-border"
          : newQuery === "/review-not-login" && props.path === "/revise"
          ? // "border-b-4 border-primary"
            "NavItem-custom-border"
          : ""
      }`}
    >
      <Link
        href={props.path}
        className={`${
          // eslint-disable-next-line no-nested-ternary
          newQuery === props.path
            ? "NavItem-custom font-bold text-primary"
            : newQuery === "/review-not-login" && props.path === "/revise"
            ? "NavItem-custom font-bold text-primary"
            : "NavItem-custom-active text-white"
        } flex flex-col items-center justify-center gap-4 text-18`}
      >
        <span>{props.name}</span>
      </Link>
    </li>
  );
};
// eslint-disable-next-line unused-imports/no-unused-vars
const Menu = () => {
  const profileInfo = useSelector(getProfileSelector);
  const accessToken = localStorageHelpers.getToken();
  const router = useRouter();
  const [isOpenMenu, setIsOpenMenu] = useState<boolean>(false);
  const [isOpenAddCode, setIsOpenAddCode] = useState(false);
  const [linkPage, setLinkPage] = useState("/");
  const trans = useTrans();
  const directHomePage = () => {
    if (accessToken) {
      router.push("/review-not-login");
    } else {
      router.push("/");
    }
  };

  const menuList = [
    { name: `${trans["header.revise"]}`, path: linkPage, icon: <IcRevise /> },
    {
      name: `${trans["header.learnNewWord.title"]}`,
      path: "/courses",
      icon: <IcLearn />,
    },
    {
      name: `${trans["header.notebook.title"]}`,
      path: "/note",
      icon: <IcNote />,
    },
    {
      name: `${trans["header.grammaBlog"]}`,
      path: "/study-guide",
      icon: <IcNews />,
    },
  ];
  useEffect(() => {
    if (accessToken) {
      setLinkPage("/revise");
    } else {
      setLinkPage("/review-not-login");
    }
  }, []);
  const hanldeEventLogin = () => {
    router.push("/login");
  };

  return (
    <div>
      <ExpiredAccount />
      <header
        className="auto relative inset-x-0 top-0 z-50 mx-auto flex items-center bg-[#ffffff] py-12 "
        id="menu-main"
      >
        <div className="container mx-auto grid h-full grid-cols-12 gap-4 px-8 md:hidden">
          <div className="col-span-4 flex items-center justify-start">
            <button onClick={() => setIsOpenMenu(true)} className="p-12">
              <IcMenu />
            </button>
          </div>
          <div
            className="col-span-8 flex cursor-pointer items-center justify-end"
            onClick={directHomePage}
          >
            <Image
              src={LogoImage}
              alt="LogoImage"
              className="h-auto w-[8rem]"
            />
          </div>
        </div>
        <div
          className={`fixed right-0 top-0 flex h-screen w-full justify-start transition-all duration-700 md:hidden ${
            isOpenMenu ? "translate-x-0" : "-translate-x-full"
          }`}
        >
          <OutsideClickHandler
            onOutsideClick={() => {
              setIsOpenMenu(false);
            }}
            display="inline"
          >
            <div className="flex h-full w-screen-60 flex-col justify-between bg-secondary pb-26 pt-0">
              <div className="pt-[2rem]">
                <ul>
                  {menuList.map((item, index) => (
                    <NavItemVertical
                      key={index}
                      path={item.path}
                      name={item.name}
                    />
                  ))}
                </ul>
              </div>
              <div className="mb-[46px] w-full">
                <Link
                  href={"/profile"}
                  className="flex items-center justify-center py-18"
                >
                  <Avatar
                    src={
                      profileInfo?.photoUrl
                        ? // eslint-disable-next-line no-unsafe-optional-chaining
                          `${process.env.NEXT_PUBLIC_API_IMAGES}${profileInfo?.photoUrl}`
                        : "/assets/icons/Default-Shark-happy.jpg"
                    }
                    icon={<IcUnlogged className="icon-default" />}
                    // className={`avatar-custom ${props.src && "avatar-logged"}`}
                  />
                  <span className="ml-18 font-bold text-white">
                    {profileInfo?.name || "Guest"}
                  </span>
                </Link>
                {profileInfo ? (
                  <button
                    className="flex w-full cursor-pointer items-center justify-center border-t-2 border-white py-18 hover:opacity-70"
                    onClick={logout}
                  >
                    <IcLogout />
                    <p className="mb-0 ml-12 text-white">
                      {trans["common.button.logout"]}
                    </p>
                  </button>
                ) : (
                  <button
                    className="flex w-full cursor-pointer items-center justify-center border-t-2 border-white py-18 hover:opacity-70"
                    onClick={hanldeEventLogin}
                  >
                    <IcLogin />
                    <p className="mb-0 ml-12 text-white">
                      {trans["common.button.login"]}
                    </p>
                  </button>
                )}
              </div>
            </div>
          </OutsideClickHandler>
        </div>
        <div className="container mx-auto hidden h-full grid-cols-12 gap-4 md:grid">
          <div
            className="col-span-3 flex cursor-pointer items-center lg:col-span-2"
            onClick={directHomePage}
          >
            <Logo />
          </div>
          <div className="col-span-6 flex h-full items-center lg:col-span-8">
            <ul className="mb-0 grid h-full w-full grid-cols-4 gap-[1.5rem]">
              {menuList.map((item, index) => (
                <NavItem
                  key={index}
                  path={item.path}
                  name={item.name}
                  icon={item.icon}
                />
              ))}
            </ul>
          </div>
          <MenuAvatarCustom src="https://zos.alipayobjects.com/rmsportal/ODTLcjxAfvqbxHnVXCYX.png" />
          <ModalAddCode
            isOpen={isOpenAddCode}
            onClose={() => setIsOpenAddCode(false)}
          />
        </div>
      </header>
    </div>
  );
};

export default Menu;
