import axios from "axios";
// eslint-disable-next-line import/no-extraneous-dependencies
import moment from "moment";
import { useEffect, useState } from "react";
import { useSelector } from "react-redux";

import { getProfileSelector } from "@/store/auth/selectors";
import { localStorageHelpers } from "@/utils/localStorage";
import useTrans from "@/utils/userTrans";

function ExpiredAccount() {
  const [setState, getState] = useState({
    name: "",
    title: "",
  });
  const profile = useSelector(getProfileSelector);
  const a: any = moment(new Date());
  const b: any = moment(profile?.expiredDate);
  const as: any = new Date(a);
  const bs: any = new Date(b);
  const timeDiff = Math.abs(as.getTime() - bs.getTime());
  const diffDays = Math.ceil(timeDiff / (1000 * 3600 * 24));
  const trans = useTrans();
  const sendMail = async () => {
    const token = localStorageHelpers.getToken();
    await axios({
      method: "post",
      url: `${process.env.NEXT_PUBLIC_API_ENDPOINT}/Dashbroads/SendEmailNotify`,
      data: {
        template: 3,
        toName: profile?.name,
        toEmail: profile?.email,
        dateExpire: moment(profile?.expiredDate).format("DD-MM-YYYY"),
      },
      headers: {
        accept: "application/json",
        Authorization: `Bearer ${token}`,
      },
    })
      .then((response) => {
        localStorage.setItem("mailService", "true");
      })
      .catch((err) => {
        // handle error
      });
  };
  const [openHighlight, setOpenHighlight] = useState(true);
  useEffect(() => {
    if (bs.getTime() < as.getTime()) {
      getState({
        name: `${trans["popup.expired.noti.name"]}`,
        title: `${trans["popup.expired.noti.title"]}`,
      });
    } else if (
      moment(profile?.expiredDate).format("YYYYMMDD") >
        moment(new Date()).format("YYYYMMDD") &&
      diffDays < 7
    ) {
      getState({
        name: `${trans["popup.expired.warning"]} ${diffDays} ${trans["common.label.day"]}`,
        title: "",
      });
    } else if (diffDays === 7 && as.getYear() === bs.getYear()) {
      getState({
        name: `${trans["popup.expired.warning"]} 7 ${trans["common.label.day"]}`,
        title: "",
      });
      if (localStorage.getItem("mailService") !== "true") {
        sendMail();
      }
    } else if (diffDays === 0 && as.getYear() === bs.getYear() && profile) {
      getState({
        name: `${trans["popup.expired.notiToday"]}`,
        title: `${trans["popup.expired.noti.title"]}`,
      });
    } else if (diffDays > 7) {
      localStorage.setItem("mailService", "false");
    }
  }, [profile]);
  return (
    <>
      {setState?.name && openHighlight && (
        <div className="box-expired-account relative w-full">
          <div>
            <span>{setState?.name}!!</span> <span>{setState?.title}</span>
          </div>
          <button
            className="absolute right-5"
            onClick={() => setOpenHighlight(false)}
          >
            X
          </button>
        </div>
      )}
    </>
  );
}

export default ExpiredAccount;
