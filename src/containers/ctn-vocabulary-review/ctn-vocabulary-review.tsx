// react plugin used to create charts
import { Empty } from "antd";
import { useRouter } from "next/router";
// eslint-disable-next-line import/no-extraneous-dependencies
import queryString from "query-string";
import { useState } from "react";

import useTrans from "@/utils/userTrans";

import IndexVocabulary from "./ctn-index";

const CtnVocabularyReviews = ({ data, type }: any) => {
  const [start, setStart] = useState(true);
  const router = useRouter();

  // eslint-disable-next-line no-restricted-globals
  const values = queryString.parse(location.search);

  const getSearch = values.success;

  const handleClick = () => {
    router.push({
      pathname: "/vocabulary-review",
      query: { success: true },
    });
    // eslint-disable-next-line no-lone-blocks
    {
      // eslint-disable-next-line @typescript-eslint/no-unused-expressions
      start === true && setStart(false);
    }
    // eslint-disable-next-line @typescript-eslint/no-use-before-define
  };
  const trans = useTrans();
  return (
    <section
      className="bg-img-primary-opacity min-h-screen"
      id="learn-new-word-page"
    >
      {" "}
      {start && getSearch !== "true" ? (
        <>
          <div className="wrapper-src-ctn-vocabulary-reviews">
            <div className="bg-ctn-vocabulary-reviews">
              <div className="header-ctn-vocabulary-review">
                <img
                  src="/assets/images/vocabulary-review.png"
                  alt="logo vocabulary review"
                />
                <div className="bg-txt-header-main">
                  <p className="title-main__header">
                    {data?.length} {trans["learn.revise.wordInNote"]}
                  </p>
                  <p className="title-main__child">
                    {trans["learn.revise.reviseNowP"]}
                  </p>
                </div>
              </div>
              <div className="content-ctn-vocabulary-review">
                {data.length > 0 ? (
                  data.map((key: any, index: number) => {
                    return (
                      <div className="child-item-vocabulary-review" key={index}>
                        {key.isReview ? (
                          <img
                            src="/assets/icons/ic_check.svg"
                            alt="icon check"
                          />
                        ) : (
                          <img
                            src="/assets/icons/ic_false.svg"
                            alt="icon false"
                          />
                        )}
                        <div className="bg-item">
                          <div className="bg-english-txt left">
                            <p>{key.word}</p>
                            <p className="txt__description">
                              / {key.spelling} /
                            </p>
                          </div>
                          <p className="txt__noun">{key.from}</p>
                          <div className="bg-english-txt right">
                            <p>{key.meaning}</p>
                          </div>
                        </div>
                      </div>
                    );
                  })
                ) : (
                  <Empty
                    description={`${trans["learn.revise.noData"]} ${type}`}
                  />
                )}
              </div>
              <div className="footer-ctn-vocabulary-review">
                {data.length > 0 ? (
                  <div className="btn-vocabulary" onClick={() => handleClick()}>
                    {trans["common.button.reviseNow"]}
                  </div>
                ) : (
                  <div className="btn-vocabulary disabled">
                    {trans["common.button.reviseNow"]}
                  </div>
                )}
              </div>
            </div>
          </div>
        </>
      ) : (
        <>
          <IndexVocabulary />
        </>
      )}
    </section>
  );
};
export default CtnVocabularyReviews;
