// react plugin used to create charts
import React, { useEffect, useState } from "react";

import ChooseTheMeaning from "@/components/VocabularyReviews/ChooseTheMeaning";
import FillInTheWork from "@/components/VocabularyReviews/fillInTheWord";
// import FillWords from "@/components/VocabularyReviews/FillWord";
// import ListenAndRewrite from "@/components/LearnNewWords/ListenAndRewrite";
import ListenAndRewrite from "@/components/VocabularyReviews/ListenAndRewrite";
import ReReadVocabulary from "@/components/VocabularyReviews/ReReadVocabulary";
import RewriteInEnglish from "@/components/VocabularyReviews/RewriteInEnglish";
import RewriteInVietnamese from "@/components/VocabularyReviews/RewriteInVietnamese";

// import RewriteInEnglish from "@/components/VocabularyReviews/RewriteInEnglish";

const RenderContentStep = ({
  data,
  nextStep,
  getBindAPIData,
  setProgressReview,
}: any) => {
  // eslint-disable-next-line no-param-reassign
  const [saveData, getSaveData] = useState<any>({
    id: "",
    step: 999,
    isMemorized: true,
    data: [],
  });
  const [showResults, setShowResults] = useState({
    title: "",
    success: "",
    submit: false,
    id: "",
    keyStep: 0,
    count: 0,
  });

  const handleSaveData = (body: any) => {
    const index = saveData.data.findIndex(
      (item: any) => item?.step === body.keyStep
    );

    if (index === -1) {
      if (body.success === "1") {
        // eslint-disable-next-line @typescript-eslint/no-use-before-define
        saveStateUnits(true, body);
      } else {
        // eslint-disable-next-line @typescript-eslint/no-use-before-define
        saveStateUnits(false, body);
      }
    } else if (body.success === "1") {
      getSaveData((prev: any) => ({
        ...prev,
        step: prev.step + 1,
      }));
    }

    // eslint-disable-next-line @typescript-eslint/no-use-before-define
    reloadState();
  };

  const customNumberData = () => {
    const numberStep: any = data?.currentData?.vocabularyLevel || 0;
    let numberData = 999;

    switch (numberStep) {
      case 1:
      case 7:
      case 6:
        numberData = 2;
        break;
      case 3:
        numberData = 3;
        break;
      case 4:
        numberData = 5;
        break;
      case 5:
      case 2:
        numberData = 7;
        break;
      default:
        numberData = 999;
        break;
    }

    return numberData;
  };

  const handleSaveDataStepLevel = (body: any) => {
    const index = saveData.data.findIndex(
      (item: any) => item?.step === body.keyStep
    );

    if (index === -1) {
      if (body.success === "1") {
        setProgressReview((prev: any) => prev + 1);
        // eslint-disable-next-line @typescript-eslint/no-use-before-define
        saveStateUnits2(true, body);
      } else {
        // eslint-disable-next-line @typescript-eslint/no-use-before-define
        saveStateUnits2(false, body);
      }
    } else if (body.success === "1") {
      setProgressReview((prev: any) => prev + 1);
      getSaveData((prev: any) => ({
        ...prev,
        step: customNumberData(),
      }));
    }

    // eslint-disable-next-line @typescript-eslint/no-use-before-define
    reloadState();
  };

  const endUnitStep = (body: any) => {
    if (body.success === "0") {
      handleSaveData(body);
    } else {
      const { isMemorized, id } = saveData;
      if (isMemorized === true && body.success === "1") {
        getBindAPIData((prev: any) => [
          ...prev,
          { id: id || body.id, isMemorized: true },
        ]);
      } else {
        getBindAPIData((prev: any) => [
          ...prev,
          { id: id || body.id, isMemorized: false },
        ]);
      }
      setProgressReview((prev: any) => prev + 1);
      getSaveData({
        id: "",
        step: 999,
        isMemorized: true,
        data: [],
      });
      // eslint-disable-next-line @typescript-eslint/no-use-before-define
      reloadState();
      nextStep();
    }
  };

  const reloadState = () => {
    setShowResults({
      title: "",
      success: "",
      submit: false,
      id: "",
      keyStep: 0,
      count: 0,
    });
  };

  const saveStateUnits = (checker: boolean, body: any) => {
    if (checker) {
      getSaveData((prev: any) => ({
        ...prev,
        id: body.id,
        step: prev.step + 1,
        isMemorized:
          body.success === "1" ? prev.isMemorized === true && true : false,
        data: [
          ...prev.data,
          {
            id: body.id,
            isMemorized: body.success === "1",
            step: body.keyStep,
          },
        ],
      }));
    } else {
      getSaveData((prev: any) => ({
        ...prev,
        id: body.id,
        isMemorized:
          body.success === "1" ? prev.isMemorized === true && true : false,
        data: [
          ...prev.data,
          {
            id: body.id,
            isMemorized: body.success === "1",
            step: body.keyStep,
          },
        ],
      }));
    }
  };

  const saveStateUnits2 = (checker: boolean, body: any) => {
    if (checker) {
      getSaveData((prev: any) => ({
        ...prev,
        id: body.id,
        step: customNumberData(),
        isMemorized:
          body.success === "1" ? prev.isMemorized === true && true : false,
        data: [
          ...prev.data,
          {
            id: body.id,
            isMemorized: body.success === "1",
            step: body.keyStep,
          },
        ],
      }));
    } else {
      getSaveData((prev: any) => ({
        ...prev,
        id: body.id,
        isMemorized:
          body.success === "1" ? prev.isMemorized === true && true : false,
        data: [
          ...prev.data,
          {
            id: body.id,
            isMemorized: body.success === "1",
            step: body.keyStep,
          },
        ],
      }));
    }
  };

  useEffect(() => {
    if (data?.currentData?.id) {
      getSaveData((prev: any) => {
        let step = 5;
        if (data?.currentData?.vocabularyLevel === 5) {
          step = 6;
        } else if (data?.currentData?.vocabularyLevel === 2) {
          step = 8;
        } else if (data?.currentData?.vocabularyLevel === 4) {
          step = 1;
        }
        return {
          ...prev,
          step,
        };
      });
    }
  }, [data]);
  switch (saveData.step) {
    case 0: // Điền từ vào chỗ trống
      return (
        <div className="wrapper-background-render-step">
          <FillInTheWork
            data={data.currentData}
            handleSaveData={endUnitStep}
            setShowResults={setShowResults}
            showResults={showResults}
            keyStep={saveData.step}
          />
        </div>
      );

    case 1: // Chọn nghĩa của từ được gạch chân
      return (
        <>
          <div className="wrapper-background-render-step">
            <ChooseTheMeaning
              className="fade-up-animation"
              data={data.currentData}
              handleSaveData={handleSaveDataStepLevel}
              setShowResults={setShowResults}
              showResults={showResults}
              keyStep={saveData.step}
            />
          </div>
        </>
      );

    case 2: // Đọc lại từ vựng
      return (
        <>
          <div className="wrapper-background-render-step">
            <ReReadVocabulary
              className="fade-up-animation"
              data={data.currentData}
              endUnitStep={endUnitStep}
              setstateTitle={setShowResults}
              stateTitle={showResults}
              keyStep={saveData.step}
            />
          </div>
        </>
      );
    case 3: // viết lại bằng tiếng anh
      return (
        <>
          <div className="wrapper-background-render-step">
            <RewriteInEnglish
              className="fade-up-animation"
              data={data.currentData}
              handleSaveData={endUnitStep}
              setShowResults={setShowResults}
              showResults={showResults}
              keyStep={saveData.step}
            />
          </div>
        </>
      );
    case 4: // viết lại bằng tiếng việt
      return (
        <>
          <div className="wrapper-background-render-step">
            <RewriteInVietnamese
              className="fade-up-animation"
              data={data.currentData}
              handleSaveData={endUnitStep}
              setShowResults={setShowResults}
              showResults={showResults}
              keyStep={saveData.step}
            />
          </div>
        </>
      );
    case 5: // Nghe và viết lại
      return (
        <>
          <div className="wrapper-background-render-step">
            <ListenAndRewrite
              className="fade-up-animation"
              data={data.currentData}
              handleSaveData={handleSaveDataStepLevel}
              setShowResults={setShowResults}
              showResults={showResults}
              keyStep={saveData.step}
            />
          </div>
        </>
      );

    case 6: // viết lại bằng tiếng việt ( Next case)
      return (
        <>
          <div className="wrapper-background-render-step">
            <RewriteInVietnamese
              className="fade-up-animation"
              data={data.currentData}
              handleSaveData={handleSaveDataStepLevel}
              setShowResults={setShowResults}
              showResults={showResults}
              keyStep={saveData.step}
            />
          </div>
        </>
      );

    case 7: // Nghe và viết lại ( Next từ vựng)
      return (
        <>
          <div className="wrapper-background-render-step">
            <ListenAndRewrite
              className="fade-up-animation"
              data={data.currentData}
              handleSaveData={endUnitStep}
              setShowResults={setShowResults}
              showResults={showResults}
              keyStep={saveData.step}
            />
          </div>
        </>
      );

    case 8: // Điền từ vào chỗ trống ( Next case)
      return (
        <div className="wrapper-background-render-step">
          <FillInTheWork
            data={data.currentData}
            handleSaveData={handleSaveDataStepLevel}
            setShowResults={setShowResults}
            showResults={showResults}
            keyStep={saveData.step}
          />
        </div>
      );
    default:
      return <></>;
  }
};

export default RenderContentStep;
