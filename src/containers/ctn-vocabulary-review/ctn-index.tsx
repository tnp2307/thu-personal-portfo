import { Button, notification } from "antd";
import Link from "next/link";
import { useRouter } from "next/router";
import { useEffect, useState } from "react";

import ClosePopup from "@/common/popup/popupCloseUnit2";
import * as callAPINoti from "@/common/services/apilearnnewworks";
import * as callAPI from "@/common/services/apiReviewNoteBook";
import CpnFinishReviews from "@/components/FinishLessions/CpnFinishReviews";
import FinishLesson from "@/components/FinishLesson/FinishLessons";
import ProgressBox from "@/components/ProgressBox";
import IcClose from "@/public/assets/icons/ic-close.svg";
import useTrans from "@/utils/userTrans";

import RenderContentStep from "./ctn-render-content-step";

type NotificationType = "success" | "info" | "warning" | "error";

const CtnIndexVocabularyReviews = () => {
  const [dataReview, setDataReview] = useState<any>({
    currentStep: 0,
    currentData: {},
    dataReview: [],
    bindStep: 0,
  });

  const [progressReview, setProgressReview] = useState<number>(0);
  const trans = useTrans();
  const [close, setClose] = useState<boolean>(false);
  const [bindAPIData, getBindAPIData] = useState<any>([]);
  const router = useRouter();

  useEffect(() => {
    // eslint-disable-next-line @typescript-eslint/no-use-before-define
    getDataReview();
  }, []);
  useEffect(() => {
    if (
      dataReview?.dataReview?.length === bindAPIData?.length &&
      dataReview?.dataReview?.length > 0
    ) {
      setDataReview((prev: any) => ({
        ...prev,
        bindStep: 1,
      }));
    }
  }, [bindAPIData]);

  const getDataReview = async () => {
    const response = await callAPI.resDataReview();
    if (response.message === "SUCCESSFULL") {
      const resdata = response.data;
      if (resdata?.length > 0) {
        setDataReview((prev: any) => ({
          ...prev,
          dataReview: resdata,
          currentData: resdata[prev.currentStep],
        }));
      } else {
        // eslint-disable-next-line @typescript-eslint/no-use-before-define
        openNotificationWithIcon("error");
        router.push("/note");
      }
    } else {
      router.back();
    }
  };

  const nextStep = () => {
    setDataReview((prev: any) => ({
      ...prev,
      currentStep: prev.currentStep + 1,
      currentData: prev.dataReview[prev.currentStep + 1],
    }));
  };

  const openNotificationWithIcon = (type: NotificationType) => {
    notification[type]({
      message: `${trans["common.label.notification"]}`,
      description: `${trans["profile.vocab.notification"]}`,
    });
  };

  const bindApiResponse = async () => {
    const output = dataReview?.dataReview
      .map((item: any) => {
        const matchedItem = bindAPIData.find((x: any) => x.id === item.id);
        if (matchedItem) {
          return {
            id: matchedItem.id,
            isReview: matchedItem.isMemorized,
            vocabularyLevel: item.vocabularyLevel,
          };
          // eslint-disable-next-line no-else-return
        } else {
          return null;
        }
      })
      .filter(Boolean);

    const convertData = output?.map((item: any) => {
      if (item?.isReview && item?.vocabularyLevel < 7) {
        return {
          ...item,
          // eslint-disable-next-line no-unsafe-optional-chaining
          vocabularyLevel: item?.vocabularyLevel + 1,
        };
      }
      return item;
    });

    const output1 = {
      lstSchedule: convertData,
    };
    await callAPINoti.resBindNotifi(output1);
  };

  const ResAPIReview = async () => {
    const response = await callAPI.pushDataReview(bindAPIData);
    if (response.message === "SUCCESSFULL") {
      bindApiResponse();
      setDataReview((prev: any) => ({
        ...prev,
        bindStep: 2,
      }));
    } else {
      // eslint-disable-next-line @typescript-eslint/no-use-before-define
      openNotificationWithIcons("error");
    }
  };

  const openNotificationWithIcons = (type: NotificationType) => {
    notification[type]({
      message: `${trans["learn.containerIndex.noti"]}`,
      description: `${trans["learn.containerIndex.alreadyLearn"]}`,
    });
  };
  const openNotificationWithIcons2 = (type: NotificationType) => {
    notification[type]({
      message: `${trans["learn.containerIndex.noti"]}`,
      description: `${trans["learn.containerIndex.saveResult"]}`,
    });
  };

  const Close = async () => {
    const response = await callAPI.pushDataReview(bindAPIData);
    if (response.message === "SUCCESSFULL") {
      bindApiResponse();
      openNotificationWithIcons2("success");
    } else {
      // eslint-disable-next-line @typescript-eslint/no-use-before-define
      openNotificationWithIcons("error");
    }
  };

  return (
    <>
      {dataReview.bindStep === 0 && (
        <div className="container mx-auto grid grid-cols-12">
          <div className="col-span-2">
            <section className="sticky-upgrate-account ml:mt-[3.5rem] sticky top-32 mt-[3rem] hidden w-[95%] lg:block">
              {/* <UpgradeAccountSide /> */}
            </section>
          </div>
          <div className="relative col-span-12 min-h-screen bg-[#F6F6F6] p-[1rem] py-[2rem] lg:col-span-8">
            {progressReview !== dataReview.dataReview.length * 2 && (
              <div className="mx-auto flex w-[82%] items-center justify-between gap-[2rem] md:w-3/4">
                <button onClick={() => setClose(true)}>
                  <IcClose />
                </button>
                <ProgressBox
                  value={`${
                    progressReview * (100 / (dataReview.dataReview.length * 2))
                  }`}
                />
              </div>
            )}

            {/* {renderContentStep({handleChangeToNextStep, handleReset, step})} */}
            <RenderContentStep
              data={dataReview}
              nextStep={nextStep}
              getBindAPIData={getBindAPIData}
              setProgressReview={setProgressReview}
            />
          </div>
          <div className="col-span-2">
            <div className="fixed bottom-[20vh] right-[3rem] ml-[3rem] mt-[3rem] hidden flex-col gap-[1rem] pb-[1rem] md:flex">
              {/* <SideInfoQuestion /> */}
            </div>
          </div>
        </div>
      )}

      {dataReview.bindStep === 1 && (
        <section
          className="bg-img-primary-opacity min-h-screen"
          id="learn-new-word-page"
        >
          <div className="container mx-auto grid grid-cols-12">
            <div className="col-span-2">
              <section className="sticky-upgrate-account ml:mt-[3.5rem] sticky top-32 mt-[3rem] hidden w-[95%] lg:block">
                {/* <UpgradeAccountSide /> */}
              </section>
            </div>
            <div className="relative col-span-12 min-h-screen bg-[#F6F6F6] p-[1rem] py-[2rem] lg:col-span-8">
              <CpnFinishReviews
                bindAPIData={bindAPIData}
                dataReview={dataReview.dataReview}
                ResAPIReview={ResAPIReview}
              />
            </div>
            <div className="col-span-2">
              <div className="fixed bottom-[20vh] right-[3rem] ml-[3rem] mt-[3rem] hidden flex-col gap-[1rem] pb-[1rem] md:flex">
                {/* <SideInfoQuestion /> */}
              </div>
            </div>
          </div>
        </section>
      )}

      {dataReview.bindStep === 2 && (
        <section
          className="bg-img-primary-opacity min-h-screen"
          id="learn-new-word-page"
        >
          <div className="container mx-auto grid grid-cols-12">
            <div className="col-span-2">
              <section className="sticky-upgrate-account ml:mt-[3.5rem] sticky top-32 mt-[3rem] hidden w-[95%] lg:block">
                {/* <UpgradeAccountSide /> */}
              </section>
            </div>
            <div className="relative col-span-12 min-h-screen bg-[#F6F6F6] p-[1rem] py-[2rem] lg:col-span-8">
              <div className="mx-auto my-[4rem] w-[80%]" key={6}>
                <FinishLesson className="fade-up-animation" datingAPI={5} />
              </div>
              <Link href={"/courses"}>
                <Button
                  type="primary"
                  size="large"
                  style={{ display: "block", width: "40%", height: "46px" }}
                  className="mx-auto my-[1rem]"
                >
                  <Link href={"/courses"}>
                    {trans["common.button.learnNewWord"]}
                  </Link>
                </Button>
              </Link>
              <Link href={"/revise"}>
                <Button
                  size="large"
                  className="mx-auto"
                  style={{
                    display: "block",
                    width: "40%",
                    background: "transparent",
                    color: "#ec3237",
                    borderColor: "#ec3237",
                    height: "46px",
                  }}
                  // onClick={() => handleNextStep()}
                >
                  <Link href={"/revise"}>
                    {trans["common.button.backToRevise"]}
                  </Link>
                </Button>
              </Link>
            </div>
            <div className="col-span-2">
              <div className="fixed bottom-[20vh] right-[3rem] ml-[3rem] mt-[3rem] hidden flex-col gap-[1rem] pb-[1rem] md:flex">
                {/* <SideInfoQuestion /> */}
              </div>
            </div>
          </div>
        </section>
      )}
      {close && <ClosePopup setClose={setClose} handleEvent={Close} />}
    </>
  );
};
export default CtnIndexVocabularyReviews;
