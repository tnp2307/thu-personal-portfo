// react plugin used to create charts
import { Button, message } from "antd";
import Link from "next/link";
import { useRouter } from "next/router";
import { useEffect, useMemo, useState } from "react";
import { useSelector } from "react-redux";

import ClosePopup from "@/common/popup/popupCloseUnit";
import * as callAPI from "@/common/services/apilearnnewworks";
import ComponentLearnNewWord from "@/components/CpnLearnNewWords/CpnLearnNewWords";
import ProgressBox from "@/components/ProgressBox";
import IcClose from "@/public/assets/icons/ic-close.svg";
import { getProfileSelector } from "@/store/auth/selectors";
import useTrans from "@/utils/userTrans";

import CpnFinishLearnNewWork from "../../components/FinishLessions/CpnFinishLearnNewWork";
import FinishLesson from "../../components/FinishLesson";

const ContainerLearnNewWords = ({ data, idQuery }: any) => {
  const [currentStep, setCurrentStep] = useState<number>(0);
  const [currentData, setCurrentData] = useState<any>();
  const [saveQuesionUnit, setSaveQuesionUnit] = useState<any>([]);
  const [bindAPI, setBindAPI] = useState<any>({
    step: 0,
    data: [],
    saveDatareview: [],
    totalDays: 0,
  });
  const trans = useTrans();
  const [progressBar, setProgressBar] = useState<number>(0);

  const [countLengh, setCountLengh] = useState<number>(0);
  const [countLenght, setCountLenght] = useState<number>(0);

  const [close, setClose] = useState<boolean>(false);

  const router = useRouter();

  const key = "updatable_key";

  const profile: any = useSelector(getProfileSelector);

  useEffect(() => {
    setCurrentData(data[currentStep]);
  }, [currentStep, data]);

  useEffect(() => {
    setCountLengh(countLengh + 1);
  }, [countLenght]);

  useEffect(() => {
    if (saveQuesionUnit.length > 0 && saveQuesionUnit.length === data.length) {
      const b = [];
      const a = [];

      // eslint-disable-next-line no-plusplus
      for (let i = 0; i < saveQuesionUnit.length; i++) {
        if (saveQuesionUnit[i].success) {
          b.push(saveQuesionUnit[i]);
        } else {
          a.push(saveQuesionUnit[i]);
        }
      }

      if (a.length > 0) {
        setCurrentStep(a[0].currentStep);
        // setCountLenght(countLenght + 1);
        setCountLengh(countLengh + 1);
      } else if (b.length === data.length) {
        // eslint-disable-next-line @typescript-eslint/no-shadow
        const mergeData = (b: any, data: any) => {
          return b.map((item1: any) => {
            const item2 = data.find((item: any) => item.id === item1.id);
            return { ...item1, ...item2 };
          });
        };

        const data3 = mergeData(b, data);

        const result = data3.map((item: any) => ({
          isMemorized:
            item.isReview === true ? true : item.isMemorized === true,
          id: item.id,
          isReview: item.isReview,
        }));

        const isReviews = data3.map((item: any) => ({
          isMemorized: item.isMemorized,
          id: item.id,
          isReview: item.isReview,
        }));

        // eslint-disable-next-line @typescript-eslint/no-use-before-define
        resAPILearnNewWords(result, isReviews, data3);
      }
    }
  }, [saveQuesionUnit]);

  const onNextStep = () => {
    if (
      currentStep === data.length - 1 ||
      saveQuesionUnit.length === data.length
    ) {
      const a = [];
      const b = [];

      // eslint-disable-next-line no-plusplus
      for (let i = 0; i < saveQuesionUnit.length; i++) {
        if (saveQuesionUnit[i].success) {
          b.push(saveQuesionUnit[i]);
        } else {
          a.push(saveQuesionUnit[i]);
        }
      }

      if (a.length > 0) {
        setCurrentStep(a[0].currentStep);
      }
    } else if (currentStep < data.length - 1) {
      setCurrentStep(currentStep + 1);
    }
  };

  const onSaveLessonUnit = (props: any) => {
    const filteredState = props.data.reduce((acc: any, current: any) => {
      const existing = acc.find(
        (item: any) =>
          item.id === current.id && item.stepChild === current.stepChild
      );

      if (!existing) {
        return [...acc, current];
      }

      if (existing.success && !current.success) {
        return [...acc.filter((item: any) => item !== existing), current];
      }

      return acc;
    }, []);
    const updatedProps = { ...props, data: filteredState };

    const result = saveQuesionUnit.some(
      (item: any) => item.id === updatedProps.id
    );

    if (result) {
      const getData: any = saveQuesionUnit.find(
        (item: any) => item.id === updatedProps.id
      );

      const newdata: any = props?.data[0];

      // eslint-disable-next-line no-plusplus
      const index = getData.data.findIndex(
        (item: any) => item.stepChild === newdata.stepChild
      );
      if (index !== -1) {
        getData.data[index] = newdata;
      }

      const allSuccess = getData?.data.every(
        (item: any) => item.success === true
      );

      const updatedPropz = { ...getData, success: allSuccess };

      setSaveQuesionUnit((prevState: any) => {
        const sortedArray = [
          ...prevState.filter((x: any) => x.id !== updatedPropz.id),
          updatedPropz,
        ];
        sortedArray.sort((a, b) => a.currentStep - b.currentStep);
        return sortedArray;
      });
      onNextStep();
    } else {
      setSaveQuesionUnit((prevState: any) => [
        ...prevState.filter((x: any) => x.id !== updatedProps.id),
        updatedProps,
      ]);
      onNextStep();
    }
  };

  const onStepLessonUnit = (props: any, curStep: number) => {
    const updatedProps = {
      currentStep: curStep,
      success: true,
      isMemorized: true,
      isReview: false,
      id: props.id,
      data: [],
    };
    setSaveQuesionUnit((prevState: any) => [
      ...prevState.filter((x: any) => x.id !== updatedProps.id),
      updatedProps,
    ]);
    setProgressBar((prev: any) => prev + 4);

    onNextStep();
  };

  const resAPILearnNewWords = async (req: any, isReviews: any, data3: any) => {
    const response = await callAPI.resBindData(req, idQuery);
    if (response.message === "SUCCESSFULL") {
      setBindAPI((prev: any) => ({
        ...prev,
        step: 1,
        data: data3,
        saveDatareview: data3,
        totalDays: response?.data?.totalDays || 0,
      }));
    } else if (response.success === false) {
      if (response.message === "USER_STUDIED_LESSON") {
        message.error({
          content: `${trans["learnNewWord.alreadyLearned"]}`,
          key,
          duration: 7,
        });
      } else if (response.message === "ACCOUNT_EXPIRES") {
        message.error({
          content: `${trans["popup.expired.noti.name"]}`,
          key,
          duration: 7,
        });
      } else if (response.message === "USER_STUDIED_UNIT") {
        message.error({
          content: `${trans["learnNewWord.alreadyLearned.short"]}`,
          key,
          duration: 7,
        });
      } else {
        message.error({
          content: response.message,
          key,
          duration: 7,
        });
      }
      router.push("/courses");
    }
  };

  const ResAPIReview = async () => {
    const getDataReview = bindAPI.saveDatareview;

    const result = getDataReview.map((item: any) => ({
      id: item.id,
      isReview: !item.isMemorized,
    }));

    const response = await callAPI.resSaveHandBook(result);
    if (response.message === "SUCCESSFULL") {
      const respon = result?.filter((item: any) => item?.isReview === true);

      const outputs = respon?.map((item: any) => ({
        ...item,
        vocabularyLevel: 1,
      }));

      const output = {
        lstSchedule: outputs,
      };

      await callAPI.resBindNotifi(output);
      setBindAPI((prev: any) => ({
        ...prev,
        step: 2,
      }));
    } else if (response.success === false) {
      message.error({
        content: response.message,
        key,
      });
    }
  };
  const startTime: Date = useMemo(() => new Date(), []);

  return (
    <>
      {bindAPI.step === 0 && (
        <section
          className="bg-img-primary-opacity min-h-screen"
          id="learn-new-word-page"
        >
          <div className="container mx-auto grid grid-cols-12">
            <div className="col-span-2">
              <section className="sticky-upgrate-account ml:mt-[3.5rem] sticky top-32 mt-[3rem] hidden w-[95%] lg:block">
                {/* <UpgradeAccountSide /> */}
              </section>
            </div>
            <div className="relative col-span-12 flex min-h-screen flex-col bg-[#F6F6F6] pt-[2rem] lg:col-span-8">
              {progressBar < data.length * 4 + 100 && (
                <div className="mx-auto flex w-[82%] items-center justify-between gap-[2rem] md:w-3/4">
                  <button onClick={() => setClose(true)}>
                    <IcClose />
                  </button>
                  <ProgressBox
                    value={`${progressBar * (100 / (data.length * 4))}`}
                  />
                </div>
              )}

              {/* {renderContentStep()} */}
              {data?.length > 0 && (
                <ComponentLearnNewWord
                  data={currentData}
                  onNextStep={onNextStep}
                  currentStep={currentStep}
                  onSaveLessonUnit={onSaveLessonUnit}
                  onStepLessonUnit={onStepLessonUnit}
                  saveQuesionUnit={saveQuesionUnit}
                  countLengh={countLengh}
                  setProgressBar={setProgressBar}
                />
              )}
            </div>
            <div className="col-span-2">
              <div className="fixed bottom-[20vh] right-[3rem] ml-[3rem] mt-[3rem] hidden flex-col gap-[1rem] pb-[1rem] md:flex">
                {/* <SideInfoQuestion /> */}
              </div>
            </div>
          </div>
        </section>
      )}

      {bindAPI.step === 1 && (
        <section
          className="bg-img-primary-opacity min-h-screen"
          id="learn-new-word-page"
        >
          <div className="container mx-auto grid grid-cols-12">
            <div className="col-span-2">
              <section className="sticky-upgrate-account ml:mt-[3.5rem] sticky top-32 mt-[3rem] hidden w-[95%] lg:block">
                {/* <UpgradeAccountSide /> */}
              </section>
            </div>
            <div className="relative col-span-12 min-h-screen bg-[#F6F6F6] p-[1rem] py-[2rem] lg:col-span-8">
              <CpnFinishLearnNewWork
                startTime={startTime}
                idQuery={idQuery}
                profileID={profile.id}
                data={bindAPI.saveDatareview}
                setBindAPI={setBindAPI}
                ResAPIReview={ResAPIReview}
              />
            </div>
            <div className="col-span-2">
              <div className="fixed bottom-[20vh] right-[3rem] ml-[3rem] mt-[3rem] hidden flex-col gap-[1rem] pb-[1rem] md:flex">
                {/* <SideInfoQuestion /> */}
              </div>
            </div>
          </div>
        </section>
      )}

      {bindAPI.step === 2 && (
        <section
          className="bg-img-primary-opacity min-h-screen"
          id="learn-new-word-page"
        >
          <div className="container mx-auto grid grid-cols-12">
            <div className="col-span-2">
              <section className="sticky-upgrate-account ml:mt-[3.5rem] sticky top-32 mt-[3rem] hidden w-[95%] lg:block">
                {/* <UpgradeAccountSide /> */}
              </section>
            </div>
            <div className="relative col-span-12 min-h-screen bg-[#F6F6F6] p-[1rem] py-[2rem] lg:col-span-8">
              <div className="mx-auto my-[4rem] w-[80%]" key={6}>
                <FinishLesson
                  className="fade-up-animation"
                  datingAPI={bindAPI}
                  profile={profile}
                  saveQuesionUnit={saveQuesionUnit}
                />
              </div>
              <Link href={"/courses"}>
                <Button
                  type="primary"
                  size="large"
                  style={{ display: "block", width: "40%", height: "46px" }}
                  className="mx-auto my-[1rem]"
                >
                  <Link href={"/courses"}>
                    {trans["common.button.learnNewWord"]}
                  </Link>
                </Button>
              </Link>
              <Link href={"/revise"}>
                <Button
                  size="large"
                  className="mx-auto"
                  style={{
                    display: "block",
                    width: "40%",
                    background: "transparent",
                    color: "#ec3237",
                    borderColor: "#ec3237",
                    height: "46px",
                  }}
                  // onClick={() => handleNextStep()}
                >
                  <Link href={"/revise"}>
                    {trans["common.button.backToRevise"]}
                  </Link>
                </Button>
              </Link>
            </div>
            <div className="col-span-2">
              <div className="fixed bottom-[20vh] right-[3rem] ml-[3rem] mt-[3rem] hidden flex-col gap-[1rem] pb-[1rem] md:flex">
                {/* <SideInfoQuestion /> */}
              </div>
            </div>
          </div>
        </section>
      )}

      {close && <ClosePopup idQuery={idQuery} setClose={setClose} />}
    </>
  );
};
export default ContainerLearnNewWords;
