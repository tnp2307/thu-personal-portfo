// eslint-disable-next-line import/no-extraneous-dependencies
import "../styles/global.css";
import "antd/dist/reset.css";
import "../styles/app.scss";
import "react-spring-bottom-sheet/dist/style.css";
// eslint-disable-next-line import/no-extraneous-dependencies
import "babel-polyfill";
// eslint-disable-next-line import/no-extraneous-dependencies
import "nprogress/nprogress.css";

// eslint-disable-next-line import/no-extraneous-dependencies
import { StyleProvider } from "@ant-design/cssinjs";
import { ConfigProvider } from "antd";
// eslint-disable-next-line import/no-extraneous-dependencies
import AOS from "aos";
import type { AppProps } from "next/app";
import Head from "next/head";
import Router from "next/router";
import { GoogleAnalytics } from "nextjs-google-analytics";
// eslint-disable-next-line import/no-extraneous-dependencies
import NProgress from "nprogress";
import React, { useEffect } from "react";
import { Provider } from "react-redux";
import { PersistGate } from "redux-persist/integration/react";
import config from "@/store";

const MyApp = ({ Component, pageProps }: AppProps) => {
  useEffect(() => {
    AOS.init({ duration: 1000 });
  }, []);
  useEffect(() => {
    const metaViewport = document.querySelector('meta[name="viewport"]');
    if (metaViewport) {
      metaViewport.setAttribute(
        "content",
        "width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no"
      );
    }
  }, []);

  NProgress.configure({
    easing: "ease-in-out",
    speed: 500,
  });
  Router.events.on("routeChangeStart", () => NProgress.start());
  Router.events.on("routeChangeComplete", () => NProgress.done());
  Router.events.on("routeChangeError", () => NProgress.done());
  return (
    <>
      <GoogleAnalytics trackPageViews />
      <Head>
        <title>Home App</title>
        <link rel="preconnect" href="https://fonts.googleapis.com" />
        <link
          rel="preconnect"
          href="https://fonts.gstatic.com"
          crossOrigin=""
        />
        <link
          href="https://fonts.googleapis.com/css2?family=Public+Sans:ital,wght@0,300;0,400;0,600;0,700;0,900;1,300;1,400;1,600;1,700;1,900&display=swap"
          rel="stylesheet"
        />
        <link
          rel="stylesheet"
          href="https://use.typekit.net/hwm0gbd.css"
        ></link>
        <link
          rel="stylesheet"
          href="https://use.typekit.net/hwm0gbd.css"
        ></link>
        <link rel="preconnect" href="https://fonts.googleapis.com" />
        <link rel="preconnect" href="https://fonts.gstatic.com" />
        <link
          href="https://fonts.googleapis.com/css2?family=Inter:wght@100;200;300;400;500;600;700;800;900&display=swap"
          rel="stylesheet"
        />
      </Head>
      <ConfigProvider
        // touch-action="pan-x pan-y"
        theme={{
          token: {
            colorPrimary: "#EC3237",
            colorFillSecondary: "#3E4095",
            fontFamily: "Public Sans, sans-serif",
          },
        }}
      >
        <StyleProvider hashPriority="high">
          <Provider store={config.store}>
            <PersistGate loading={null} persistor={config.persistor}>
              <Component {...pageProps} />
            </PersistGate>
          </Provider>
        </StyleProvider>
      </ConfigProvider>
    </>
  );
};
export default MyApp;
