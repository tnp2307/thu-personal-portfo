/* eslint-disable simple-import-sort/imports */
import { useEffect, useState } from "react";

// eslint-disable-next-line import/no-extraneous-dependencies
import { Meta } from "@/layouts/Meta";
import AOS from "aos";
import Image from "next/image";
import { ScrollContainer } from "react-scroll-motion";

// eslint-disable-next-line import/no-extraneous-dependencies
// eslint-disable-next-line import/no-extraneous-dependencies
import "aos/dist/aos.css";

import { MainNew } from "@/components/Main/main";
import ScrollToTop from "@/components/ScrollToTop";
import { AnimatePresence, motion } from "framer-motion";
import { debounce } from "lodash";
// import { ParallaxController } from "parallax-controller";

import ProjectItem from "@/components/ProjectItem";
import TitlePanel from "@/components/Title/TitlePanel";
const listService = [
  {
    id: "01",
    url: "/assets/images/service_list_1.svg",
    label: "Web/ Landing page design",
  },
  {
    id: "02",
    url: "/assets/images/service_list_2.svg",
    label: "Printing design",
  },
  { id: "03", url: "/assets/images/service_list_3.svg", label: "Branding" },
  { id: "04", url: "/assets/images/service_list_4.svg", label: "Ecommerce" },
  {
    id: "05",
    url: "/assets/images/service_list_5.svg",
    label: "Comm/ social media",
  },
];
const listProject = [
  {
    id: "01",
    img: "/assets/images/project1.png",
    url: "/",
    size: "large",
  },
  {
    id: "02",
    img: "/assets/images/project2.png",
    url: "/",
    size: "small",
  },
  {
    id: "03",
    img: "/assets/images/project3.png",
    url: "/",
    size: "small",
  },
];
const Index = () => {
  const [viewImgService, setviewImgService] = useState("01");
  const debouncedSetState = debounce(setviewImgService, 0);

  const useWindowDimensions = () => {
    const hasWindow = typeof window !== "undefined";

    function getWindowDimensions() {
      return {
        width: hasWindow ? window.innerWidth : null,
        height: hasWindow ? window.innerHeight : null,
      };
    }

    const [windowDimensions, setWindowDimensions] = useState<{
      width: number | null;
      height: number | null;
    }>({
      width: null,
      height: null,
    });

    const handleResize = () => {
      setWindowDimensions(getWindowDimensions());
    };

    // eslint-disable-next-line consistent-return
    useEffect(() => {
      if (hasWindow) {
        handleResize();
        window.addEventListener("resize", handleResize);
        return () => window.removeEventListener("resize", handleResize);
      }
    }, [hasWindow]);

    return windowDimensions;
  };

  const { height, width } = useWindowDimensions();
  const breakpointMD = 1025;
  const breakpointSM = 768;

  useEffect(() => {
    AOS.init({ duration: 1000 });
  }, []);
  return (
    <MainNew
      meta={
        <Meta
          title="Best-English"
          description={`Best-English được xây dựng bởi công nghệ trí tuệ nhân tạo AI, giúp người học phát âm như người bản xứ và thuật toán
          "Spaced Repetion - ôn tập ngắt quãng" nhắc người học ôn tập đúng thời điểm vàng, tăng x10 lần khả năng ghi nhớ 15,000 từ vựng dễ dàng và lâu dài hơn.`}
        />
      }
      noContainer
      id="home"
    >
      <div className="relative">
        <ScrollContainer>
          {width && width >= breakpointMD ? (
            <>
              <section className="container  h-[35.5625rem] w-[75rem] relative inspiration mx-auto  my-[6.5rem] mb-[7.5rem] ">
                <div
                  data-aos="fade-right"
                  className="absolute text_1 top-[0rem] left-[1.1875rem] "
                >
                  Be my
                </div>

                <div
                  data-aos="fade-left"
                  className="absolute text_2 top-[3rem] right-[0rem]"
                >
                  inspiration
                </div>
                <div
                  data-aos="fade-right"
                  className="absolute text_3 top-[19.9375rem] left-[1.1875rem]"
                >
                  I’ll be yours
                </div>
                <div data-aos="fade-left">
                  <Image
                    width={133}
                    height={133}
                    src="/assets/images/inspiration-1.png"
                    className="rounded-8 absolute img_inspi_1 top-[0.25rem] left-[24.8125rem]"
                    alt="Inspiration 1"
                  />
                </div>
                <div data-aos="fade-right">
                  <Image
                    width={295}
                    height={295}
                    src="/assets/images/inspiration-2.png"
                    className=" rounded-12 absolute img_inspi_2 top-[8.125rem] left-[0rem]"
                    alt="Inspiration 1"
                  />
                </div>
                <div data-aos="fade-right">
                  <Image
                    width={265}
                    height={265}
                    src="/assets/images/inspiration-4.jfif"
                    className="rounded-20 absolute img_inspi_3 top-[21.25rem] left-[43.375rem] -z-10"
                    alt="Inspiration 1"
                  />
                </div>

                <div
                  data-aos="fade-right"
                  className="absolute bottom-[0rem] left-[1.8125rem]"
                >
                  <div className="flex flex-col gap-3">
                    <div className="flex gap-3 justify-start">
                      <div className="title-button">20+ Clients</div>
                      <div className="title-button">5+ year of experience</div>
                    </div>
                    <div className="flex gap-3 justify-start">
                      <div className="title-button">Art direction</div>
                      <div className="title-button">Graphic design</div>
                      <div className="title-button">Social designZ</div>
                    </div>
                  </div>
                </div>
              </section>

              <section
                className="container service-list mx-auto "
                style={{ padding: "24px 0 24px 0" }}
              >
                <div
                  className="order-1 lg:order-1"
                  style={{ marginBottom: "32px" }}
                >
                  <TitlePanel title="Services" subTitle="My area of experts" />
                  <div className="flex flex-row justify-between">
                    <div data-aos="fade-right" className="relative">
                      <AnimatePresence initial={false} mode="popLayout">
                        {listService.map((item) => {
                          return (
                            viewImgService === item.id && (
                              <motion.div
                                key={item.id}
                                initial={{
                                  // scaleY: 0.7,
                                  y: 50,
                                  opacity: 0,
                                  scale: 1,
                                  z: 2,
                                }}
                                animate={{
                                  scale: [1, 1, 1, 1, 1],
                                  y: [100, 90, 60, 30, 20, 10, 0],
                                  opacity: [0, 0.3, 0.5, 1, 1, 1, 1],
                                  z: 3,
                                }}
                                exit={{
                                  // scaleY: 0.7,

                                  opacity: [1, 0.5, 0.8, 0.8, 0.5, 0.5, 0.5],
                                  scale: [1, 0.9, 0.8, 0.7, 0.6, 0.5, 0.4],
                                  y: [0, -30, -60, -90, -100, -100, -100],
                                  z: [-1, -1, -1, -1, -1],
                                }}
                                transition={{
                                  ease: "easeInOut",
                                  duration: 0.5,
                                }}
                              >
                                <Image
                                  key={item.id}
                                  width={438.4}
                                  height={428}
                                  src={item.url}
                                  className=""
                                  alt={item.label}
                                />
                              </motion.div>
                            )
                          );
                        })}
                      </AnimatePresence>
                    </div>
                    <div data-aos="fade-left">
                      <ul className="flex flex-col ">
                        {listService.map((item) => {
                          return (
                            <li key={item.id}>
                              <motion.a
                                onHoverStart={(e) => {
                                  debouncedSetState(item.id);
                                }}
                                onHoverEnd={(e) => {
                                  debouncedSetState("01");
                                }}
                                href=""
                                className={`flex items-stretch gap-5 text-[#C8C8C8] justify-end py-[1.3rem] transition-all ease-in-out ${
                                  viewImgService === item.id
                                    ? "text-black-0 opacity-60 "
                                    : ""
                                }`}
                              >
                                <span className="text-[1.25rem] font-normal self-start ">
                                  {item.id}
                                </span>
                                <h3 className=" pt-3 inline-block text-[2.5rem] font-normal">
                                  {item.label}
                                </h3>
                              </motion.a>
                            </li>
                          );
                        })}
                      </ul>
                    </div>
                  </div>
                </div>
              </section>
              <section
                className="container service-list mx-auto "
                style={{ padding: "24px 0 24px 0" }}
              >
                <div
                  className="order-1 lg:order-1"
                  style={{ marginBottom: "32px" }}
                >
                  <TitlePanel title="Works" subTitle="Featured project" />
                  <div className="flex flex-col gap-20">
                    <div className="relative">
                      <h4>
                        Projects that define
                        <br />
                        <b data-aos="fade-top">
                          my creativity and problem solving skill
                        </b>
                      </h4>
                    </div>
                    <div className="flex gap-5" data-aos="fade-left">
                      <div
                        data-aos="fade-right"
                        data-aos-easing="ease-in-back"
                        data-aos-offset="500"
                        className="col-span-2"
                      >
                        <ProjectItem
                          size={listProject[0]?.size}
                          img={listProject[0]?.img}
                          url={listProject[0]?.url}
                        />
                      </div>
                      <div className="col-span-1 h-full">
                        <div className="flex flex-col justify-between gap-5">
                          {listProject.slice(1, 3).map((item, index) => {
                            return (
                              <div
                                data-aos="fade-left"
                                data-aos-easing="ease-in-back"
                                data-aos-delay={index === 0 ? "400" : "800"}
                                data-aos-offset={index === 0 ? "500" : "250"}
                              >
                                <ProjectItem
                                  size={item.size}
                                  img={item.img}
                                  url={item.url}
                                />
                              </div>
                            );
                          })}
                          <img
                            data-aos="fade-left"
                            data-aos-easing="ease-in-back"
                            data-aos-delay="1200"
                            data-aos-offset="0"
                            className="h-[4rem] w-[23.9375rem] cursor-pointer"
                            src="/assets/images/button-view-work.png"
                            alt=""
                          />
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </section>
              <section
                className="container service-list mx-auto "
                style={{ padding: "24px 0 24px 0" }}
              >
                <div
                  className="order-1 lg:order-1"
                  style={{ marginBottom: "32px" }}
                >
                  <TitlePanel
                    title="Partners"
                    subTitle=" Brands me partner with"
                  />
                  <div className="flex flex-col gap-20">
                    <div
                      data-aos="fade-right"
                      className="flex justify-between item-center"
                    >
                      <div className="flex justify-center items-center">
                        <Image
                          width={101}
                          height={88.1}
                          alt=""
                          src="/assets/images/partner1.png"
                        />
                      </div>
                      <div className="flex justify-center items-center">
                        <Image
                          width={137.2}
                          height={50}
                          alt=""
                          src="/assets/images/partner2.png"
                        />
                      </div>
                      <div className="flex justify-center items-center">
                        <Image
                          width={152}
                          height={64}
                          alt=""
                          src="/assets/images/partner3.png"
                        />
                      </div>
                      <div className="flex justify-center items-center">
                        <Image
                          width={176.2}
                          height={57.2}
                          alt=""
                          src="/assets/images/partner4.png"
                        />
                      </div>
                      <div className="flex justify-center items-center">
                        <Image
                          width={182.6}
                          height={32.7}
                          alt=""
                          src="/assets/images/partner5.png"
                        />
                      </div>
                    </div>
                    <div
                      data-aos="fade-left"
                      className="flex justify-between item-center"
                    >
                      <div className="flex justify-center items-center">
                        <Image
                          width={182.6}
                          height={32.7}
                          alt=""
                          src="/assets/images/partner6.png"
                        />
                      </div>
                      <div className="flex justify-center items-center">
                        <Image
                          width={111}
                          height={82}
                          alt=""
                          src="/assets/images/partner7.png"
                        />
                      </div>
                      <div className="flex justify-center items-center">
                        <Image
                          width={106}
                          height={106}
                          alt=""
                          src="/assets/images/partner8.png"
                        />
                      </div>
                      <div className="flex justify-center items-center">
                        <Image
                          width={172}
                          height={34}
                          alt=""
                          src="/assets/images/partner9.png"
                        />
                      </div>
                      <div className="flex justify-center items-center">
                        <Image
                          width={190}
                          height={41}
                          alt=""
                          src="/assets/images/partner10.png"
                        />
                      </div>
                    </div>
                  </div>
                </div>
              </section>
            </>
          ) : (
            <div></div>
          )}
        </ScrollContainer>
        <div className="fixed bottom-36 right-16">
          <ScrollToTop />
        </div>
      </div>
    </MainNew>
  );
};

export default Index;
