import { useEffect } from "react";

import CateListBlog from "@/components/CateListBlog";
import { Main } from "@/components/Main";
import { Meta } from "@/layouts/Meta";
import useTrans from "@/utils/userTrans";

const Blog = (props: any) => {
  const listData = props?.data?.data;

  useEffect(() => {
    if ("scrollRestoration" in window.history) {
      window.history.scrollRestoration = "manual";
    }
  }, []);
  const trans = useTrans();
  return (
    <Main
      meta={
        <Meta
          title={trans["header.grammaBlog"]}
          description={trans["header.grammaBlog"]}
        />
      }
      noContainer
      headerLanding
    >
      <div className="backgroundBlogs col-span-12 w-full text-center">
        <div className="backgroundBlog">
          <h2
            // style={{ textShadow: "0px 4px 4px rgba(0, 0, 0, 0.9)" }}
            className="mx-auto flex h-full max-w-[70%] items-center justify-center text-[1.5rem] font-bold text-[#FFF] md:text-[2.5rem] lg:text-[3.5rem]"
          >
            {trans["blog.title"]}
          </h2>
          <div className="bg-blg-img">
            <img src="/assets/images/img-gr-blog.png" alt="img" />
          </div>
        </div>
      </div>
      <section className="container mx-auto pt-[2rem]" id="categories-page">
        <div className="grid grid-cols-1 gap-0 px-[1rem] pb-[3rem] lg:grid-cols-3 lg:gap-[2rem] lg:divide-x lg:px-0">
          <div className="css-blog-items col-span-4 leading-8">
            <CateListBlog data={listData as any} />
          </div>
        </div>
      </section>
    </Main>
  );
};

export default Blog;

export async function getServerSideProps() {
  let data = null;

  const URL = `${process.env.NEXT_PUBLIC_API_ENDPOINT}/Blogs/Search`;

  await fetch(URL, {
    method: "POST",
    body: JSON.stringify({
      search: "",
      pageSize: 1000,
      pageIndex: 0,
      filterOptions: [
        {
          column: "",
          value: "",
        },
      ],
      sortOptions: [
        {
          column: "AddedTimestamp",
          direction: "desc",
        },
      ],
    }),
    headers: {
      "Content-type": "application/json",
    },
  })
    .then((response) => response.json())
    .then((result) => {
      data = result;
    })
    .catch((error) => {});

  return {
    props: {
      data,
    }, // will be passed to the page component as props
  };
}
