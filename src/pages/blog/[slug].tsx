import React, { useEffect, useState } from "react";

import { getEmbedLink } from "@/common/convert-video-url/video";
import * as callAPI from "@/common/services/apiCoursesDetail";
import BlogItemsExtra from "@/components/BlogItem/newBlogItemExtra";
import { Main } from "@/components/Main";
import type { BlogItem } from "@/interfaces/blog.interfaces";
import { Meta } from "@/layouts/Meta";
import useTrans from "@/utils/userTrans";

type IBlogUrl = {
  slug: string;
};

const MAX_CHARACTERS = 500;

const Blog = ({ data }: any) => {
  const [showFullDescription, setShowFullDescription] = useState([]);
  const res: BlogItem = data?.data;
  const trans = useTrans();
  const { id } = data.data;
  const video = res?.filePathVideo;
  const renderVideo = (videoItem: string) => {
    if (
      videoItem.includes("https://www.youtube.com/") ||
      videoItem.includes("https://youtu")
    ) {
      const outputA = getEmbedLink(videoItem);
      return (
        <>
          <iframe
            className="custom-iframe-video"
            allow="autoplay; encrypted-media"
            src={outputA}
          ></iframe>
          <div className="my-[2rem] border-b"></div>
        </>
      );
    }
    return <></>;
  };
  const titleShort = res?.titleShort || "";
  const description = res?.description || "";
  let displayDescription = description;
  let showButton = false;

  if (description.length > MAX_CHARACTERS) {
    displayDescription = `${description.slice(0, MAX_CHARACTERS)}...`;
    showButton = true;
  }

  const ResDetailBlog = async () => {
    const response = await callAPI.SearchHashtagAsync(res?.hashTag);
    if (response?.message === "SUCCESSFULL") {
      setShowFullDescription(response?.data);
    }
  };

  useEffect(() => {
    ResDetailBlog();
  }, [res?.hashTag]);
  useEffect(() => {
    if ("scrollRestoration" in window.history) {
      window.history.scrollRestoration = "manual";
    }
  }, []);
  const [listData, setListData] = useState([]);
  const fetchBlogData = async () => {
    let dataBlog = null;
    const URL = `${process.env.NEXT_PUBLIC_API_ENDPOINT}/Blogs/Search`;
    await fetch(URL, {
      method: "POST",
      body: JSON.stringify({
        search: "",
        pageSize: 1000,
        pageIndex: 0,
        filterOptions: [
          {
            column: "",
            value: "",
          },
        ],
        sortOptions: [
          {
            column: "AddedTimestamp",
            direction: "desc",
          },
        ],
      }),
      headers: {
        "Content-type": "application/json",
      },
    })
      .then((response) => response.json())
      .then((result) => {
        setListData(result.data.items.filter((item: any) => item.id !== id));
        dataBlog = result;
      })
      .catch((error) => {});

    return {
      props: {
        dataBlog,
      }, // will be passed to the page component as props
    };
  };
  useEffect(() => {
    fetchBlogData();
  }, [id]);

  return (
    <Main
      meta={<Meta title={res?.title} description="Tổng hợp ngữ pháp" />}
      noContainer
      headerLanding
    >
      <div className="bg-white">
        {/* <div className="backgroundBlogs col-span-12 w-full text-center">
          <div className="backgroundBlog">
            <h2 className="mx-auto flex h-full max-w-[70%] items-center justify-center text-[1.5rem] font-bold text-[#FFF] md:text-[2.5rem] lg:text-[3.5rem]">
              {trans["blog.title"]}
            </h2>
            <div className="bg-blg-img">
              <img src="/assets/images/img-gr-blog.png" alt="img" />
            </div>
          </div>
        </div> */}
        <div className="container grid grid-cols-3 ">
          <section className=" container col-span-3 mx-auto py-[2rem] lg:col-span-2">
            <div
              className="displex-customer-blog grid grid-cols-1 gap-0 p-3 px-[1rem] lg:grid-cols-3 lg:gap-[2rem] lg:divide-x lg:px-0"
              style={{ background: "white", borderRadius: "5px" }}
            >
              <article className="full-size-responsives col-span-4 leading-8">
                <div className="mb-[2rem] border-b">
                  <h1 className="text-32 font-bold text-[#3E4095]">
                    {res?.title}
                  </h1>
                  <p className="description-date-time-blogs">
                    <img src="/assets/icons/icon_date.png" alt="date"></img>
                    {res?.addedTimestamp}
                  </p>
                </div>
                {video && renderVideo(video)}
                <div
                  className="font-bold"
                  dangerouslySetInnerHTML={{ __html: titleShort }}
                ></div>
                <br />
                <div>
                  <div
                    className="indent-[2.25rem]"
                    dangerouslySetInnerHTML={{ __html: description }}
                  ></div>
                </div>
              </article>
            </div>
          </section>
          <section className="container col-span-3 mx-auto mb-[3rem] mt-[40px] px-12 lg:col-span-1 lg:px-0">
            <div className="flex justify-between">
              <h2 className=" text-20 font-bold ">{trans["blog.news"]}</h2>
            </div>
            <div className="grid grid-cols-1 ">
              {listData?.slice(0, 3).map((value: any, key: any) => {
                return (
                  <div key={key} data-aos="fade-left">
                    <BlogItemsExtra listdata={value} />
                  </div>
                );
              })}
            </div>
          </section>
        </div>
      </div>
    </Main>
  );
};

export default Blog;

export async function getServerSideProps(context: any) {
  const slug = context?.query?.slug;
  let data = null;

  const URL = `${process.env.NEXT_PUBLIC_API_ENDPOINT}/Blogs/${slug}`;

  await fetch(URL, {
    method: "GET",
    headers: {
      "Content-Type": "application/json",
    },
  })
    .then((response: any) => response.json())
    // eslint-disable-next-line @typescript-eslint/no-shadow
    .then((result: any) => {
      data = result;
    })
    .catch((error) => {});

  return {
    props: {
      data,
    },
  };
}
