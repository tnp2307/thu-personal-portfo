import * as React from "react";
import IcArrow from "@/public/assets/icons/ic-arrow-diag.svg";
import { MainNew } from "@/components/Main/main";
import { Meta } from "@/layouts/Meta";

export interface IAppProps {}

export default function MainPage(props: IAppProps) {
  return (
    <MainNew
      meta={
        <Meta
          title="ThuPhanSone"
          description={`Best-English được xây dựng bởi công nghệ trí tuệ nhân tạo AI, giúp người học phát âm như người bản xứ và thuật toán
        "Spaced Repetion - ôn tập ngắt quãng" nhắc người học ôn tập đúng thời điểm vàng, tăng x10 lần khả năng ghi nhớ 15,000 từ vựng dễ dàng và lâu dài hơn.`}
        />
      }
      noContainer
    >
      <div className="main">
        <div className="main-box">
          <div className="b1">
            <img src="/assets/images/new-ing-home-page.png" alt="" />
          </div>
          <div className="b2">
            <button className="diag-arrow">
              <IcArrow />
            </button>
          </div>
        </div>
      </div>
    </MainNew>
  );
}
