import { initializeApp } from "firebase/app";
import { getMessaging, getToken } from "firebase/messaging";
import React, { useEffect, useRef, useState } from "react";

import { firebaseConfig, publicValidKey } from "@/utils/firebase.config";
import { localStorageHelpers } from "@/utils/localStorage";

function App() {
  const [inputValue, setInputValue] = useState("");

  const [enterCount, setEnterCount] = useState(0);

  const [isDisabled, setisDisabled] = useState(false);
  const firebaseCode: any = localStorageHelpers.getData("tokenFirebase");
  const divRef: any = useRef(null);

  const ReqNotiTwo = async () => {
    try {
      const permission = await Notification.requestPermission();
      if (permission === "granted") {
        await navigator.serviceWorker?.register("./firebase-messaging-sw.js");

        const app = initializeApp(firebaseConfig);

        const messaging = getMessaging(app);
        const currentToken = await getToken(messaging, {
          vapidKey: publicValidKey,
        });
        const value = localStorage.getItem("tokenFirebase");
        if (value === null || value === undefined) {
          localStorage.setItem("tokenFirebase", currentToken);
        } else {
          // eslint-disable-next-line no-lonely-if
          if (value !== currentToken) {
            localStorage.setItem("tokenFirebase", currentToken);
          }
        }
      }
    } catch (error) {
      console.error(error);
    }
  };

  const handleInputChange = (event: any) => {
    setInputValue(event.target.value);
  };

  ReqNotiTwo();

  useEffect(() => {
    document.getElementById("newInput")?.focus();
  }, []);

  const handleKeyDown = (event: any) => {
    if (event.key === "Enter") {
      if (enterCount === 0) {
        setEnterCount(1);
        setisDisabled(true);
        divRef.current.focus();
      } else if (enterCount === 1) {
        setEnterCount(2);
      } else if (enterCount === 2) {
        console.log(2);
      }
      setInputValue("");
    }
  };

  return (
    <div id="body">
      <div ref={divRef} tabIndex={0} onKeyDown={handleKeyDown}>
        <input
          id="newInput"
          type="number"
          value={inputValue}
          onChange={handleInputChange}
          disabled={isDisabled}
          style={{ opacity: "0", width: "0", height: "0" }}
        />
      </div>
    </div>
  );
}

export default App;
