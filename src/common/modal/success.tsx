const SuccessModal = () => {
  return (
    <div className="modal-handle-question success">
      <img src="/assets/icons/ic-question-success.svg" alt="icon success" />
      <div className="bg-txt-question-answer">
        <p className="title__name-en">discuss</p>
        <div className="wrapper-child-question-answer">
          <p className="title__pronounce">/dɪˈskʌs/</p>
          <p className="title__noun">(v)</p>
          <p className="title__name-vi">Bàn luận, thảo luận, tranh luận </p>
        </div>
        <div className="wrapper-example">
          <p className="title__example">
            Example: We Often discuss some global affairs in the class
          </p>
        </div>
        <div className="wrapper-child-question-answer two">
          <img src="/assets/icons/ic-flag.svg" alt="icon flag" />
          <p>Báo lỗi nội dung</p>
        </div>
      </div>
    </div>
  );
};

export default SuccessModal;
