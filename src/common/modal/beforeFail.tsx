import { useState } from "react";

import IcClose from "@/public/assets/icons/ic-close-primary.svg";
import IcFlag from "@/public/assets/icons/ic-flag.svg";
import IcTranslate from "@/public/assets/icons/ic-translate.svg";
import useTrans from "@/utils/userTrans";

import NotiFeedback from "../popup/popupFeedback";

function BeforeFail({ data }: any) {
  const [show, setShow] = useState<boolean>(false);
  const [trans, setShowTrans] = useState<boolean>(false);
  const transs = useTrans();
  return (
    <>
      <div
        className="fade-up-animation inset-x-0 bottom-0 z-0 mt-auto bg-[#F48487] p-[1rem] md:p-[1.5rem]"
        style={{ position: "absolute" }}
      >
        <div className="flex gap-[0.5rem] md:gap-[2rem]">
          <div className="flex h-[3rem] max-h-[3rem] w-[3rem] min-w-[3rem] items-center justify-center rounded-full bg-white md:h-[4rem] md:max-h-[4rem] md:w-[4rem] md:min-w-[4rem]">
            <IcClose />
          </div>
          <button
            onClick={() => setShowTrans(!trans)}
            className=" absolute right-[1rem] top-[1rem] flex h-[3rem] w-[3rem] animate-pulse  items-center justify-center rounded-full bg-white "
          >
            <IcTranslate />
          </button>
          <div className="pr-[3rem] text-white md:py-0">
            <h3 className="text-[1.6rem] font-bold md:text-[2.5rem]">
              {data?.word}
            </h3>
            <p className="fade-up-animation mt-1">
              <span className="text-16 md:text-18">/{data?.spelling}/</span>
              <span className="mx-[1.25rem] text-14 md:mx-[2.5rem] md:text-18">
                ({data?.form})
              </span>
              <span>{data?.meaning} </span>
            </p>
            <p className="fade-up-animation mt-1">
              <span className="text-16 font-bold md:text-18">Example: </span>
              <span className="mx-[0.2rem] text-16 md:text-18">
                {data?.sentence}
              </span>
            </p>
            {trans && (
              <div>
                <p className="fade-up-animation mt-1">
                  <span className=" text-18 font-bold md:text-18">
                    {transs["common.label.translate"]}{" "}
                  </span>
                </p>
                <p className="fade-up-animation mt-1">
                  <span className="text-18 md:text-18">
                    {data?.meanSentence}
                  </span>
                </p>
              </div>
            )}
            <button
              onClick={() => setShow(true)}
              className=" mt-[1.5rem] flex items-center gap-3 border-b-[1px] border-b-[#FEC84B] pb-[0.6rem] text-18"
            >
              <IcFlag />
              {transs["learnNewWord.fillWord.reportBug"]}
            </button>
          </div>
        </div>
      </div>
      {show && <NotiFeedback unitId={data.id} setPopupFeedback={setShow} />}
    </>
  );
}

export default BeforeFail;
