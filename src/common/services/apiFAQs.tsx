// import type * as type from "@/interfaces/question.interface";
import type * as type from "@/interfaces/courses.interfaces";
import ItemQuestionService from "@/services/QuestionService";

const bodyUnit: type.ReqGetCourses = {
  search: "",
  pageSize: 1000,
  pageIndex: 0,
  filterOptions: [
    {
      column: "",
      value: "",
    },
  ],
  sortOptions: [
    {
      column: "Priority",
      direction: "desc",
    },
  ],
};

const bodyUnits: type.ReqGetCourses = {
  search: "",
  pageSize: 3,
  pageIndex: 0,
  filterOptions: [
    {
      column: "",
      value: "",
    },
  ],
  sortOptions: [
    {
      column: "AddedTimestamp",
      direction: "desc",
    },
  ],
};

export const resQuestion = async (body: type.ReqGetCourses) => {
  const response = await ItemQuestionService.question(body)
    // eslint-disable-next-line @typescript-eslint/no-shadow
    .then(function (response) {
      return response?.data;
    })
    .catch(function (error) {});

  return response;
};

export const getBlog = async () => {
  const response = await ItemQuestionService.blog(bodyUnits)
    // eslint-disable-next-line @typescript-eslint/no-shadow
    .then(function (response) {
      return response?.data;
    })
    .catch(function (error) {});

  return response;
};
