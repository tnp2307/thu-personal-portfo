import { message } from "antd";
import { event } from "nextjs-google-analytics";

import type * as type from "@/interfaces/courses.interfaces";
import ItemCoursesService from "@/services/ItemCourses";

const bodyUnit: type.ReqGetCourses = {
  search: "",
  pageSize: 1000,
  pageIndex: 0,
  filterOptions: [
    {
      column: "",
      value: "",
    },
  ],
  sortOptions: [
    {
      column: "Priority",
      direction: "desc",
    },
  ],
};

export const resUnit = async (id: string, body: type.ReqGetCourses) => {
  const response = await ItemCoursesService.initCoursesAuth(body, id)
    // eslint-disable-next-line @typescript-eslint/no-shadow
    .then((response) => {
      return response?.data;
    })
    .catch((error) => {});

  return response;
};

export const resUnitLesson = async (id: string) => {
  const response = await ItemCoursesService.initLesson(bodyUnit, id)
    // eslint-disable-next-line @typescript-eslint/no-shadow
    .then((response) => {
      return response?.data;
    })
    .catch((error) => {});

  return response;
};

export const resLearnWords = async (id: string) => {
  const response = await ItemCoursesService.learnWords(bodyUnit, id)
    // eslint-disable-next-line @typescript-eslint/no-shadow
    .then((response) => {
      return response?.data;
    })
    .catch((error) => {});

  return response;
};

export const UPdateAccount = async (data: any) => {
  const newObj = {
    fullName: data.fullName,
    email: data.email,
    phoneNumber: data.phoneNumber,
    address: data.address,
    subcriptionId: data.subcriptionId,
    description: data.description,
  };
  const response = await ItemCoursesService.ServiceUPdateAccount(newObj)
    // eslint-disable-next-line @typescript-eslint/no-shadow
    .then((response) => {
      event("Upgrade request", {
        category: "Upgrade request",
        label: "Upgrade request",
      });
      message.success("Gửi yêu cầu thành công !");
      return response?.data;
    })
    .catch((error) => {
      message.error("Gửi yêu cầu thất bại !");
    });

  return response;
};

export const UPdateAccountInfor = async (data: any) => {
  const newObj = {
    fullName: data.name,
    email: data.email,
    phoneNumber: data.phoneNumber,
    address: data.address,
    dob: data.dob,
    gender: data.gender,
    provincesId: data.provincesId,
    districtsId: data.districtsId,
    oldPassword: data.oldPassword,
    newPassword: data.newPassword,
  };

  const response = await ItemCoursesService.ServiceUPdateAccountInfor(newObj)
    // eslint-disable-next-line @typescript-eslint/no-shadow
    .then((response) => {
      if (response.data.statusCode === 417) {
        message.error("Sai mật khẩu cũ");
      } else {
        message.success("Lưu thay đổi thành công !");
      }
      return response?.data;
    })
    .catch((error) => {
      message.error("Lưu thay đổi thất bại !");
    });

  return response;
};

export const resStaticPages = async () => {
  const response = await ItemCoursesService.staticPage()
    // eslint-disable-next-line @typescript-eslint/no-shadow
    .then((response) => {
      return response?.data;
    })
    .catch((error) => {});

  return response;
};
export const resStaticDetailsPages = async (url: any) => {
  const response = await ItemCoursesService.staticPageDetails(url)
    // eslint-disable-next-line @typescript-eslint/no-shadow
    .then((response) => {
      return response?.data;
    })
    .catch((error) => {});

  return response;
};
export const resStaticPagesFooter = async () => {
  const response = await ItemCoursesService.staticPageFooter()
    // eslint-disable-next-line @typescript-eslint/no-shadow
    .then((response) => {
      return response?.data;
    })
    .catch((error) => {});

  return response;
};

export const SearchHashtagAsync = async (data: any) => {
  const response = await ItemCoursesService.SearchHashtagAsync(data)
    // eslint-disable-next-line @typescript-eslint/no-shadow
    .then((response) => {
      return response?.data;
    })
    .catch((error) => {});

  return response;
};

export const RefreshTokensFireBase = async (token: any) => {
  const response = await ItemCoursesService.RefreshTokensFireBase(token)
    // eslint-disable-next-line @typescript-eslint/no-shadow
    .then((response) => {
      return response?.data;
    })
    .catch((error) => {});

  return response;
};

export const UpdateNotifi = async (dataReq: any) => {
  const response = await ItemCoursesService.UdpateNotifi(dataReq)
    // eslint-disable-next-line @typescript-eslint/no-shadow
    .then((response) => {
      return response?.data;
    })
    .catch((error) => {});

  return response;
};
