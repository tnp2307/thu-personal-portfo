import ItemReviseService from "@/services/ReviseService";

export const resDataRevise = async (
  value: number,
  index: number,
  year: number
) => {
  const response = await ItemReviseService.revises(value, index, year)
    // eslint-disable-next-line @typescript-eslint/no-shadow
    .then(function (response) {
      return response?.data;
    })
    .catch(function (error) {});

  return response;
};
export const resDataReviseLevel = async () => {
  const response = await ItemReviseService.revisesLevel()
    // eslint-disable-next-line @typescript-eslint/no-shadow
    .then(function (response) {
      return response?.data;
    })
    .catch(function (error) {});

  return response;
};
