import type * as type from "@/interfaces/learnnewworks.interfaces";
import LearnersService from "@/services/LearnersService";

export const resBindData = async (
  body: type.ResponsaveNewLearn,
  idQuery: string
) => {
  const response = await LearnersService.learnNewWord(body, idQuery)
    // eslint-disable-next-line @typescript-eslint/no-shadow
    .then(function (response) {
      return response?.data;
    })
    .catch(function (error) {});
  return response;
};

export const resSaveHandBook = async (body: type.ResponsaveNewLearn) => {
  const response = await LearnersService.saveHandBook(body)
    // eslint-disable-next-line @typescript-eslint/no-shadow
    .then(function (response) {
      return response?.data;
    })
    .catch(function (error) {});
  return response;
};

export const resSaveHandBooks = async (body: any) => {
  const response = await LearnersService.saveHandBook(body)
    // eslint-disable-next-line @typescript-eslint/no-shadow
    .then(function (response) {
      return response?.data;
    })
    .catch(function (error) {});
  return response;
};

export const resBindNotifi = async (body: any) => {
  const response = await LearnersService.postBindNotifi(body)
    // eslint-disable-next-line @typescript-eslint/no-shadow
    .then(function (response) {
      return response?.data;
    })
    .catch(function (error) {});
  return response;
};
