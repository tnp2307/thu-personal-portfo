import { event } from "nextjs-google-analytics";

import type * as type from "@/interfaces/courses.interfaces";
import type * as types from "@/interfaces/notebook.interfaces";
// eslint-disable-next-line import/no-cycle
import ItemCoursesService from "@/services/NoteService";

const bodyUnit: type.ReqGetCourses = {
  search: "",
  pageSize: 1000,
  pageIndex: 0,
  filterOptions: [
    {
      column: "",
      value: "",
    },
  ],
  sortOptions: [
    {
      column: "",
      direction: "",
    },
  ],
};

export const resNoteBook = async (type: types.NoteBookSearch) => {
  const response = await ItemCoursesService.PostNoteBookSearchs(type, bodyUnit)
    .then(function (res) {
      return res?.data;
    })
    .catch(function (error) {});

  return response;
};

export const resDelVocabulary = async (type: types.VocabularyID) => {
  const response = await ItemCoursesService.DeleteVocabularyNote(type)
    .then(function (res) {
      return res?.data;
    })
    .catch(function (err) {});

  return response;
};

export const resDataReview = async () => {
  const response = await ItemCoursesService.ResDataReview()
    .then(function (res) {
      return res?.data;
    })
    .catch(function (error) {});

  return response;
};

export const pushDataReview = async (bindAPIData: any) => {
  if (bindAPIData.length > 0) {
    event("Revise event", {
      category: "Revise event",
      label: "Revise event",
    });
  }
  const response = await ItemCoursesService.PushDataReview(bindAPIData)
    .then(function (res) {
      return res.data;
    })
    .catch(function (error) {});

  return response;
};

export const ruleReview = async (data: any) => {
  const response = await ItemCoursesService.RuleReiew(data)
    .then(function (res) {
      return res?.data;
    })
    .catch(function (error) {});

  return response;
};

export const removeTokenFirebase = async (type: any) => {
  const response = await ItemCoursesService.RemoveTokenFirebase(type)
    .then(function (res) {
      return res?.data;
    })
    .catch(function (error) {});

  return response;
};
