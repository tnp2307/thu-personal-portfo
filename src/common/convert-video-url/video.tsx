export const getVideoIdFromLink = (link: string) => {
  let videoId: string = "";
  if (link.includes("watch?v=")) {
    // eslint-disable-next-line prefer-destructuring
    videoId = link.split("watch?v=")[1] || "";
  } else if (link.includes("youtu.be/")) {
    // eslint-disable-next-line prefer-destructuring
    videoId = link.split("youtu.be/")[1] || "";
  }
  return videoId;
};

export const getEmbedLink = (link: string) => {
  const videoId = getVideoIdFromLink(link || "");
  return `https://www.youtube.com/embed/${videoId}?autoplay=1&mute=1`;
};
