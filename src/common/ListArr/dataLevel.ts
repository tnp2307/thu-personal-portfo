export const dataLevel = [
  {
    id: 1,
    name: "Cấp độ 1",
    color: "#46076B",
  },
  {
    id: 2,
    name: "Cấp độ 2",
    color: "#54A3DF",
  },
  {
    id: 3,
    name: "Cấp độ 3",
    color: "#FDB022",
  },
  {
    id: 4,
    name: "Cấp độ 4",
    color: "#3E4095",
  },
  {
    id: 5,
    name: "Cấp độ 5",
    color: "#FF800B",
  },
  {
    id: 6,
    name: "Cấp độ 6",
    color: "#0C4E92",
  },
  {
    id: 7,
    name: "Cấp độ 7",
    color: "#62A109",
  },
];

export const getReturnCode = (value: string, index: number) => {
  const result = index / 3;
  const roundedNumber = Math.round(result);

  const lowercaseAbc = value.toLowerCase();

  switch (lowercaseAbc) {
    case "gói trial":
      return 1;
    case "gói basic":
      return roundedNumber === 0 ? 1 : roundedNumber;
    case "gói premium":
      return index;
    case "gói pro":
      return index;
    default:
      return 1;
  }
};
