export const highlightWord = (text: any, words: any) => {
  const wordsArray = words?.split(";").map((word: any) => word.trim());
  const regex = new RegExp(`(${wordsArray.join("|")})`, "gi");
  return text?.split(regex).map((part: any, index: number) => (
    <span
      key={index}
      className={
        wordsArray.some(
          (word: any) => part.toLowerCase() === word.toLowerCase()
        )
          ? "text-primary underline"
          : ""
      }
    >
      {part}
    </span>
  ));
};

// export const highlightWord = (text: any, word: any) => {
//   const regex = new RegExp(`(${word})`, "gi");
//   return text?.split(regex).map((part: any, index: any) => (
//     <span
//       key={index}
//       className={
//         part.toLowerCase() === word.toLowerCase()
//           ? "text-primary underline"
//           : ""
//       }
//     >
//       {part}
//     </span>
//   ));
// };

export const hideHighlightWord = (text: any, words: any) => {
  const wordsArray = words?.split(";").map((word: any) => word.trim());
  const regex = new RegExp(`(${wordsArray.join("|")})`, "gi");
  // const regex = new RegExp(`(${word})`, "gi");
  return text?.split(regex).map((part: any, index: any) => (
    <span
      key={index}
      className={
        wordsArray.some(
          (word: any) => part.toLowerCase() === word.toLowerCase()
        )
          ? "text-highlight"
          : ""
      }
      // className={
      //   part.toLowerCase() === words.toLowerCase() ? "text-highlight" : ""
      // }
    >
      {part}
    </span>
  ));
};

export const highlightWords = (text: any, word: any, selectedItem: any) => {
  const regex = new RegExp(`(${word})`, "gi");
  return text?.split(regex).map((part: any, index: any) => (
    <span
      key={index}
      className={
        part.toLowerCase() === word.toLowerCase()
          ? "text-primary underline"
          : ""
      }
    >
      {part}
    </span>
  ));
};

export const shuffleArray = (array: any) => {
  // eslint-disable-next-line no-plusplus
  for (let i = array.length - 1; i > 0; i--) {
    const j = Math.floor(Math.random() * (i + 1));
    // eslint-disable-next-line no-param-reassign
    [array[i], array[j]] = [array[j], array[i]];
  }
  return array;
};
