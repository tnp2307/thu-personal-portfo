import { Button, Form, Input, Modal, Select } from "antd";
import axios from "axios";
import React, { useEffect, useState } from "react";
import { useSelector } from "react-redux";

import * as callAPI from "@/common/services/apiCoursesDetail";
import IcMail from "@/public/assets/icons/ic-mail.svg";
import IcPhone from "@/public/assets/icons/ic-phone.svg";
import IcUser from "@/public/assets/icons/ic-user.svg";
import { getProfileSelector } from "@/store/auth/selectors";
import useTrans from "@/utils/userTrans";

function UpgradeAccount({ open, setOpen }: any) {
  const [dataSubcribe, GetDataSubcribe] = useState([]);
  const [form] = Form.useForm();
  const trans = useTrans();
  useEffect(() => {
    const dataSubribe = async () => {
      const data = await axios({
        method: "get",
        url: `${process.env.NEXT_PUBLIC_API_ENDPOINT}/LandingPage/GetSubcriptionLandingPage/?lang=0`,
        headers: {
          accept: "application/json",
          // Authorization: `Bearer ${accessToken}`,
        },
      })
        .then((response: any) => {
          // handle success
          const dataSelect = response?.data?.data?.items || [];
          const SelectOptions = dataSelect?.map((item: any, index: number) => ({
            key: index,
            value: item.id,
            label: item.name,
            originalPrice: `${item.originalPrice.toLocaleString("it-IT")}đ`,
            currentPrice: `${item.price.toLocaleString("it-IT")}đ`,
            totalLesson: item.totalLesson,
            timeperiod: item.periodOfTime,
            description: item.description,
            isActive: item.isActive,
          }));

          const sortedOutput = SelectOptions.sort((a: any, b: any) => {
            const priceA = parseFloat(a.currentPrice.replace("đ", ""));
            const priceB = parseFloat(b.currentPrice.replace("đ", ""));
            return priceA - priceB;
          });

          GetDataSubcribe(sortedOutput);
        })
        .catch((err: any) => {
          // handle error
          //
        });
    };
    dataSubribe();
  }, []);

  const newArr = dataSubcribe?.filter(
    (value: any) => value?.label !== "Gói Trial"
  );

  const handleSendReq = async (e: any) => {
    const response = await callAPI.UPdateAccount(e);
    form.resetFields();
    setOpen(!open);
  };
  const profile = useSelector(getProfileSelector);
  let initialValue;
  if (profile) {
    initialValue = {
      fullName: profile.name,
      email: profile?.email,
      phoneNumber: profile?.phoneNumber,
      subcriptionId: `${trans["formUpgrade.note.placeholder"]}`,
    };
  }

  return (
    <Modal
      wrapClassName="modal-add-noti"
      className="z-auto"
      open={open}
      footer={false}
      onCancel={() => setOpen(!open)}
    >
      <img
        src="/assets/icons/ic-shark.gif"
        alt="gif"
        style={{ objectFit: "contain" }}
        className="absolute left-1/2 top-0 h-160 w-160 translate-x-center translate-y-center"
      />
      <div className="mt-[3rem] flex items-center justify-center md:mt-[3.5rem]">
        <div className="text-center md:ml-0 md:text-left">
          <h6 className="font-UTM text-center text-[1.11rem] font-bold leading-38 text-secondary">
            {trans["formUpgrade.title"]}
          </h6>
          <p className="italic leading-25.6 text-black-400 md:block">
            {trans["popup.upgrade.title"]}
          </p>
        </div>
      </div>
      <Form
        className="w-full"
        layout="vertical"
        onFinish={handleSendReq}
        form={form}
        initialValues={initialValue}
      >
        <Form.Item
          name="fullName"
          rules={[
            {
              required: true,
              message: `${trans["login.form.name.required"]}`,
            },
          ]}
        >
          <Input
            placeholder={trans["login.form.name.placeholder"]}
            prefix={<IcUser />}
          />
        </Form.Item>
        <Form.Item
          name="email"
          rules={[
            {
              required: true,
              message: `${trans["login.form.email.required"]}`,
            },
          ]}
        >
          <Input
            placeholder={trans["login.form.email.placeholder"]}
            prefix={<IcMail />}
          />
        </Form.Item>
        <Form.Item
          name="phoneNumber"
          rules={[
            {
              required: true,
              message: `${trans["homepage.upgrade.form.phone.require"]}`,
            },
          ]}
        >
          <Input
            placeholder={trans["homepage.upgrade.form.phone"]}
            prefix={<IcPhone />}
          />
        </Form.Item>
        {/* <Form.Item name="address">
        <Input
          placeholder="Địa chỉ bạn muốn nhận thẻ học"
          prefix={<IcLocation />}
        />
      </Form.Item> */}
        <Form.Item
          name="subcriptionId"
          rules={[
            {
              required: true,
              message: `${trans["common.form.require.select"]}`,
            },
          ]}
        >
          <Select
            defaultValue={trans["common.placeholder.select"]}
            className="custom-select-antd"
            size="large"
            // onChange={handleChange}
            // onChange={(label) => {
            //
            // }}
            // value={newArr.label}

            options={newArr}
            // }
            // onSelect={(key) =>
          >
            {/* {newArr.map((index: any) => (
            <Option key={index} value={index?.value}>
              {index?.label}
            </Option>
          ))} */}
          </Select>
        </Form.Item>
        <Form.Item name="description">
          <Input.TextArea placeholder={trans["page.label.customer.note"]} />
        </Form.Item>
        <div className="mb-2 mt-20 flex justify-center">
          <Button
            size="large"
            type="primary"
            className="rounded-[12px]"
            htmlType="submit"
            // onClick={() => {
            //   // eslint-disable-next-line @typescript-eslint/no-unused-expressions
            //   infoPermission === true ? setIsOpen(!isOpen) : ReqNoti();
            // }}
          >
            {" "}
            {trans["common.button.registerNow"]}
          </Button>
        </div>
      </Form>
    </Modal>
  );
}

export default UpgradeAccount;
