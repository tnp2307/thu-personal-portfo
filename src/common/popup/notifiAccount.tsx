import { Button, notification, Space } from "antd";
// eslint-disable-next-line import/no-extraneous-dependencies
import moment from "moment";
import { useRouter } from "next/router";
import { useEffect, useState } from "react";
import { useSelector } from "react-redux";

import { getProfileSelector } from "@/store/auth/selectors";
import { localStorageHelpers } from "@/utils/localStorage";

import UpgradeAccount from "./popup_Upgrade";

const NotiAccount = () => {
  const router = useRouter();
  const profileInfo = useSelector(getProfileSelector);
  const accessToken = localStorageHelpers.getToken();
  const [open, setOpen] = useState(false);
  const [api, contextHolder] = notification.useNotification();
  const a: any = moment(new Date()).format("YYYY-MM-DD");
  const b: any = moment(profileInfo?.expiredDate).format("YYYY-MM-DD");
  const as: any = new Date(a);
  const bs: any = new Date(b);
  const timeDiff = Math.abs(as.getTime() - bs.getTime());
  const diffDays = Math.ceil(timeDiff / (1000 * 3600 * 24)) || 365;

  const hanldeEventLogin = () => {
    if (accessToken) {
      setOpen(!open);
      api.destroy();
      // router.push("/#upgrade-form");
    } else {
      router.push("/#upgrade-form");
    }
  };
  const openNotification = (text: string, desc: string) => {
    const key = `open${Date.now()}`;
    const btn = (
      <Space>
        <Button type="link" size="small" onClick={() => api.destroy()}>
          Đóng
        </Button>
        <Button
          type="primary"
          size="small"
          // onClick={() => router.push("/#upgrade-form")}
          onClick={() => hanldeEventLogin()}
        >
          Nâng cấp ngay
        </Button>
      </Space>
    );
    api.open({
      message: text,
      description: desc,
      btn,
      key,
      // onClose: () =>
      duration: 3000,
      // style: { marginTop: 100 },
    });
  };
  // const { setPopupWanning, title } = props;
  // const hanldeEventLogin = () => {
  //   router.push("/#upgrade-form");
  // };
  useEffect(() => {
    if (
      moment(profileInfo?.expiredDate).format("YYYYMMDD") <
      moment(new Date()).format("YYYYMMDD")
    ) {
      // accountStatus = Expired;
      // openNotification(
      //   "Tài khoản đã hết hạn",
      //   "Nâng cấp tài khoản để tiếp tục học cùng Best-English nhé"
      // );
      // setHide(!hide);
    } else if (
      moment(profileInfo?.expiredDate).format("YYYYMMDD") >
        moment(new Date()).format("YYYYMMDD") &&
      diffDays <= 7
    ) {
      // accountStatus = `${accountStatus}
      //   - Hết hạn sau ${diffDays} ngày
      // `;
      openNotification(
        `Tài khoản sẽ hết hạn sau ${diffDays} ngày`,
        "Nâng cấp tài khoản để tiếp tục học cùng Best-English nhé"
      );
      // setHide(!hide);
    } else if (
      moment(profileInfo?.expiredDate).format("YYYYMMDD") ===
      moment(new Date()).format("YYYYMMDD")
    ) {
      // accountStatus = ExpiredToday;
      openNotification(
        "Tài khoản sẽ hết hạn hôm nay",
        "Nâng cấp tài khoản để tiếp tục học cùng Best-English nhé"
      );
      // setHide(!hide);
    } else {
      // setHide(false);
      // openNotification("aaa", "ccc");
    }
  });
  return (
    <>
      <div>{contextHolder}</div>
      {open && <UpgradeAccount setOpen={setOpen} open={open} />}
    </>
  );
};

export default NotiAccount;
