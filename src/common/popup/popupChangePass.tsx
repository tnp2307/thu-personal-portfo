import { Button, Form, Input, Modal } from "antd";
import { useDispatch } from "react-redux";

import * as callAPI from "@/common/services/apiCoursesDetail";
import type { ResGetUserProfileData } from "@/interfaces/auth.interface";
import { getUserRequest } from "@/store/auth/actions";
import useTrans from "@/utils/userTrans";

interface IProfilePopupProp {
  open: boolean;
  setOpen: (open: boolean) => void;
  profile: ResGetUserProfileData | null;
}

function ChangePass(props: IProfilePopupProp) {
  const { open, setOpen, profile } = props;
  const [form] = Form.useForm();
  const trans = useTrans();
  const dispatch = useDispatch();

  const handleSendReq = async (e: any) => {
    const response = await callAPI.UPdateAccountInfor(e);
    form.resetFields();
    setOpen(!open);
    dispatch(getUserRequest());
  };

  return (
    <Modal
      wrapClassName="modal-edit-profile"
      className="z-auto"
      open={open}
      footer={false}
      onCancel={() => setOpen(!open)}
    >
      <div className="pt-[20px]">
        <div className="absolute left-1/2 top-0 h-160 w-160 translate-x-center translate-y-center overflow-hidden rounded-full border-2 border-green-400 bg-white">
          <img
            style={{
              height: 170,
              width: 170,
              objectFit: "cover",
              objectPosition: "center",
            }}
            src={
              profile?.photoUrl
                ? process.env.NEXT_PUBLIC_API_IMAGES + profile.photoUrl
                : "/assets/icons/ic-shark.gif"
            }
            alt="gif"
          />
        </div>
        <div className="mt-[2rem] flex items-center justify-center md:mt-[2.5rem]">
          <div className="text-center md:ml-0 md:text-left">
            <h6 className="font-UTM text-center text-[1.11rem] font-bold capitalize leading-38 text-secondary">
              {trans["profile.changePass.title"]}
            </h6>
          </div>
        </div>
        <Form
          className=" flex w-full flex-col gap-20"
          layout="vertical"
          onFinish={handleSendReq}
          form={form}
        >
          <Form.Item
            label={trans["profile.changePass.oldPass.title"]}
            name="oldPassword"
            rules={[
              {
                required: true,
                message: `${trans["profile.changePass.oldPass.require"]}`,
              },
            ]}
          >
            <Input.Password
              placeholder={trans["profile.changePass.oldPass.placeholder"]}
            />
          </Form.Item>
          <Form.Item
            name="newPassword"
            label={trans["profile.changePass.newPass.title"]}
            rules={[
              {
                required: true,
                message: `${trans["profile.changePass.newPass.require"]}`,
              },
            ]}
          >
            <Input.Password
              placeholder={trans["profile.changePass.newPass.placeholder"]}
            />
          </Form.Item>

          <Form.Item
            name="newPasswordRepeate"
            label={trans["profile.changePass.passRepeate.title"]}
            rules={[
              {
                required: true,
                message: `${trans["profile.changePass.passRepeate.require"]}`,
              },
              ({ getFieldValue }) => ({
                validator(_, value) {
                  if (!value || getFieldValue("newPassword") === value) {
                    return Promise.resolve();
                  }
                  return Promise.reject(
                    new Error(
                      `${trans["profile.changePass.passRepeate.require"]}`
                    )
                  );
                },
              }),
            ]}
          >
            <Input.Password
              placeholder={trans["profile.changePass.passRepeate.placeholder"]}
            />
          </Form.Item>
          <div className="col-span-2 mb-2 mt-20 flex justify-center">
            <Button
              size="large"
              type="primary"
              className="rounded-[12px]"
              htmlType="submit"
            >
              {" "}
              {trans["profile.changePass.title"]}
            </Button>
          </div>
        </Form>
      </div>
    </Modal>
  );
}

export default ChangePass;
