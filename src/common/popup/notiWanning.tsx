import { useRouter } from "next/router";
import { useState } from "react";

import useTrans from "@/utils/userTrans";

import UpgradeAccount from "./popup_Upgrade";

const NotiWanning = (props: any) => {
  const router = useRouter();
  const { setPopupWanning, title, showPopup } = props;
  const [open, setOpen] = useState(false);
  const handleUpgrade = () => {
    setOpen(true);
  };

  const trans = useTrans();
  return (
    <>
      <div className="bg-wrapper-popup-login">
        <div className="auth-box relative mx-auto w-full max-w-600 rounded-20 bg-white px-16 pb-44 pt-68 md:pt-100 lg:max-w-500 lg:px-44">
          {/* <IcShark className="absolute left-1/2 top-0 h-160 w-160 translate-x-center translate-y-center md:h-auto md:w-auto" /> */}
          <img
            src="/assets/icons/ic-shark-sad.gif"
            alt="gif"
            // style={{ width: "80%", height: "80%" }}
            className="absolute left-1/2 top-0 h-160 w-160 translate-x-center translate-y-center"
          />
          <div className="mb-24 flex items-center justify-center">
            <img src="/assets/icons/ic-logo-vertical.svg" alt="icon logo" />
            <div className="text-center md:ml-32 md:text-left">
              <h3 className="font-UTM mb-4 text-32 font-bold leading-38 text-secondary">
                {trans["common.label.notification"]}
              </h3>
              <p className="italic leading-25.6 text-black-400">{title}</p>
            </div>
          </div>
          <div className="wrapper-button">
            <div
              className="btn-popup cancel"
              onClick={() => setPopupWanning(false)}
            >
              {trans["common.button.cancel"]}
            </div>
            <div className="btn-popup login" onClick={() => handleUpgrade()}>
              {trans["common.button.upgrade"]}
            </div>
          </div>
        </div>
      </div>
      {open && <UpgradeAccount setOpen={setOpen} open={open} />}
    </>
  );
};

export default NotiWanning;
