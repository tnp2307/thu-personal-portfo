import axios from "axios";
import { useRouter } from "next/router";

import Logo from "@/public/assets/icons/ic-logo-vertical.svg";
import { localStorageHelpers } from "@/utils/localStorage";

const PopupLoginNoti = (props: any) => {
  const { open, setOpen } = props;
  const router = useRouter();
  const FCM: any = localStorageHelpers.getData("FCMService");
  const firebaseCode: any = localStorageHelpers.getData("tokenFirebase");
  const social: any = localStorageHelpers.getData("social");
  const hanldeEventNotify = async () => {
    if (FCM) {
      await axios({
        method: "post",
        url: `${process.env.NEXT_PUBLIC_API_ENDPOINT}/Auth/Login`,
        data: {
          userName: FCM.userName,
          password: FCM.password,
          firebaseToken: firebaseCode,
        },
        headers: {
          accept: "application/json",
        },
      })
        .then(() => {
          setOpen(true);
          // handle success
          // eslint-disable-next-line no-restricted-globals
          //
        })
        .catch(() => {
          // handle error
          //
        });
    }
    if (social) {
      await axios({
        method: "post",
        url: `${process.env.NEXT_PUBLIC_API_ENDPOINT}/Auth/LoginSocial`,
        data: {
          accesssToken: social.accesssToken,
          firebaseToken: firebaseCode,
          typeLogin: social.typeLogin,
          uid: social.uid,
        },
        headers: {
          accept: "application/json",
        },
      })
        .then(() => {
          setOpen(true);
          // handle success
          // eslint-disable-next-line no-restricted-globals
          //
        })
        .catch(() => {
          // handle error
          //
        });
    } else {
      router.push("/login");
    }
    // Cookies.remove(COOKIE_KEY.ACCESS_TOKEN);
    // router.push("/login");
    // setTimeout(() => {
    // }, 3000);
  };

  return (
    <>
      {open ? (
        <div className="bg-wrapper-popup-login">
          <div className="auth-box relative mx-auto mt-52 w-full max-w-600 rounded-20 bg-white px-16 pb-44 pt-68 md:pt-100 lg:max-w-500 lg:px-44">
            {/* <IcShark className="absolute left-1/2 top-0 h-160 w-160 translate-x-center translate-y-center md:h-auto md:w-auto" /> */}
            <img
              src="/assets/icons/ic-shark-sad.gif"
              alt="gif"
              // style={{ width: "80%", height: "80%" }}
              className="absolute left-1/2 top-0 h-160 w-160 translate-x-center translate-y-center"
            />
            <div className="mb-24 flex items-center justify-center">
              <Logo className="hidden md:block" />
              <div className="text-center md:ml-32 md:text-left">
                <h3 className="font-UTM mb-4 ml-4 text-32 font-bold leading-38 text-secondary">
                  {"Đăng nhập tài khoản"}
                </h3>
                <p className="italic leading-25.6 text-black-400">
                  {"Đăng nhập để nhận thông báo ôn tập bạn nhé !"}
                </p>
              </div>
            </div>
            <div className="wrapper-button-noti">
              {/* <div className="btn-popup login" onClick={() => handlePopUp()}>
            Huy
          </div> */}
              <div
                className="btn-popup login"
                onClick={() => router.push("/login")}
              >
                {"Đăng nhập"}
              </div>
            </div>
          </div>
        </div>
      ) : (
        ""
      )}
    </>
  );
};

export default PopupLoginNoti;
