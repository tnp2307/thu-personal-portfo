import { useRouter } from "next/router";

import Logo from "@/public/assets/icons/ic-logo-vertical.svg";
import useTrans from "@/utils/userTrans";
// import IcShark from "@/public/assets/icons/ic-shark-sad.svg";

const PopupLogin = (props: any) => {
  const router = useRouter();
  const { setHandle } = props;
  const hanldeEventLogin = () => {
    router.push("/login?courses=true");
  };
  const trans = useTrans();
  return (
    <div className="bg-wrapper-popup-login">
      <div className="auth-box relative mx-auto w-[95%] max-w-600 rounded-20 bg-white px-16 pb-44 pt-68 md:pt-100 lg:max-w-500 lg:px-44">
        {/* <IcShark className="absolute left-1/2 top-0 h-160 w-160 translate-x-center translate-y-center md:h-auto md:w-auto" /> */}
        <img
          src="/assets/icons/ic-shark-sad.gif"
          alt="gif"
          // style={{ width: "80%", height: "80%" }}
          className="absolute left-1/2 top-0 h-160 w-160 translate-x-center translate-y-center"
        />
        <div className="mb-24 flex items-center justify-center">
          <Logo className="hidden md:block" />
          <div className="text-center md:ml-32 md:text-left">
            <h3 className="font-UTM mb-4 text-32 font-bold leading-38 text-secondary">
              {trans["login.title.beforeLogin"]}
            </h3>
            <p className="italic leading-25.6 text-black-400">
              {trans["login.title.beforeLogin.sub"]}
            </p>
          </div>
        </div>
        <div className="wrapper-button">
          <div className="btn-popup cancel" onClick={() => setHandle(false)}>
            {trans["common.button.cancel"]}
          </div>
          <div className="btn-popup login" onClick={() => hanldeEventLogin()}>
            {trans["common.button.login"]}
          </div>
        </div>
      </div>
    </div>
  );
};

export default PopupLogin;
