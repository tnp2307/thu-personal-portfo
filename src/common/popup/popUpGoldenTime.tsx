import { Button, Form, Modal } from "antd";
import axios from "axios";
import { initializeApp } from "firebase/app";
import { getMessaging, getToken } from "firebase/messaging";
import React, { useEffect, useState } from "react";

import { firebaseConfig, publicValidKey } from "@/utils/firebase.config";
import { localStorageHelpers } from "@/utils/localStorage";
import useTrans from "@/utils/userTrans";

type ModalNotificationProps = {
  isOpen: boolean;
  onClose: Function;
};

export const ModalNotification = (props: ModalNotificationProps) => {
  const [isOpen, setIsOpen] = useState(false);
  const trans = useTrans();
  const FCM: any = localStorageHelpers.getData("FCMService");
  const firebaseCode: any = localStorageHelpers.getData("tokenFirebase");
  const social: any = localStorageHelpers.getData("social");
  const [infoPermission, GetInfoPermission] = useState(false);
  const ReqNoti = async () => {
    await Notification.requestPermission().then(async (permission) => {
      if (permission === "granted") {
        await navigator.serviceWorker
          .register("./firebase-messaging-sw.js")
          .then(() => {
            setIsOpen(!isOpen);
          })
          .catch((err) => {});
        // Initialize Firebase
        const app = initializeApp(firebaseConfig);
        // Initialize Firebase Cloud Messaging and get a reference to the service
        const messaging = getMessaging(app);
        // Add the public key generated from the console here.
        getToken(messaging, {
          vapidKey: publicValidKey,
        }).then(async (currentToken) => {
          localStorage.setItem("tokenFirebase", currentToken);
          if (FCM) {
            await axios({
              method: "post",
              url: `${process.env.NEXT_PUBLIC_API_ENDPOINT}/Auth/Login`,
              data: {
                userName: FCM.userName,
                password: FCM.password,
                firebaseToken: currentToken,
              },
              headers: {
                accept: "application/json",
              },
            })
              .then(() => {
                // setOpen(true);
                // handle success
                // eslint-disable-next-line no-restricted-globals
                //
              })
              .catch(() => {
                // handle error
                //
              });
          } else if (social) {
            await axios({
              method: "post",
              url: `${process.env.NEXT_PUBLIC_API_ENDPOINT}/Auth/LoginSocial`,
              data: {
                accesssToken: social.accesssToken,
                firebaseToken: currentToken,
                typeLogin: social.typeLogin,
                uid: social.uid,
              },
              headers: {
                accept: "application/json",
              },
            })
              .then(() => {
                // setOpen(true);
                // handle success
                // eslint-disable-next-line no-restricted-globals
                //
              })
              .catch(() => {
                // handle error
                //
              });
          }
        });
      } else {
        GetInfoPermission(true);
      }
    });
  };
  const ReqNotiTwo = async () => {
    // await Notification.requestPermission().then(async (permission) => {
    if (typeof window !== "undefined" && window.Notification) {
      if (Notification.permission === "granted") {
        await navigator.serviceWorker
          .register("./firebase-messaging-sw.js")
          .then(() => {
            // setIsOpen(!isOpen);
          })
          .catch((err) => {});
        // Initialize Firebase
        const app = initializeApp(firebaseConfig);
        // Initialize Firebase Cloud Messaging and get a reference to the service
        const messaging = getMessaging(app);
        // Add the public key generated from the console here.
        getToken(messaging, {
          vapidKey: publicValidKey,
        }).then(async (currentToken) => {
          localStorage.setItem("tokenFirebase", currentToken);
          if (FCM) {
            await axios({
              method: "post",
              url: `${process.env.NEXT_PUBLIC_API_ENDPOINT}/Auth/Login`,
              data: {
                userName: FCM.userName,
                password: FCM.password,
                firebaseToken: currentToken,
              },
              headers: {
                accept: "application/json",
              },
            })
              .then(() => {
                // setOpen(true);
                // handle success
                // eslint-disable-next-line no-restricted-globals
                //
              })
              .catch(() => {
                // handle error
                //
              });
          } else if (social) {
            await axios({
              method: "post",
              url: `${process.env.NEXT_PUBLIC_API_ENDPOINT}/Auth/LoginSocial`,
              data: {
                accesssToken: social.accesssToken,
                firebaseToken: currentToken,
                typeLogin: social.typeLogin,
                uid: social.uid,
              },
              headers: {
                accept: "application/json",
              },
            })
              .then(() => {
                // setOpen(true);
                // handle success
                // eslint-disable-next-line no-restricted-globals
                //
              })
              .catch(() => {
                // handle error
                //
              });
          }
        });
      } else {
        GetInfoPermission(true);
      }
    }
    // }
    // );
  };
  useEffect(() => {
    if (typeof window !== "undefined" && window.Notification) {
      if (
        Notification.permission === "granted" &&
        localStorage.getItem("tokenFirebase") === null
      ) {
        ReqNotiTwo();
      }
      if (Notification.permission === "granted") {
        setIsOpen(false);
      }
      if (Notification.permission === "default") {
        setIsOpen(true);
        navigator.serviceWorker?.getRegistrations().then((registrations) => {
          // for (const registration of registrations) {
          //   registration.unregister();
          // }
          if (caches) {
            // Service worker cache should be cleared with caches.delete()
            caches.keys().then(async (names) => {
              await Promise.all(names.map((name) => caches.delete(name)));
            });
          }
        });
      }
      if (Notification.permission === "denied") {
        GetInfoPermission(true);
        setIsOpen(true);
        // localStorage.removeItem("tokenFirebase");
        navigator.serviceWorker?.getRegistrations().then((registrations) => {
          // for (const registration of registrations) {
          //   registration.unregister();
          // }
          if (caches) {
            // Service worker cache should be cleared with caches.delete()
            caches.keys().then(async (names) => {
              await Promise.all(names.map((name) => caches.delete(name)));
            });
          }
        });
      }
    }
  }, []);
  // const handleReqLogin = async () => {
  //   if (FCM) {
  //     await axios({
  //       method: "post",
  //       url: `${process.env.NEXT_PUBLIC_API_ENDPOINT}/Auth/Login`,
  //       data: {
  //         userName: FCM.userName,
  //         password: FCM.password,
  //         firebaseToken: firebaseCode,
  //       },
  //       headers: {
  //         accept: "application/json",
  //       },
  //     })
  //       .then(() => {
  //         // setOpen(true);
  //         // handle success
  //         // eslint-disable-next-line no-restricted-globals
  //         //
  //       })
  //       .catch(() => {
  //         // handle error
  //         //
  //       });
  //   } else if (social) {
  //     await axios({
  //       method: "post",
  //       url: `${process.env.NEXT_PUBLIC_API_ENDPOINT}/Auth/Login`,
  //       data: {
  //         accesssToken: social.accesssToken,
  //         firebaseToken: firebaseCode,
  //         typeLogin: social.typeLogin,
  //         uid: social.uid,
  //       },
  //       headers: {
  //         accept: "application/json",
  //       },
  //     })
  //       .then(() => {
  //         // setOpen(true);
  //         // handle success
  //         // eslint-disable-next-line no-restricted-globals
  //         //
  //       })
  //       .catch(() => {
  //         // handle error
  //         //
  //       });
  //   } else {
  //
  //   }
  // };
  return (
    <>
      <Modal
        wrapClassName="modal-add-noti"
        open={isOpen}
        footer={false}
        onCancel={() => setIsOpen(!isOpen)}
      >
        {/* <IcShark className="absolute left-1/2 top-0 h-160 w-160 translate-x-center translate-y-center md:h-auto md:w-auto " /> */}

        {infoPermission === true ? (
          <img
            src="/assets/icons/ic-shark-sad.gif"
            alt="gif"
            // style={{ width: "80%", height: "80%" }}
            className="absolute left-1/2 top-0 h-160 w-160 translate-x-center translate-y-center"
          />
        ) : (
          <img
            src="/assets/icons/ic-shark.gif"
            alt="gif"
            style={{ objectFit: "contain" }}
            className="absolute left-1/2 top-0 h-160 w-160 translate-x-center translate-y-center"
          />
        )}

        <div className="mt-[3rem] flex items-center justify-center md:mt-[3.5rem]">
          {/* <Logo className="hidden md:block" /> */}
          <div className="text-center md:ml-0 md:text-left">
            <h6 className="font-UTM text-center text-[1.11rem] font-bold leading-38 text-secondary">
              {infoPermission === true
                ? "Mở thông báo trình duyệt để được nhắc nhở ôn tập bạn nhé !"
                : `Bạn có muốn nhận thông báo "Thời điểm vàng" không!`}
              {}
            </h6>
            {/* <p className="italic leading-25.6 text-black-400 md:block">
              Hãy nhập mã thẻ học đã nhận được từ Best-English
            </p> */}
          </div>
        </div>
        <Form className="w-full" layout="vertical">
          {/* <Form.Item label="Nhập mã thẻ học của bạn">
            <Input placeholder="Nhập mã thẻ" prefix={<IcCard />} />
          </Form.Item> */}
          <div className="mb-2 mt-20 flex justify-center">
            <Button
              size="large"
              type="primary"
              className="rounded-[12px]"
              onClick={() => {
                // eslint-disable-next-line @typescript-eslint/no-unused-expressions
                infoPermission === true ? setIsOpen(!isOpen) : ReqNoti();
              }}
            >
              <span className="leading-16">
                {infoPermission === true
                  ? `${trans["common.button.underStood"]}`
                  : `${trans["common.button.turnNoti"]}`}
              </span>
            </Button>
          </div>
        </Form>
      </Modal>
    </>
  );
};

// export const ModalNotification;
