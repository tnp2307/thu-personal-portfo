import { useRouter } from "next/router";
import React from "react";
import { useDispatch } from "react-redux";

import { learnStatusRequest } from "@/store/unit/actions";
import useTrans from "@/utils/userTrans";

const CloseUnitPopup = ({ setClose, idQuery }: any) => {
  const router = useRouter();
  const trans = useTrans();
  const dispatch = useDispatch();
  const handleFinish = () => {
    setClose(false);
  };
  const handleCloseUnit = () => {
    router.back();
    const dataLearnStatus = {
      isCompleted: false,
      unitHoursLearner: 0,
      unitiD: idQuery,
    };
    dispatch(learnStatusRequest(dataLearnStatus));
  };
  return (
    <div className="bg-wrapper-popup-login">
      <div className="auth-box relative mx-auto w-full max-w-600 rounded-20 bg-white px-16 pb-44 pt-68 md:pt-100 lg:max-w-500 lg:px-44">
        {/* <IcShark className="absolute left-1/2 top-0 h-160 w-160 translate-x-center translate-y-center md:h-auto md:w-auto" /> */}
        <img
          src="/assets/icons/ic-shark-sad.gif"
          alt="gif"
          // style={{ width: "80%", height: "80%" }}
          className="absolute left-1/2 top-0 h-160 w-160 translate-x-center translate-y-center"
        />
        <div className="flex items-center justify-center">
          {/* <Logo className="hidden md:block" /> */}
          <div className="text-center md:text-center">
            <h3 className="font-UTM  text-20 font-bold leading-38 text-secondary">
              {trans["popup.closeUnit.title"]}
            </h3>

            <p className="text-14 italic leading-25.6 text-black-400">
              {trans["popup.closeUnit.sub"]}
            </p>
          </div>
        </div>
        <div className="wrapper-btn">
          <div className="btn_close red" onClick={() => handleFinish()}>
            {trans["popup.closeUnit.button.stay"]}
          </div>
          <div className="btn_close white" onClick={() => handleCloseUnit()}>
            {trans["common.button.quit"]}
          </div>
        </div>
      </div>
    </div>
  );
};

export default CloseUnitPopup;
