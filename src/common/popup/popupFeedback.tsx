import { Checkbox, Input, message } from "antd";
import axios from "axios";
import { event } from "nextjs-google-analytics";
import React, { useState } from "react";

import { localStorageHelpers } from "@/utils/localStorage";
import useTrans from "@/utils/userTrans";

const arr = [
  { id: 1, name: "Phát âm không chính xác", checker: false },
  { id: 2, name: "Nghĩa của từ bị sai", checker: false },
  {
    id: 3,
    name: "Câu trả lời của tôi không được chấp nhận",
    checker: false,
  },
  { id: 4, name: "Xảy ra lỗi khác", checker: false },
];
// const element = document.querySelectorAll("audio");
const NotiFeedback = (props: any) => {
  const trans = useTrans();
  const [desc, setDesc] = useState("");
  const [getchecker, setchecker] = React.useState({
    dataList: arr,
  });
  const accessToken = localStorageHelpers.getToken();
  const { setPopupFeedback, unitId } = props;
  const result = getchecker.dataList.filter((x) => x.checker === true);
  const newString = result.map((x) => x.name).join(", ");
  const [textError, setTextError] = useState(false);
  const hanldeEventFeedback = async () => {
    if (desc === "") {
      message.error(trans["learnNewWord.fillWord.reportBug.error"]);
      setTextError(true);
      // setPopupFeedback(false);
    } else {
      setTextError(false);
      await axios({
        method: "post",
        url: `${process.env.NEXT_PUBLIC_API_ENDPOINT}/FAQs/Feedbacks`,
        data: [
          {
            description: `${`${newString}, ${desc}`}`,
            unitVocabularyId: unitId,
          },
        ],
        headers: {
          accept: "application/json",
          Authorization: `Bearer ${accessToken}`,
        },
      })
        .then((response) => {
          // handle success
          event("Bug report", {
            category: "Bug report",
            label: "Bug report",
          });
          message.success(
            trans["learnNewWord.fillWord.reportBug.send.success"]
          );
        })
        .catch((response) => {
          // handle error
          message.error(trans["learnNewWord.fillWord.reportBug.send.error"]);
        });
      setPopupFeedback(false);
    }
  };
  const onChange = (e: any) => {
    setchecker((prev: any) => ({
      ...prev,
      dataList: [
        // eslint-disable-next-line no-unsafe-optional-chaining
        ...prev.dataList?.filter((x: { id: any }) => x.id !== e.id),
        {
          ...e,
          checker: !prev.dataList?.find((x: { id: any }) => x.id === e.id)
            ?.checker,
        },
      ],
    }));
  };

  return (
    <div className="bg-wrapper-popup-login">
      <div className="auth-box relative mx-auto mt-[2rem] w-[95%] max-w-600 rounded-20 bg-white px-16 pb-44 pt-68 md:pt-100 lg:max-w-500 lg:px-44">
        {/* <IcShark className="absolute left-1/2 top-0 h-160 w-160 translate-x-center translate-y-center md:h-auto md:w-auto" /> */}
        <img
          src="/assets/icons/ic-shark-sad.gif"
          alt="gif"
          // style={{ width: "80%", height: "80%" }}
          className="absolute left-1/2 top-0 h-160 w-160 translate-x-center translate-y-center"
        />
        <div className="flex items-center justify-center">
          {/* <Logo className="hidden md:block" /> */}
          <div className="text-center md:text-center">
            <h3 className="font-UTM  text-20 font-bold leading-38 text-secondary">
              {trans["learnNewWord.fillWord.reportBug.send.title"]}
            </h3>

            <p className="text-14 italic leading-25.6 text-black-400">
              {trans["learnNewWord.fillWord.reportBug.send.sub"]}
            </p>
            <div className="flex-column mt-2 flex">
              {getchecker.dataList
                ?.sort((a, b) => {
                  if (a.id < b.id) {
                    return -1;
                  }
                  return 1;
                })
                .map((value, index) => {
                  return (
                    <Checkbox
                      value={value.id}
                      onChange={() => onChange(value)}
                      key={index}
                      checked={value.checker}
                    >
                      {value.name}
                    </Checkbox>
                  );
                })}
              <Input.TextArea
                value={desc}
                onChange={(value: any) => {
                  setDesc(value.target.value);
                  setTextError(false);
                }}
                className={`mt-3 ${textError ? "warning-text-area" : ""}`}
                rows={4}
                placeholder={
                  trans["learnNewWord.fillWord.reportBug.send.notePlaceholder"]
                }
              />
              {textError ? (
                <div className="text-left text-red-600">
                  {trans["learnNewWord.fillWord.reportBug.error"]}
                </div>
              ) : (
                <></>
              )}
            </div>
          </div>
        </div>
        <div className="wrapper-button">
          <div
            className="btn-popup cancel"
            onClick={() => setPopupFeedback(false)}
          >
            {trans["common.button.quit"]}
          </div>
          <div
            className="btn-popup login"
            onClick={() => hanldeEventFeedback()}
          >
            {trans["learnNewWord.fillWord.reportBug.send.button"]}
          </div>
        </div>
      </div>
    </div>
  );
};

export default NotiFeedback;
