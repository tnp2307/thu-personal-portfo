import { Button, Form, message, Modal, notification, Space } from "antd";
import React, { useEffect, useState } from "react";
import { useDispatch } from "react-redux";

import * as callAPI from "@/common/services/apilearnnewworks";
import { StatisticalRequest } from "@/store/notebook/actions";
import useTrans from "@/utils/userTrans";

type NotificationType = "success" | "info" | "warning" | "error";

const key = "PopupVocabularyHandbook";

export const PopupVocabularyHandbook: React.FC = () => {
  const [api, contextHolder] = notification.useNotification();
  const isLogged: any =
    JSON.parse(sessionStorage.getItem("Vocabulary_handbook")!) || [];

  const callDataAPI = async () => {
    const response = await callAPI.resSaveHandBooks(isLogged);
    if (response.message === "SUCCESSFULL") {
      message.success("Lưu thay đổi thành công ");
      sessionStorage.removeItem("Vocabulary_handbook");
    }
  };

  const btn = (
    <Space>
      <Button
        type="link"
        size="small"
        onClick={() => {
          sessionStorage.removeItem("Vocabulary_handbook");
          api.destroy();
        }}
      >
        Huỷ
      </Button>
      <Button
        type="primary"
        size="small"
        onClick={() => {
          callDataAPI();
          api.destroy();
        }}
      >
        Lưu thay đổi
      </Button>
    </Space>
  );

  const openNotificationWithIcon = (type: NotificationType) => {
    api[type]({
      key,
      message: "Thông báo",
      description: "Từ vựng trong sổ tay của bạn đã được thay đổi",
      btn,
      onClose: () => {
        sessionStorage.removeItem("Vocabulary_handbook");
      },
    });
  };
  useEffect(() => {
    if (isLogged?.length > 0) {
      openNotificationWithIcon("success");
    }
  }, [isLogged.length]);

  return <>{contextHolder}</>;
};

//  default PopupVocabularyHandbook;

type ModalAddCodeProps = {
  isOpen: boolean;
  onClose: Function;
};

export const ModalNotes = (props: ModalAddCodeProps) => {
  const trans = useTrans();
  const dispatch = useDispatch();
  // const { setIsOpenAddCode } = props;
  const isLogged: any =
    JSON.parse(sessionStorage.getItem("Vocabulary_handbook")!) || [];

  const callDataAPI = async () => {
    const response = await callAPI.resSaveHandBooks(isLogged);
    // open backdrop
    if (response.message === "SUCCESSFULL") {
      message.success(trans["popup.notebookChange.button.save"]);
      dispatch(StatisticalRequest());
      sessionStorage.removeItem("Vocabulary_handbook");
    }
  };
  const [loading, setLoading] = useState(false);
  // useEffect(() => {
  //   if (isLogged?.length > 0) {
  //     // openNotificationWithIcon("success");

  //     // props.isOpen = true;s
  //   }
  // }, [isLogged.length]);
  return (
    <>
      <Modal
        wrapClassName="modal-add-code"
        open={props.isOpen}
        footer={false}
        onCancel={() => props.onClose()}
        style={{ marginTop: "20vh" }}
      >
        {/* <IcShark className="absolute left-1/2 top-0 h-160 w-160 translate-x-center translate-y-center md:h-auto md:w-auto " /> */}
        <img
          src="/assets/icons/ic-shark.gif"
          alt="gif"
          style={{ objectFit: "contain" }}
          className="absolute left-1/2 top-0 h-160 w-160 translate-x-center translate-y-center"
        />
        <div className="mb-24 mt-20 flex items-center justify-center md:mt-[2.5rem]">
          {/* <Logo className="hidden md:block" /> */}
          <div className="text-center md:text-left">
            <h6 className="font-UTM mt-[2rem] text-22 font-bold leading-38 text-secondary sm:mt-[1.25rem]">
              {trans["popup.notebookChange.title"]}
            </h6>
            {/* <p className="italic leading-25.6 text-black-400 md:block">
              Bạn vừa thay đổi danh sách từ cần ôn tập
            </p> */}
          </div>
        </div>
        <Form className="w-full" layout="vertical">
          <div className="mt-30 flex flex-col justify-around md:px-[7rem] ">
            <Button
              size="large"
              type="primary"
              className="rounded-4"
              loading={loading}
              onClick={() => {
                setLoading(true);
                callDataAPI().then(() => {
                  setLoading(false);
                  props.onClose();
                });
              }}
            >
              <span className="leading-16">
                {trans["popup.notebookChange.button.save"]}
              </span>
            </Button>
            <Button
              size="large"
              className="mt-2 rounded-4"
              onClick={() => props.onClose()}
            >
              <span className="leading-16">
                {trans["common.button.cancel"]}
              </span>
            </Button>
          </div>
        </Form>
      </Modal>
    </>
  );
};
