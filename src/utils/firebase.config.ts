import { getApps, initializeApp } from "firebase/app";

// Your web app's Firebase configuration
export const publicValidKey =
  "BDpx82IX-t14_rLBfNyO1tbE3TYwtOxgC40vu_9NMAfpOzoRuXiRZFvLIasToetWXdFoIGkNI5xp1Zg0wP0ep7k";

export const firebaseConfig = {
  apiKey: "AIzaSyCbwf12qeC94VdcnShSsG0DJkdkri8yuKU",
  authDomain: "best-english-3a100.firebaseapp.com",
  projectId: "best-english-3a100",
  storageBucket: "best-english-3a100.appspot.com",
  messagingSenderId: "250446488952",
  appId: "1:250446488952:web:7e8a6d7b7860773f9a948d",
  measurementId: "G-JG5YXW1YK7",
};

// Initialize Firebase
const firebaseApp =
  // create a new app only if it doesn't already exists
  getApps().length === 0 ? initializeApp(firebaseConfig) : getApps()[0];

export default firebaseApp;
