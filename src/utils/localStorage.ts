import { STORAGE_KEY } from "./config";

const loadState = (key: string): string => {
  const serializedState = localStorage.getItem(key);

  if (serializedState === null) {
    return "false";
  }
  return JSON.parse(serializedState);
};

const getData = (key: string): string | null => {
  try {
    const value = localStorage.getItem(key);
    if (value !== null) {
      return String(value);
    }
    return null;
  } catch (error) {
    return null;
  }
};

const saveState = (key: string, value: any): void => {
  const serializedState = value;
  return localStorage.setItem(key, serializedState);
};

const deleteData = (key: string): void => localStorage.removeItem(key);

const getToken = (): string | null => {
  try {
    const value = getData(STORAGE_KEY.ACCESS_TOKEN);
    if (value !== null) {
      return value;
    }
    return null;
  } catch (error) {
    return null;
  }
};

const saveToken = (token: string): boolean => {
  try {
    saveState(STORAGE_KEY.ACCESS_TOKEN, token);
    return true;
  } catch {
    return false;
  }
};

const deleteToken = (key: string): void => deleteData(key);

export const localStorageHelpers = {
  loadState,
  getData,
  saveState,
  deleteData,
  getToken,
  saveToken,
  deleteToken,
};
