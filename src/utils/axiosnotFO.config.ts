import { message } from "antd";
import axios from "axios";

import type { Res } from "@/interfaces/common.interface";

// eslint-disable-next-line import/no-cycle
import { logout } from "./helpers";
import { localStorageHelpers } from "./localStorage";

export const apinotFO = axios.create({
  baseURL: process.env.NEXT_PUBLIC_API_AUTH,
  timeout: 30000,
  headers: {
    "X-Requested-With": "XMLHttpRequest",
    "Content-Type": "application/json",
    Accept: "application/json",
  },
});

apinotFO.interceptors.request.use(async (config) => {
  const accessToken = localStorageHelpers.getToken();
  if (accessToken) {
    // eslint-disable-next-line no-param-reassign
    config.headers.Authorization = `Bearer ${accessToken}`;
  }
  return config;
});

apinotFO.interceptors.response.use(
  (response) => response,
  (error) => {
    const data: Res = error?.response?.data;
    if (data && data.statusCode === 401) {
      window.location.href = "/login";
    } else if (data.statusCode === 406) {
      logout();
      message.error("Hãy chọn tài khoản khác !");
    }
    // const message =
    //   error && error?.response?.data?.message
    //     ? error?.response?.data?.message
    //     : "Sorry, something went wrong";
    // const status =
    //   error && error?.response?.status ? error?.response?.status : null;
    // if (status === 401) {
    // } else if (status === 422) {
    // } else if (status >= 500 || message === "Network Error") {
    // } else if (status === 410) {
    // } else {
    // }
    return Promise.reject(error);
  }
);

// apinotFO.defaults.headers.common.Authorization = getToken()
//   ? `Bearer ${getToken()}`
//   : null;

export function setAuthorization(token: any) {
  apinotFO.defaults.headers.common.Authorization = token
    ? `Bearer ${token}`
    : null;
}

export function clearAuthorization() {
  delete apinotFO.defaults.headers.common.Authorization;
}

export function removeAuthorization() {
  setAuthorization(null);
}
