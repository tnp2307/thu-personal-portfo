// eslint-disable-next-line import/no-cycle
import * as callAPI from "@/common/services/apiReviewNoteBook";
import { localStorageHelpers } from "@/utils/localStorage";

export async function logout() {
  const value: any = localStorageHelpers.getData("tokenFirebase") || "";
  const data = {
    deivce: "Web",
    firebaseToken: value,
  };
  await callAPI.removeTokenFirebase(data);
  if (typeof window !== "undefined") {
    localStorageHelpers.deleteData("tokenFirebase");
  }

  localStorage.clear();
  window.location.href = "/courses";
}
