import { useRouter } from "next/router";

import cm from "@/public/lang/cm.json";
import cn from "@/public/lang/cn.json";
import la from "@/public/lang/la.json";
import vi from "@/public/lang/vi.json";

const useTrans = () => {
  const { locale } = useRouter();

  let trans = vi;
  // locale === "cn" ? cn : locale === "cm" ? cm : locale === "la" ? la : vi;
  switch (locale) {
    case "cn":
      trans = cn;
      return trans;
    case "vi":
      trans = vi;
      return trans;
    case "cm":
      trans = cm;
      return trans;
    case "la":
      trans = la;
      return trans;
    default:
      break;
  }
  return trans;
};

export default useTrans;
