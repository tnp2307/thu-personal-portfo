export interface ResponsaveNewLearn {
  isMemorized: boolean;
  id: string;
  isReview: boolean;
}

export interface ResponsaveHandBook {
  id: string;
  isReview: boolean;
}
