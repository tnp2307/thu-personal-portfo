import type * as type from "@/interfaces/courses.interfaces";

export interface NoteBookSearch {
  type: string;
}
export interface NoteBookSearchs extends type.ReqGetCourses {}

export interface ResGetListNoteBookData {
  id: string;
  word: string;
  from: string;
  meaning: string;
  spelling: string;
  vocabularyLevel: number;
  isReview: boolean;
}
export interface ResGetListNotificationData {
  id: string;
  title: string;
  body: string;
  addedTimeStamp: string;
  typeIcon: number;
  isChecked: boolean;
}

export interface Notifi {
  noti: any;
}

export interface VocabularyID {
  id: string;
}
