export interface ReqGetCourses {
  search: string;
  pageSize: number;
  pageIndex: number;
  filterOptions: {
    column: string;
    value: string;
  }[];
  sortOptions: {
    column: string;
    direction: string;
  }[];
}

export interface ResponseStateUnit {
  loading: boolean;
  unit: Unit[];
  activeIndex: null | number;
  lesson: Lesson[];
  name: string;
}

interface Unit {
  id: string;
  isActive: boolean;
  name: string;
  isCompleted: boolean;
  imagesUnit: any;
}

interface Lesson {
  id: string;
  imagesLesson: string;
  description: string;
  name: string;
  isActive: boolean;
  isCompleted: boolean;
  imagesUnit: any;
}

export interface ButtonState {
  [key: number]: boolean;
}
