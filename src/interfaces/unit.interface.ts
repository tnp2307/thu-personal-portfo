import type * as type from "@/interfaces/courses.interfaces";

import type { Res } from "./common.interface";

export interface UnitDataRequest extends type.ReqGetCourses {}

export interface ResGetListUnitData {
  id: string;
  code: string;
  description: string;
  imagesCourse: string;
  isActive: boolean;
  name: string;
  numberUnit: number;
  priority: number;
  courseComplete: boolean;
}

export interface ResUnitData {
  items: ResGetListUnitData[] | null;
  totalRecord: number;
}

export interface ResUnit extends Res {
  data: ResUnitData;
}
